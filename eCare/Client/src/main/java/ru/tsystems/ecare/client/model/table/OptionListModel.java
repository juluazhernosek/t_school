package ru.tsystems.ecare.client.model.table;

import ru.tsystems.ecare.common.entities.OptionCommon;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 23.08.14.
 */
public class OptionListModel extends AbstractListModel<OptionCommon> {
    private List<OptionCommon> listOption;

    public OptionListModel(List<OptionCommon> options) {
        this();
        if(options != null)
            listOption.addAll(options);
    }

    public OptionListModel() {
        listOption = new ArrayList<>();
    }

    @Override
    public int getSize() {
        return listOption.size();
    }

    public void add(OptionCommon optionCommon) {
        listOption.add(optionCommon);
        fireIntervalAdded(this,listOption.size() - 1,listOption.size()-1);
    }
    public void remove(OptionCommon optionCommon) {
        listOption.remove(optionCommon);
        fireIntervalAdded(this,listOption.size() == 0 ? 0 : listOption.size() + 1,listOption.size() == 0 ? 0 : listOption.size());
    }


    @Override
    public OptionCommon getElementAt(int index) {
        return listOption.get(index);
    }

    public void clear() {
        listOption.clear();
        fireIntervalAdded(this,0,0);
    }

    public boolean contains(OptionCommon common) {
        for(OptionCommon optionCommon : listOption) {
            if(optionCommon.equals(common))
                return true;
        }
        return false;
    }
}
