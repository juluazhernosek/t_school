package ru.tsystems.ecare.client.bucket.entities;

import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

/**
 * Created by julua on 21.08.14.
 */
public class AddedContract {

    private ContractCommon contract;

    public AddedContract(ContractCommon contract) {
        this.contract = contract;
    }

    public ContractCommon getContract() {
        return contract;
    }


    @Override
    public boolean equals(Object o) {
        return o != null && contract != null && o instanceof AddedContract && contract.equals(((AddedContract)o).contract);
    }

}
