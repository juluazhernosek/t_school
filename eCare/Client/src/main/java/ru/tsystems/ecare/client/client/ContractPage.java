package ru.tsystems.ecare.client.client;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.common.entities.ContractCommon;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.List;

/**
 * Created by julua on 09.08.14.
 */
public class ContractPage extends AbstractFrame {

    public ContractPage(Callable callable, Object obj) {
        super(true, callable, obj);
    }

    protected void initWindow() {
        setSize(420, 250);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(230, 3, 250, 39);
        width = 400;
        height += 42;
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {

    }

    @Override
    protected void addActionsButton() {

    }

    protected void addContent() {

        List<ContractCommon> contracts = (List<ContractCommon>)obj;
        if(contracts == null)
            return;
        int countContracts = contracts.size();
        int countOptions = 0;
        for(ContractCommon contract : contracts)
            if(contract.getSelectedOptions() != null)
                countOptions += contract.getSelectedOptions().size();

        JLabel allContractsLabel = new JLabel("Your contracts:");
        allContractsLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        allContractsLabel.setBounds(10, 5, 290, 30);

        for (int m = 0; m < countContracts; m++) {

            ContractCommon contract = contracts.get(m);

            JLabel contractLabel = new JLabel((m+1)+". Contract:");
            contractLabel.setBounds(10, 50 + 150 * m + m*countOptions*40, 290, 30);

            JLabel numberLabel = new JLabel("Number:");
            numberLabel.setBounds(10, 80 +150 * m + m*countOptions*40, 290, 30);

            JTextField numberField = new JTextField();
            numberField.setBounds(100, 80 +150 * m + m*countOptions*40, 290, 30);
            numberField.setEnabled(false);
            numberField.setDisabledTextColor(Color.black);
            numberField.setText(contract.getPhoneNumber());

            JLabel tariffLabel = new JLabel("Tariff:");
            tariffLabel.setBounds(10, 120 +150 * m + m*countOptions*40, 290, 30);

            JTextField tariffField = new JTextField();
            tariffField.setBounds(100, 120 +150 * m + m*countOptions*40, 290, 30);
            tariffField.setText(contract.getTariff() == null ? "" : contract.getTariff().getName());
            tariffField.setEnabled(false);
            tariffField.setDisabledTextColor(Color.black);

            JLabel optionsLabel = new JLabel("Options:");
            optionsLabel.setBounds(10, 160 +150 * m + m*countOptions*40, 290, 30);

            int countOptionsContract = contracts.get(m).getSelectedOptions().size();
            height += 150 + countOptions*40;
            for (int i = 0; i < countOptionsContract; i++) {
                JTextField optionsField = new JTextField();
                optionsField.setBounds(10, (200 + i * 40)+150*m + m*countOptions*40, 290, 30);
                optionsField.setText(contract.getSelectedOptions().get(i).toString());
                optionsField.setEnabled(false);
                optionsField.setDisabledTextColor(Color.black);
                mainPanel.add(optionsField);
            }


            mainPanel.add(allContractsLabel);

            mainPanel.add(contractLabel);

            mainPanel.add(numberLabel);
            mainPanel.add(numberField);

            mainPanel.add(tariffLabel);
            mainPanel.add(tariffField);

            mainPanel.add(optionsLabel);
        }
    }

}
