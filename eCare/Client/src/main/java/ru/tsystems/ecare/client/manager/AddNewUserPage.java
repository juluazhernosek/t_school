package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.entities.AddedContract;
import ru.tsystems.ecare.client.bucket.entities.CommonBucket;
import ru.tsystems.ecare.client.commands.manager.WindowManagerNewContract;
import ru.tsystems.ecare.client.commands.manager.WindowManagerOpenBucketCreatingUser;
import ru.tsystems.ecare.client.commands.server.CreateUserCommand;
import ru.tsystems.ecare.client.commands.server.IsUniqueEmailCommand;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by julua on 12.08.14.
 */
public class AddNewUserPage extends AbstractFrame {

    private JButton addContractButton;
    private JButton addNewUserButton;
    private JButton bucketButton;
    private JTextField countContractsField;
    private JTextField nameField;
    private JTextField surnameField;
    private JTextField birthdayField;
    private JTextField passportField;
    private JTextField addressField;
    private JTextField emailField;
    private JTextField passwordField;
    private UserCommon newUser;
    private JComboBox roleComboBox;

    private CommonBucket commonBucket;

    public AddNewUserPage(Callable callable) {
        super(false, callable, null);
    }

    protected void initWindow() {
        commonBucket = new CommonBucket((newUser = new UserCommon()));
        newUser = new UserCommon();
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowActivated(WindowEvent e) {
                super.windowActivated(e);
                    countContractsField.setText(Integer.toString(commonBucket.getAddedContracts().size()));
            }
        });
        setSize(430, 580);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<>();
        commands.put("NewContract", new WindowManagerNewContract());
        commands.put("IsUniqueEmail",new IsUniqueEmailCommand());
        commands.put("CreateUser",new CreateUserCommand());
        commands.put("OpenBucket", new WindowManagerOpenBucketCreatingUser());
    }

    @Override
    protected void addActionsButton() {
        addContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("NewContract").execute(AddNewUserPage.this,commonBucket);
            }
        });
        addNewUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(!isValidName()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Name is not valid");
                    return;
                }
                if(!isValidSurname()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Surname is not valid");
                    return;
                }
                if(!isValidBirthday()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Birthday is not valid. Format : dd.mm.yyyy");
                    return;
                }
                if(!isValidEmail()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Email is not valid");
                    return;
                }
                if(!(Boolean)commands.get("IsUniqueEmail").execute(emailField.getText())) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "User with so email already exists");
                    return;
                }
                if(!isValidName()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Name is not valid");
                    return;
                }
                if(!isValidSurname()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Surname is not valid");
                    return;
                }
                if(!isValidPassword()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Password field is empty or very large");
                    return;
                }
                if(!isValidBirthday()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Birthday is not valid. Format : dd.mm.yyyy");
                    return;
                }
                if(!isValidPassport()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Passport field is empty or very large");
                    return;
                }
                if(!isValidAddress()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Address field is empty or very large");
                    return;
                }
                if(!isValidEmail()) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "Email is not valid");
                    return;
                }
                if(!(Boolean)commands.get("IsUniqueEmail").execute(emailField.getText())) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this, "User with so email already exists");
                    return;
                }
                newUser.setName(nameField.getText());
                newUser.setSurname(surnameField.getText());
                newUser.setPassword(passwordField.getText());
                newUser.setEmail(emailField.getText());
                newUser.setRegDate(new Date().toString());
                newUser.setAddress(addressField.getText());
                newUser.setPassport(passportField.getText());
                newUser.setRole(roleComboBox.getItemAt(roleComboBox.getSelectedIndex()).toString());
                try {
                    newUser.setBirthdayDate(new SimpleDateFormat("dd.MM.yyyy").parse(birthdayField.getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                newUser.setContracts(new ArrayList<ContractCommon>());
                for(AddedContract addedContract : commonBucket.getAddedContracts()) {
                    newUser.getContracts().add(addedContract.getContract());
                }
                Boolean response = (Boolean) commands.get("CreateUser").execute(newUser);
                if(response != null && response) {
                    JOptionPane.showMessageDialog(AddNewUserPage.this,"User saved");
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(AddNewUserPage.this,"Error user save", "Error saved",JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        bucketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("OpenBucket").execute(AddNewUserPage.this, commonBucket);
            }
        });
    }

    protected void addContent() {

        String [] roles = {"Client", "Manager"};

        JLabel addNewUserLabel = new JLabel("Add new user:");
        addNewUserLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        addNewUserLabel.setBounds(10, 20, 200, 30);

        JLabel nameLabel = new JLabel("Name");
        nameLabel.setBounds(10, 70, 200, 30);

        nameField = new JTextField();
        nameField.setBounds(90, 70, 300, 30);

        JLabel surnameLabel = new JLabel("Surname");
        surnameLabel.setBounds(10, 120, 200, 30);

        surnameField = new JTextField();
        surnameField.setBounds(90, 120, 300, 30);

        JLabel passwordLabel = new JLabel("Password");
        passwordLabel.setBounds(10, 170, 200, 30);

        passwordField = new JTextField();
        passwordField.setBounds(90, 170, 300, 30);

        JLabel birthdayLabel = new JLabel("Birthday");
        birthdayLabel.setBounds(10, 220, 200, 30);

        birthdayField = new JTextField();
        birthdayField.setBounds(90, 220, 300, 30);

        JLabel passportLabel = new JLabel("Passport");
        passportLabel.setBounds(10, 270, 200, 30);

        passportField = new JTextField();
        passportField.setBounds(90, 270, 300, 30);

        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(10, 320, 200, 30);

        addressField = new JTextField();
        addressField.setBounds(90, 320, 300, 30);

        JLabel emailLabel = new JLabel("Email");
        emailLabel.setBounds(10, 370, 200, 30);

        emailField = new JTextField();
        emailField.setBounds(90, 370, 300, 30);

        JLabel roleLabel = new JLabel("Role");
        roleLabel.setBounds(10, 420, 200, 30);

        roleComboBox = new JComboBox(roles);
        roleComboBox.setBounds(90, 420, 200, 30);

        JLabel countContractsLabel = new JLabel("Count user`s contract");
        countContractsLabel.setBounds(10, 470, 300, 30);

        countContractsField = new JTextField();
        countContractsField.setBounds(190, 470, 100, 30);
        countContractsField.setEnabled(false);
        countContractsField.setDisabledTextColor(Color.black);

        addContractButton = new JButton("Add contract");
        addContractButton.setBounds(190, 520, 150, 30);

        addNewUserButton = new JButton("Ok");
        addNewUserButton.setBounds(80, 520, 100, 30);

        bucketButton = new JButton();
        bucketButton.setBounds(340,470,30,30);
        bucketButton.setIcon(new ImageIcon(AbstractFrame.class.getResource("/bucket.png")));


        mainPanel.add(addNewUserLabel);

        mainPanel.add(nameLabel);
        mainPanel.add(nameField);

        mainPanel.add(surnameLabel);
        mainPanel.add(surnameField);

        mainPanel.add(passwordLabel);
        mainPanel.add(passwordField);

        mainPanel.add(birthdayLabel);
        mainPanel.add(birthdayField);

        mainPanel.add(passportLabel);
        mainPanel.add(passportField);

        mainPanel.add(addressLabel);
        mainPanel.add(addressField);

        mainPanel.add(emailLabel);
        mainPanel.add(emailField);

        mainPanel.add(roleLabel);
        mainPanel.add(roleComboBox);

        mainPanel.add(countContractsLabel);
        mainPanel.add(countContractsField);
        mainPanel.add(addContractButton);

        mainPanel.add(addNewUserButton);

        mainPanel.add(bucketButton);
    }


    private final Pattern regName = Pattern.compile("[a-zA-Z\\s]{1,20}");
    private boolean isValidName(){
        return regName.matcher(nameField.getText()).matches();
    }

    private boolean isValidSurname(){
        return regName.matcher(surnameField.getText()).matches();
    }

    private boolean isValidPassword(){
        return !passwordField.getText().isEmpty() && (passwordField.getText().length()<25);
    }

    private final Pattern regBirthday = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}");
    private boolean isValidBirthday(){
        return regBirthday.matcher(birthdayField.getText()).matches();
    }

    private boolean isValidPassport(){
        return !passportField.getText().isEmpty() && (passportField.getText().length()<50);
    }

    private boolean isValidAddress(){
        return !addressField.getText().isEmpty() && (addressField.getText().length()<50);
    }

    private final Pattern regEmail = Pattern.compile("^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$");
    private boolean isValidEmail(){
        return regEmail.matcher(emailField.getText()).matches();
    }
}
