package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.entities.AddedContract;
import ru.tsystems.ecare.client.bucket.entities.CommonBucket;
import ru.tsystems.ecare.client.bucket.entities.RemovedContract;
import ru.tsystems.ecare.client.commands.manager.WindowManagerOpenBucketCreatingUser;
import ru.tsystems.ecare.client.commands.server.AddContractCommand;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.IsUniqueNumberContractCommand;
import ru.tsystems.ecare.client.commands.server.RemoveContractCommand;
import ru.tsystems.ecare.client.model.table.OptionListModel;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by julua on 14.08.14.
 */
public class AddContractPage extends AbstractFrame {

    private List<TariffCommon> tariffList;
    private JComboBox<TariffCommon> tariffComboBox;
    private JComboBox<OptionCommon> optionsComboBox;
    private JButton addOptionButton;
    private JButton deleteContractButton;
    private JButton addContractButton;
    private ContractCommon contract;
    private UserCommon user;
    private JComboBox<ContractCommon> numbersComboBox;
    private JTextField numberTextField;
    private  JButton bucketButton;
    private CommonBucket bucket;
    private OptionListModel listModel;
    private JList<OptionCommon> optionList;
    private JButton okButton;

    public AddContractPage(Callable callable, Object obj) {
        super(false, callable, obj);
    }

    protected void initWindow() {
        setSize(370, 630);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        tariffList = (List<TariffCommon>) commands.get("GetAllTariff").execute();
        contract = new ContractCommon();
        contract.setSelectedOptions(new ArrayList<OptionCommon>());
        user= (UserCommon) obj;
        bucket = new CommonBucket(user);
        if(user.getContracts() == null)
            user.setContracts(new ArrayList<ContractCommon>());
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(175, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<>();
        commands.put("GetAllTariff", new GetAllTariffCommandServer());
        commands.put("OpenBucket", new WindowManagerOpenBucketCreatingUser());
        commands.put("IsUniqueNumberContractCommand",new IsUniqueNumberContractCommand());
        commands.put("AddContract",new AddContractCommand());
        commands.put("RemoveContract",new RemoveContractCommand());

    }

    @Override
    protected void addActionsButton() {
        addOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(contract.getSelectedOptions() == null)
                    contract.setSelectedOptions(new ArrayList<OptionCommon>());
                    OptionCommon selectedOption = optionsComboBox.getItemAt(optionsComboBox.getSelectedIndex());
                if(!contract.getSelectedOptions().contains(selectedOption)) {
                    contract.getSelectedOptions().add(selectedOption);
                    listModel.add(selectedOption);
                }

            }
        });

        tariffComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                TariffCommon tariff = (TariffCommon) itemEvent.getItem();
                if(tariff == null || tariff.getOptions() == null || tariff.getOptions().isEmpty())
                    optionsComboBox.removeAllItems();
                else {
                    optionsComboBox.removeAllItems();
                    for(OptionCommon option : tariff.getOptions()) {
                        optionsComboBox.addItem(option);
                    }
                }
                optionsComboBox.validate();
                optionsComboBox.repaint();
                if(contract.getSelectedOptions() == null)
                    contract.setSelectedOptions(new ArrayList<OptionCommon>());
                contract.getSelectedOptions().clear();
                listModel.clear();
            }
        });

        addContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Boolean unique = newTask(new java.util.concurrent.Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return (Boolean) commands.get("IsUniqueNumberContractCommand").execute(numberTextField.getText());
                    }
                },3);
                boolean isValidNumber = !numberTextField.getText().isEmpty();
                if(isValidNumber) {
                    contract.setPhoneNumber(numberTextField.getText());
                    contract.setTariff(tariffComboBox.getItemAt(tariffComboBox.getSelectedIndex()));
                    contract.setUser(user);
                    bucket.getAddedContracts().add(new AddedContract(contract));
                    numbersComboBox.addItem(contract);
                    user.getContracts().add(contract);
                    contract = new ContractCommon();
                    contract.setSelectedOptions(new ArrayList<OptionCommon>());
                    numberTextField.setText("");
                    listModel.clear();
                    numbersComboBox.validate();
                    numbersComboBox.repaint();
                }
                else {
                    JOptionPane.showMessageDialog(AddContractPage.this, "Number is not valid");
                }

            }
        });

        deleteContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ContractCommon deleteContract = numbersComboBox.getItemAt(numbersComboBox.getSelectedIndex());
                AddedContract added;
                if((added = bucket.addedContainsRemoved(new RemovedContract(deleteContract))) != null)
                    bucket.getAddedContracts().remove(added);
                else
                    bucket.getRemovedContracts().add(new RemovedContract(deleteContract));
                numbersComboBox.removeItem(deleteContract);
                user.getContracts().remove(deleteContract);
                numbersComboBox.validate();
                numbersComboBox.repaint();
                listModel.clear();
            }
        });
        bucketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("OpenBucket").execute(AddContractPage.this, bucket);
            }
        });
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                for(final AddedContract addedContract : bucket.getAddedContracts()) {
                    ContractCommon contractCommon = newTask(new java.util.concurrent.Callable<ContractCommon>() {
                        @Override
                        public ContractCommon call() throws Exception {
                            return (ContractCommon) commands.get("AddContract").execute(addedContract.getContract());
                        }
                    },4);
                    if(contractCommon == null) {
                        JOptionPane.showMessageDialog(AddContractPage.this,"Cann't save contract with number :" + addedContract.getContract().getPhoneNumber());
                    }
                }
                for(final RemovedContract removedContract : bucket.getRemovedContracts()) {
                    Boolean contractCommon = newTask(new java.util.concurrent.Callable<Boolean>() {
                        @Override
                        public Boolean call() throws Exception {
                            return (Boolean) commands.get("RemoveContract").execute(removedContract.getContract().getId());
                        }
                    },4);
                    if(contractCommon == null || !contractCommon) {
                        JOptionPane.showMessageDialog(AddContractPage.this,"Cann't remove contract with number :" + removedContract.getContract().getPhoneNumber());
                    }
                }
                dispose();
            }
        });
    }

    protected void addContent() {

        JLabel addNewContractLabel = new JLabel("Add new contract:");
        addNewContractLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        addNewContractLabel.setBounds(10, 20, 200, 30);

        JLabel numberLabel = new JLabel("Number:");
        numberLabel.setBounds(10, 70, 200, 30);

        numberTextField = new JTextField();
        numberTextField.setBounds(110, 70, 200, 30);

        JLabel tariffLabel = new JLabel("Choose tariff:");
        tariffLabel.setBounds(10, 110, 200, 30);

        if(tariffList == null || tariffList.isEmpty())
            tariffComboBox = new JComboBox<>();
        else {
            TariffCommon[] tariffs = new TariffCommon[tariffList.size()];
            for(int i = 0; i< tariffs.length; i++) {
                tariffs[i] = tariffList.get(i);
            }
            tariffComboBox = new JComboBox<>(tariffs);
            tariffComboBox.setSelectedIndex(0);
        }
        tariffComboBox.setBounds(120, 110, 200, 30);

        JLabel optionsLabel = new JLabel("Choose option:");
        optionsLabel.setBounds(10, 160, 200, 30);

        List<OptionCommon> options = tariffList == null || tariffList.isEmpty() ? null : tariffList.get(0).getOptions();

        if(options == null || options.isEmpty())
            optionsComboBox = new JComboBox<>();
        else {
            OptionCommon[] optionsArray = new OptionCommon[options.size()];
            for(int i = 0; i< optionsArray.length; i++) {
                optionsArray[i] = options.get(i);
            }
            optionsComboBox = new JComboBox<>(optionsArray);
        }
        optionsComboBox.setBounds(130, 160, 160, 30);

        addOptionButton = new JButton("Add");
        addOptionButton.setBounds(295, 160, 70, 30);

        JLabel countOptionsLabel = new JLabel("Connectivity options:");
        countOptionsLabel.setBounds(10, 200, 300, 30);


        listModel = new OptionListModel(contract.getSelectedOptions());
        optionList = new JList<>(listModel);
        optionList.setLayoutOrientation(JList.VERTICAL);
        JScrollPane scrollPaneList = new JScrollPane(optionList);
        scrollPaneList.setBounds(10, 240, 200, 100);


        addContractButton = new JButton("Add contract");
        addContractButton.setBounds(110, 360, 150, 40);

        JLabel deleteContractLabel = new JLabel("Delete contract:");
        deleteContractLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        deleteContractLabel.setBounds(10, 410, 200, 30);

        JLabel chooseNumberLabel = new JLabel("Choose number:");
        chooseNumberLabel.setBounds(10, 450, 200, 30);

        List<ContractCommon> contracts = user.getContracts();

        if(contracts.isEmpty())
            numbersComboBox = new JComboBox<>();
        else {
            ContractCommon[] contractsArray = new ContractCommon[contracts.size()];
            for(int i = 0; i< contractsArray.length; i++) {
                contractsArray[i] = contracts.get(i);
            }
            numbersComboBox = new JComboBox<>(contractsArray);
        }
        numbersComboBox.setBounds(140, 450, 200, 30);

        deleteContractButton = new JButton("Delete");
        deleteContractButton.setBounds(120, 500, 130, 40);

        okButton = new JButton("OK");
        okButton.setBounds(120, 550, 130, 40);

        bucketButton = new JButton();
        bucketButton.setBounds(340,0,30,30);
        bucketButton.setIcon(new ImageIcon(AbstractFrame.class.getResource("/bucket.png")));

        mainPanel.add(addNewContractLabel);

        mainPanel.add(numberLabel);
        mainPanel.add(numberTextField);

        mainPanel.add(okButton);

        mainPanel.add(tariffLabel);
        mainPanel.add(tariffComboBox);

        mainPanel.add(optionsLabel);
        mainPanel.add(optionsComboBox);
        mainPanel.add(addOptionButton);

        mainPanel.add(countOptionsLabel);
        mainPanel.add(scrollPaneList);

        mainPanel.add(addContractButton);

        mainPanel.add(deleteContractLabel);

        mainPanel.add(chooseNumberLabel);
        mainPanel.add(numbersComboBox);

        mainPanel.add(deleteContractButton);
        mainPanel.add(bucketButton);

    }
}

