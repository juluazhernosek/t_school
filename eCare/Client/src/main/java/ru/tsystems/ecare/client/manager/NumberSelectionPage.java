package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;

import javax.swing.*;
import java.net.URL;

/**
 * Created by julua on 11.08.14.
 */
public class NumberSelectionPage extends AbstractFrame {
    JButton numberButton;

    protected NumberSelectionPage(Callable callable) {
        super(false, callable, null);
    }

    protected void initWindow() {
        setSize(370, 160);
        setTitle("eCare");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {

    }

    @Override
    protected void addActionsButton() {

    }

    protected void addContent() {

        String[] numbers = {"4444444", "555555", "5454545454"};

        JLabel selectNumberLabel = new JLabel("Select number:");
        selectNumberLabel.setBounds(20, 30, 200, 30);

        JComboBox numberComboBox = new JComboBox(numbers);
        numberComboBox.setBounds(20, 70, 200, 20);

        numberButton = new JButton("Ok");
        numberButton.setBounds(140, 100, 100, 30);

        mainPanel.add(selectNumberLabel);
        mainPanel.add(numberComboBox);
        mainPanel.add(numberButton);
    }
}
