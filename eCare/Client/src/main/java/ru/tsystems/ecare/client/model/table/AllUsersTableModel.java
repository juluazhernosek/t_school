package ru.tsystems.ecare.client.model.table;

import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by julua on 19.08.14.
 */
public class AllUsersTableModel extends AbstractTableModel {

    public final static int COLUMN_COUNT = 7;
    private List<UserCommon> users;

    public AllUsersTableModel(List<UserCommon> userList) {
        users = userList;
    }


    @Override
    public int getRowCount() {
        return users.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public Object getValueAt(int i, int i2) {
        switch (i2) {
            case 0:
                return users.get(i).getName();
            case 1:
                return users.get(i).getSurname();
            case 2:
                return users.get(i).getBirthdayDate();
            case 3:
                return users.get(i).getPassport();
            case 4:
                return users.get(i).getAddress();
            case 5:
                return users.get(i).getEmail();
            case 6:
                return users.get(i).getRegDate();
            default:
                return "";
        }

    }

    @Override
    public String getColumnName(int c) {
        switch (c) {
            case 0:
                return "Name";
            case 1:
                return "Surname";
            case 2:
                return "Birthday";
            case 3:
                return "Passport";
            case 4:
                return "Address";
            case 5:
                return "Email";
            case 6:
                return "Date";
            default:
                return "";
        }
    }

    public UserCommon getSelectedItem(int row) {
        return users.get(row);
    }


}
