package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;

import javax.swing.*;
import java.awt.*;
import java.net.URL;


/**
 * Created by julua on 20.08.14.
 */
public class Bucket extends AbstractFrame {

    public Bucket(Callable callable, Object newObj, Object oldObj) {
        super(true, callable, newObj, oldObj);
    }

    protected void initWindow() {
        setSize(370, 200);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(170, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
    }

    @Override
    protected void addActionsButton() {
    }

    protected void addContent() {

        JLabel changesLabel = new JLabel("Your changes:");
        changesLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        changesLabel.setBounds(20, 20, 200, 30);

        JLabel newTariffLabel = new JLabel("New tariff:");
        newTariffLabel.setBounds(10, 70, 200, 30);

        JTextField newTariffField = new JTextField();
        newTariffField.setBounds(140, 70, 200, 30);
        newTariffField.setEnabled(false);
        newTariffField.setDisabledTextColor(Color.black);
        mainPanel.add(newTariffLabel);
        mainPanel.add(newTariffField);

        JLabel newOptionLabel = new JLabel("New options:");
        newOptionLabel.setBounds(10, 110, 200, 30);

        JTextField newOptionField = new JTextField();
        newOptionField.setBounds(10, 140, 200, 30);
        newOptionField.setEnabled(false);
        newOptionField.setDisabledTextColor(Color.black);
        mainPanel.add(newOptionLabel);
        mainPanel.add(newOptionField);

        JLabel deleteOptionLabel = new JLabel("Delete options:");
        deleteOptionLabel.setBounds(10, 180, 200, 30);

        JTextField deleteOptionField = new JTextField();
        deleteOptionField.setBounds(10, 220, 200, 30);
        deleteOptionField.setEnabled(false);
        deleteOptionField.setDisabledTextColor(Color.black);
        mainPanel.add(deleteOptionLabel);
        mainPanel.add(deleteOptionField);

        mainPanel.add(changesLabel);

        width = 350;
        height = 260;

    }
}
