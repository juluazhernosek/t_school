package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.server.SearchUserCommand;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

/**
 * Created by julua on 12.08.14.
 */
public class SearchByPhonePage extends AbstractFrame {

    private JTextField numberTextField;
    private JTextField nameField;
    private JTextField surnameField;
    private JTextField birthdayField;
    private JTextField passportField;
    private JTextField addressField;
    private JTextField emailField;
    private JTextField regDateField;
    private JButton searchButton;

    public SearchByPhonePage(Callable callable) {
        super(false, callable, null);;
    }

    protected void initWindow() {
        setSize(360, 480);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands.put("SearchUser",new SearchUserCommand());
    }

    @Override
    protected void addActionsButton() {
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                UserCommon user = (UserCommon) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("SearchUser").execute(numberTextField.getText());
                    }
                },3);
                if(user == null) {
                    JOptionPane.showMessageDialog(SearchByPhonePage.this, "User not found");
                    nameField.setText("");surnameField.setText("");birthdayField.setText("");
                    passportField.setText("");addressField.setText("");emailField.setText("");
                    regDateField.setText("");
                    return;
                }
                nameField.setText(user.getName());
                surnameField.setText(user.getSurname());
                birthdayField.setText(String.valueOf(user.getBirthdayDate()));
                passportField.setText(user.getPassport());
                addressField.setText(user.getAddress());
                emailField.setText(user.getEmail());
                regDateField.setText(user.getRegDate());
            }
        });
    }


    protected void addContent() {
        JLabel mainLabel = new JLabel("Search user by phone:");
        mainLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        mainLabel.setBounds(10, 10, 200, 30);

        JLabel numberLabel = new JLabel("Number:");
        numberLabel.setBounds(10, 60, 150, 30);

        numberTextField = new JTextField();
        numberTextField.setBounds(130, 60, 190, 30);

        searchButton = new JButton("Search");
        searchButton.setBounds(220, 95, 100, 20);

        JLabel nameLabel = new JLabel("Name");
        nameLabel.setBounds(10, 130, 200, 30);

        nameField = new JTextField();
        nameField.setBounds(90, 130, 230, 30);
        nameField.setEnabled(false);
        nameField.setDisabledTextColor(Color.black);

        JLabel surnameLabel = new JLabel("Surname");
        surnameLabel.setBounds(10, 170, 200, 30);

        surnameField = new JTextField();
        surnameField.setBounds(90, 170, 230, 30);
        surnameField.setEnabled(false);
        surnameField.setDisabledTextColor(Color.black);

        JLabel birthdayLabel = new JLabel("Birthday");
        birthdayLabel.setBounds(10, 220, 200, 30);

        birthdayField = new JTextField();
        birthdayField.setBounds(90, 220, 230, 30);
        birthdayField.setEnabled(false);
        birthdayField.setDisabledTextColor(Color.black);

        JLabel passportLabel = new JLabel("Passport");
        passportLabel.setBounds(10, 270, 200, 30);

        passportField = new JTextField();
        passportField.setBounds(90, 270, 230, 30);
        passportField.setEnabled(false);
        passportField.setDisabledTextColor(Color.black);

        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(10, 320, 200, 30);

        addressField = new JTextField();
        addressField.setBounds(90, 320, 230, 30);
        addressField.setEnabled(false);
        addressField.setDisabledTextColor(Color.black);

        JLabel emailLabel = new JLabel("Email");
        emailLabel.setBounds(10, 370, 200, 30);

        emailField = new JTextField();
        emailField.setBounds(90, 370, 230, 30);
        emailField.setEnabled(false);
        emailField.setDisabledTextColor(Color.black);

        JLabel regDateLabel = new JLabel("Date");
        regDateLabel.setBounds(10, 420, 200, 30);

        regDateField = new JTextField();
        regDateField.setBounds(90, 420, 230, 30);
        regDateField.setEnabled(false);
        regDateField.setDisabledTextColor(Color.black);

        mainPanel.add(mainLabel);

        mainPanel.add(numberLabel);
        mainPanel.add(numberTextField);

        mainPanel.add(searchButton);

        mainPanel.add(nameLabel);
        mainPanel.add(nameField);

        mainPanel.add(surnameLabel);
        mainPanel.add(surnameField);

        mainPanel.add(birthdayLabel);
        mainPanel.add(birthdayField);

        mainPanel.add(passportLabel);
        mainPanel.add(passportField);

        mainPanel.add(addressLabel);
        mainPanel.add(addressField);

        mainPanel.add(emailLabel);
        mainPanel.add(emailField);

        mainPanel.add(regDateLabel);
        mainPanel.add(regDateField);
    }
}
