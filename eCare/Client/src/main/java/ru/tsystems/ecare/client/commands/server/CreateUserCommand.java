package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.UserCommon;

/**
 * Created by julua on 20.08.14.
 */
public class CreateUserCommand extends AbstractCommandClient {
    @Override
    public Object execute(Object... params) {
        ICommand command = new Command();
        command.setName("CreateUser");
        command.setContent((UserCommon)params[0]);
        send(command);
        ICommand response = read();
        return response == null ? null : response.getContent();
    }
}
