package ru.tsystems.ecare.client.commands.login;

import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.manager.ManagerPrivateWindow;

import javax.swing.*;

/**
 * Created by julua on 16.08.14.
 */
public class WindowManagerCommandClient implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame) params[0]).dispose();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ManagerPrivateWindow(null,params[1]);
            }
        });
        return null;
    }
}
