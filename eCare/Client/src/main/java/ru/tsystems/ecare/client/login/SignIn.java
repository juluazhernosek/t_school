package ru.tsystems.ecare.client.login;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.login.WindowClientCommandClient;
import ru.tsystems.ecare.client.commands.login.WindowManagerCommandClient;
import ru.tsystems.ecare.client.commands.server.LoginCommandServer;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 07.08.14.
 */
public class SignIn extends AbstractFrame {


    private JButton signInButton;
    private JTextField loginTextField;
    private JPasswordField passwordPasswordField;


    public SignIn(Callable callable) {
        super(false, callable, null);
    }


    protected void initWindow() {
        setSize(370, 270);
        setTitle("eCare");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands.put("Client", new WindowClientCommandClient());
        commands.put("Manager", new WindowManagerCommandClient());
        commands.put("Login", new LoginCommandServer());
    }

    @Override
    protected void addActionsButton() {
        signInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
//                UserCommon user = new UserCommon();
//                List<ContractCommon> contracts = new ArrayList();
//                List<OptionCommon> options1 = new ArrayList();
//                List<OptionCommon> options2 = new ArrayList();
//                TariffCommon tariff = new TariffCommon();
//                tariff.setName("suchka");
//
//                OptionCommon option1_1= new OptionCommon();
//                option1_1.setName("1_1");
//
//                OptionCommon option1_2= new OptionCommon();
//                option1_2.setName("1_2");
//
//                options1.add(option1_1);
//                options1.add(option1_2);
//
//                options2.add(option1_1);
//
//                ContractCommon contract1 = new ContractCommon();
//                contract1.setTariff(tariff);
//                contract1.setPhoneNumber("111111");
//                contract1.setActive(false);
//                contract1.setSelectedOptions(options1);
//
//                ContractCommon contract2 = new ContractCommon();
//                contract2.setPhoneNumber("222222");
//                contract2.setSelectedOptions(options2);
//                contract2.setActive(false);
//                contract2.setWhoBlocked(true);
//
//                contracts.add(contract1);
//                contracts.add(contract2);
//
//                user.setContracts(contracts);
//                user.setPassword("222");
//                user.setName("www");
                UserCommon user = newTask(new java.util.concurrent.Callable<UserCommon>() {
                    @Override
                    public UserCommon call() throws Exception {
                        return (UserCommon) commands.get("Login").execute(loginTextField.getText(),new String(passwordPasswordField.getPassword()));
                    }
                },8);
                if(user == null) {
                    JOptionPane.showMessageDialog(SignIn.this,"Login or password is not valid");
                    return;
                }
                ICommandClient command = commands.get(user.getRole());
                command.execute(SignIn.this, user);
            }
        });
    }

    protected void addContent() {

        JLabel loginLabel = new JLabel("Login");
        loginLabel.setBounds(20,40,300,30);

        JLabel passwordLabel = new JLabel ("Password");
        passwordLabel.setBounds(20,120,300,30);

        loginTextField = new JTextField();
        loginTextField.setBounds(20,80,300,30);

        passwordPasswordField = new JPasswordField();
        passwordPasswordField.setBounds(20,170,300,30);

        mainPanel.add(loginLabel);
        mainPanel.add(loginTextField);

        mainPanel.add(passwordLabel);
        mainPanel.add (passwordPasswordField);

        signInButton = new JButton("Sign in");
        signInButton.setBounds(130, 220, 110, 30);

        mainPanel.add(signInButton);
    }



}
