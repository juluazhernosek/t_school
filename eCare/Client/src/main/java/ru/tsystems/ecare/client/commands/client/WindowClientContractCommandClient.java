package ru.tsystems.ecare.client.commands.client;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.client.ContractPage;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.util.List;

/**
 * Created by julua on 16.08.14.
 */
public class WindowClientContractCommandClient implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame)params[0]).setEnabled(false);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ContractPage(new Callable() {
                    @Override
                    public void call() {
                        ((JFrame)params[0]).setEnabled(true);
                    }
                }, params[1]);
            }
        });
        return null;
    }
}
