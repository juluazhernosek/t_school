package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 18.08.14.
 */
public class GetAllTariffCommandServer extends AbstractCommandClient{
    @Override
    public Object execute(final Object... params){
        ICommand command = new Command();
        command.setName("GetAllTariffs");
       send(command);
       List<TariffCommon> tariffs = (List<TariffCommon>) read().getContent();
//        List<TariffCommon> tariffs = new ArrayList<>();
//        TariffCommon tariff1 = new TariffCommon();
//        tariff1.setName("1");
//        List<OptionCommon> optionsTariff1 = new ArrayList<>();
//        OptionCommon option1 = new OptionCommon(); option1.setName("qw");
//        OptionCommon option2 = new OptionCommon(); option2.setName("er");
//        optionsTariff1.add(option1); optionsTariff1.add(option2);
//        tariff1.setOptions(optionsTariff1);
//        TariffCommon tariff2 = new TariffCommon();
//        tariff2.setName("2");
//        List<OptionCommon> optionsTariff2 = new ArrayList<>();
//        OptionCommon option3 = new OptionCommon(); option3.setName("ty");
//        OptionCommon option4 = new OptionCommon(); option4.setName("ui");
//        optionsTariff2.add(option3); optionsTariff2.add(option4);
//        tariff2.setOptions(optionsTariff2);
//        TariffCommon tariff3 = new TariffCommon();
//        tariff3.setName("3");
//        List<OptionCommon> optionsTariff3 = new ArrayList<>();
//        OptionCommon option5 = new OptionCommon(); option5.setName("op");
//        OptionCommon option6 = new OptionCommon(); option6.setName("[]");
//        optionsTariff3.add(option5); optionsTariff3.add(option6);
//        tariff3.setOptions(optionsTariff3);
//        tariffs.add(tariff1);
//        tariffs.add(tariff2);
//        tariffs.add(tariff3);
        return tariffs;
    }
}
