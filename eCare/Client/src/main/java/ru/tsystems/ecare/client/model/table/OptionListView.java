package ru.tsystems.ecare.client.model.table;

import ru.tsystems.ecare.common.entities.OptionCommon;

import java.util.AbstractList;
import java.util.List;

/**
 * Created by julua on 23.08.14.
 */
public class OptionListView extends AbstractList<OptionCommon> {

    private List<OptionCommon> options;

    public OptionListView(List<OptionCommon> options) {
        this.options = options;
    }

    @Override
    public OptionCommon get(int index) {
        return options.get(index);
    }

    @Override
    public int size() {
        return options.size();
    }
}
