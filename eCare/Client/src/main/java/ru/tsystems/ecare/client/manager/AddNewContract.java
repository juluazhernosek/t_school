package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.entities.AddedContract;
import ru.tsystems.ecare.client.bucket.entities.CommonBucket;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.IsUniqueNumberContractCommand;
import ru.tsystems.ecare.client.model.table.OptionListModel;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by julua on 12.08.14.
 */
public class AddNewContract extends AbstractFrame {

    private JComboBox<TariffCommon> tariffComboBox;
    private List<TariffCommon> tariffList;
    private JButton addOptionButton;
    private ContractCommon contract;
    private JComboBox<OptionCommon> optionsComboBox;
    private JButton addContractButton;
    private  JButton bucketButton;
    private CommonBucket bucket;
    private JTextField numberTextField;
    private OptionListModel listModel;
    private JList<OptionCommon> optionList;

    public AddNewContract(Callable callable, Object obj) {
        super(false, callable, obj);
    }

    @Override
    protected void initCommands() {
        commands.put("GetAllTariff",new GetAllTariffCommandServer());
        commands.put("IsUniqueNumberContractCommand", new IsUniqueNumberContractCommand());
    }

    @Override
    protected void addActionsButton() {
        addOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(contract.getSelectedOptions() == null)
                    contract.setSelectedOptions(new ArrayList<OptionCommon>());
                OptionCommon selectedOption = optionsComboBox.getItemAt(optionsComboBox.getSelectedIndex());
                if(!contract.getSelectedOptions().contains(selectedOption)) {
                    contract.getSelectedOptions().add(selectedOption);
                    listModel.add(selectedOption);
                }

            }
        });

        tariffComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                TariffCommon tariff = (TariffCommon) itemEvent.getItem();
                if(tariff == null || tariff.getOptions().isEmpty())
                    optionsComboBox.removeAllItems();
                else {
                    optionsComboBox.removeAllItems();
                    for(OptionCommon option : tariff.getOptions()) {
                        optionsComboBox.addItem(option);
                    }
                }
                optionsComboBox.validate();
                optionsComboBox.repaint();
                if(contract.getSelectedOptions() == null)
                    contract.setSelectedOptions(new ArrayList<OptionCommon>());
                contract.getSelectedOptions().clear();
                listModel.clear();
            }
        });

        addContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                boolean unique = newTask(new java.util.concurrent.Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return (Boolean) commands.get("IsUniqueNumberContractCommand").execute(numberTextField.getText());
                    }
                },3);
                boolean isValidNumber = !numberTextField.getText().isEmpty() && isValidContract()
                        && unique;
                if(isValidNumber) {
                    contract.setPhoneNumber(numberTextField.getText());
                    contract.setTariff(tariffComboBox.getItemAt(tariffComboBox.getSelectedIndex()));
                    contract.setUser(bucket.getUser());
                    contract.setActive(true);
                    bucket.getAddedContracts().add(new AddedContract(contract));
                    bucket.getUser().getContracts().add(contract);
                    dispose();
                }
                else {
                    JOptionPane.showMessageDialog(AddNewContract.this, "Number is not valid");
                }

            }
        });

    }

    Pattern pattern = Pattern.compile("\\d+");
    private boolean isValidContract() {
        return pattern.matcher(numberTextField.getText()).matches();
    }

    protected void initWindow() {
        setSize(370, 430);
        setTitle("eCare");
        setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 3,
                Toolkit.getDefaultToolkit().getScreenSize().height / 3);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        bucket = (CommonBucket) obj;

        if(((CommonBucket) obj).getUser().getContracts() == null)
            ((CommonBucket) obj).getUser().setContracts(new ArrayList<ContractCommon>());
        contract = new ContractCommon();
        tariffList = newTask(new java.util.concurrent.Callable<List<TariffCommon>>() {
            @Override
            public List<TariffCommon> call() throws Exception {
                return (List < TariffCommon >) commands.get("GetAllTariff").execute();
            }
        },3);
        if(tariffList == null)
            this.dispose();
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    protected void addContent() {


        JLabel addNewUserLabel = new JLabel("Add new contract:");
        addNewUserLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        addNewUserLabel.setBounds(10, 20, 200, 30);

        JLabel numberLabel = new JLabel("Number:");
        numberLabel.setBounds(10, 70, 200, 30);

        numberTextField = new JTextField();
        numberTextField.setBounds(110, 70, 200, 30);

        JLabel tariffLabel = new JLabel("Choose tariff:");
        tariffLabel.setBounds(10, 110, 200, 30);
        if(tariffList == null || tariffList.isEmpty())
            tariffComboBox = new JComboBox<>();
        else {
            TariffCommon[] tariffs = new TariffCommon[tariffList.size()];
            for(int i = 0; i< tariffs.length; i++) {
                tariffs[i] = tariffList.get(i);
            }
            tariffComboBox = new JComboBox<>(tariffs);
            tariffComboBox.setSelectedIndex(0);
        }
        tariffComboBox.setBounds(120, 110, 200, 30);

        JLabel optionsLabel = new JLabel("Choose option:");
        optionsLabel.setBounds(10, 160, 200, 30);

        List<OptionCommon> options = tariffList == null || tariffList.isEmpty() ? null : tariffList.get(0).getOptions();

        if(options == null || options.isEmpty())
            optionsComboBox = new JComboBox<>();
        else {
            OptionCommon[] optionsArray = new OptionCommon[options.size()];
            for(int i = 0; i< optionsArray.length; i++) {
                optionsArray[i] = options.get(i);
            }
            optionsComboBox = new JComboBox<>(optionsArray);
        }
        optionsComboBox.setBounds(130, 160, 160, 30);

        addOptionButton = new JButton("Add");
        addOptionButton.setBounds(295, 160, 70, 30);

        JLabel countOptionsLabel = new JLabel("Connectivity options:");
        countOptionsLabel.setBounds(10, 200, 300, 30);

        listModel = new OptionListModel(contract.getSelectedOptions());
        optionList = new JList<>(listModel);
        optionList.setLayoutOrientation(JList.VERTICAL);
        JScrollPane scrollPaneList = new JScrollPane(optionList);
        scrollPaneList.setBounds(10, 240, 200, 100);

        addContractButton = new JButton("Add contract");
        addContractButton.setBounds(110, 360, 150, 40);

        bucketButton = new JButton();
        bucketButton.setBounds(550,0,30,30);
        bucketButton.setIcon(new ImageIcon(AbstractFrame.class.getResource("/bucket.png")));



        mainPanel.add(addNewUserLabel);

        mainPanel.add(numberLabel);
        mainPanel.add(numberTextField);


        mainPanel.add(tariffLabel);
        mainPanel.add(tariffComboBox);

        mainPanel.add(optionsLabel);
        mainPanel.add(optionsComboBox);
        mainPanel.add(addOptionButton);

        mainPanel.add(countOptionsLabel);
        mainPanel.add(scrollPaneList);

        mainPanel.add(addContractButton);

        mainPanel.add(bucketButton);
    }
}
