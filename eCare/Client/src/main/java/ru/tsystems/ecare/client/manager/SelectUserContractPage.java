package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.entities.CommonBucket;
import ru.tsystems.ecare.client.bucket.entities.MergedContract;
import ru.tsystems.ecare.client.commands.manager.WindowManagerChangeContractCommand;
import ru.tsystems.ecare.client.commands.manager.WindowManagerOpenBucketCreatingUser;
import ru.tsystems.ecare.client.commands.server.UpdateContractCommand;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * Created by julua on 21.08.14.
 */
public class SelectUserContractPage extends ru.tsystems.ecare.client.client.NumberSelectionPage {

    private CommonBucket bucket;

    public SelectUserContractPage(Callable callable, UserCommon user) {
        super(callable,user);
    }

    protected void initWindow() {
        super.initWindow();
        bucket = new CommonBucket(((UserCommon)obj));
    }
    @Override
    protected void initCommands() {
        commands = new HashMap<>();
        commands.put("ChangeContract", new WindowManagerChangeContractCommand());
        commands.put("OpenBucket", new WindowManagerOpenBucketCreatingUser());
        commands.put("UpdateContract", new UpdateContractCommand());
    }

    @Override
    protected void addActionsButton() {
        numberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(numberComboBox.getSelectedItem() == null)
                    return;
                ContractCommon contract = null;
                for(ContractCommon c : ((UserCommon)obj).getContracts()){
                    if(c.getPhoneNumber().equals(numberComboBox.getSelectedItem()))
                        contract = c;
                }
                commands.get("ChangeContract").execute(SelectUserContractPage.this, contract,bucket);
            }
        });
        bucketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("OpenBucket").execute(SelectUserContractPage.this,bucket);
            }
        });
        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                for(final MergedContract mergedContract : bucket.getMergedContracts()) {
                    ContractCommon contractCommon = newTask(new java.util.concurrent.Callable<ContractCommon>() {
                        @Override
                        public ContractCommon call() throws Exception {
                            return (ContractCommon) commands.get("UpdateContract").execute(mergedContract.getContract());
                        }
                    },4);
                    if(contractCommon == null)
                        JOptionPane.showMessageDialog(SelectUserContractPage.this, "Contract not saved.");
                    else
                        JOptionPane.showMessageDialog(SelectUserContractPage.this,"Contract updated");
                };


            }
        });

    }

}
