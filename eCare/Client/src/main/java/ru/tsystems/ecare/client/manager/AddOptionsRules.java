package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.server.GetOptionsForTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.SetCompatibleOptionCommand;
import ru.tsystems.ecare.client.commands.server.SetIncompatibleOptionCommand;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 13.08.14.
 */
public class AddOptionsRules extends AbstractFrame {

    private JComboBox<OptionCommon> incompatibleOptionComboBox;
    private JComboBox<OptionCommon>  compatibleOptionComboBox;
    private JButton addIncompatible;
    private JComboBox<OptionCommon> mainOptionComboBox;
    private JButton addCompatible;
    private List<OptionCommon> options;
    private JButton addOptionRulesButton;

    public AddOptionsRules(Callable callable,Object obj) {
        super(false, callable, obj);
    }

    protected void initWindow() {
        setSize(480, 290);
        setTitle("eCare");
        options = newTask(new java.util.concurrent.Callable<List<OptionCommon>>() {
            @Override
            public List<OptionCommon> call() throws Exception {
                return (List<OptionCommon>) commands.get("GetOptionsForTariff").execute(obj);
            }
        },3);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(290, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands.put("SetCompatible", new SetCompatibleOptionCommand());
        commands.put("SetIncompatible",new SetIncompatibleOptionCommand());
        commands.put("GetOptionsForTariff",new GetOptionsForTariffCommandServer());
    }

    @Override
    protected void addActionsButton() {
        mainOptionComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                OptionCommon optionCommon = mainOptionComboBox.getItemAt(mainOptionComboBox.getSelectedIndex());
                if(optionCommon == null)
                    return;
                incompatibleOptionComboBox.removeAllItems();
                compatibleOptionComboBox.removeAllItems();
                for(OptionCommon option : options) {
                    if(option.getId() != optionCommon.getId()) {
                        incompatibleOptionComboBox.addItem(option);
                        compatibleOptionComboBox.addItem(option);
                    }
                }
            }
        });
        addCompatible.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final OptionCommon selectedMainOption = mainOptionComboBox.getItemAt(mainOptionComboBox.getSelectedIndex());
                final OptionCommon selectedCompatibleOption = compatibleOptionComboBox.getItemAt(compatibleOptionComboBox.getSelectedIndex());
                if(selectedMainOption == null || selectedCompatibleOption == null)
                    return;
                selectedMainOption.setCompatibleOptions(new ArrayList<OptionCommon>());
                selectedMainOption.getCompatibleOptions().add(selectedCompatibleOption);
                Boolean response = newTask(new java.util.concurrent.Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return (Boolean) commands.get("SetCompatible").execute(selectedMainOption);
                    }
                },5);
                if(response == null || !response) {
                    JOptionPane.showMessageDialog(AddOptionsRules.this,"Cann't add this rule");
                }
                else
                    JOptionPane.showMessageDialog(AddOptionsRules.this,"Rule created");
            }
        });
        addIncompatible.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final OptionCommon selectedMainOption = mainOptionComboBox.getItemAt(mainOptionComboBox.getSelectedIndex());
                final OptionCommon selectedIncompatibleOption = incompatibleOptionComboBox.getItemAt(incompatibleOptionComboBox.getSelectedIndex());
                if(selectedMainOption == null || selectedIncompatibleOption == null)
                    return;
                selectedMainOption.setIncompatibleOptions(new ArrayList<OptionCommon>());
                selectedMainOption.getIncompatibleOptions().add(selectedIncompatibleOption);
                Boolean response = newTask(new java.util.concurrent.Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return (Boolean) commands.get("SetIncompatible").execute(selectedMainOption);
                    }
                },5);
                if(response == null || !response) {
                    JOptionPane.showMessageDialog(AddOptionsRules.this,"Cann't add this rule");
                }
                else
                    JOptionPane.showMessageDialog(AddOptionsRules.this,"Rule created");
            }
        });

        addOptionRulesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        });

    }

    protected void addContent() {


        JLabel addOptionsRules = new JLabel("Add option rules:");
        addOptionsRules.setFont(new Font("Serif", Font.PLAIN, 20));
        addOptionsRules.setBounds(10, 20, 200, 30);

        JLabel tariffLabel = new JLabel("Tariff:");
        tariffLabel.setBounds(10, 70, 200, 30);

        JTextField tariffTextField = new JTextField();
        tariffTextField.setBounds(90, 70, 240, 30);
        tariffTextField.setEnabled(false);
        tariffTextField.setText(((TariffCommon)obj).getName());
        tariffTextField.setDisabledTextColor(Color.black);

        JLabel mainOptionLabel = new JLabel("Option");
        mainOptionLabel.setBounds(10, 110, 200, 30);

        if(options != null)
            mainOptionComboBox = new JComboBox(options.toArray());
        else
            mainOptionComboBox = new JComboBox();
        mainOptionComboBox.setBounds(130, 110, 200, 30);

        JLabel compatibleOptionLabel = new JLabel("Compatible option");
        compatibleOptionLabel.setBounds(10, 150, 200, 30);

        if(options != null)
            compatibleOptionComboBox = new JComboBox(options.toArray());
        else
            compatibleOptionComboBox = new JComboBox();
        compatibleOptionComboBox.setBounds(160, 150, 200, 30);

        JLabel incompatibleOptionLabel = new JLabel("Incompatible option");
        incompatibleOptionLabel.setBounds(10, 190, 200, 30);

        if(options != null)
            incompatibleOptionComboBox = new JComboBox(options.toArray());
        else
            incompatibleOptionComboBox = new JComboBox();
        incompatibleOptionComboBox.setBounds(160, 190, 200, 30);

        addCompatible = new JButton("Add");
        addCompatible.setBounds(370, 150, 90, 30);

        addIncompatible = new JButton("Add");
        addIncompatible.setBounds(370, 190, 90, 30);

        addOptionRulesButton = new JButton("OK");
        addOptionRulesButton.setBounds(140, 230, 150, 30);

        mainPanel.add(addOptionsRules);

        mainPanel.add(tariffLabel);
        mainPanel.add(tariffTextField);
        mainPanel.add(addCompatible);
        mainPanel.add(addIncompatible);

        mainPanel.add(mainOptionLabel);
        mainPanel.add(mainOptionComboBox);

        mainPanel.add(compatibleOptionLabel);
        mainPanel.add(compatibleOptionComboBox);

        mainPanel.add(incompatibleOptionLabel);
        mainPanel.add(incompatibleOptionComboBox);

        mainPanel.add(addOptionRulesButton);
    }
}
