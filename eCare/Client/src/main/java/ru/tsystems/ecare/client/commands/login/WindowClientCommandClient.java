package ru.tsystems.ecare.client.commands.login;

import ru.tsystems.ecare.client.client.ClientPrivateWindow;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;

/**
 * Created by julua on 16.08.14.
 */
public class WindowClientCommandClient implements ICommandClient {
    @Override
    public Object execute(Object ...params) {
        ((JFrame) params[0]).dispose();
        final UserCommon user = (UserCommon) params[1];
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientPrivateWindow(null, user);
            }
        });
        return null;
    }
}
