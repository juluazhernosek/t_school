package ru.tsystems.ecare.client.commands;

/**
 * Created by julua on 16.08.14.
 */
public interface ICommandClient  {
    Object execute(Object ...params);
}
