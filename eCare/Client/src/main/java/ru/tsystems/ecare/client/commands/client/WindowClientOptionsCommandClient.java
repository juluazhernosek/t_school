package ru.tsystems.ecare.client.commands.client;

import ru.tsystems.ecare.client.client.OptionsPage;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.common.entities.ContractCommon;

import javax.swing.*;

/**
 * Created by julua on 16.08.14.
 */
public class WindowClientOptionsCommandClient implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame)params[0]).dispose();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new OptionsPage(null, (ContractCommon) params[1]);
            }
        });
        return null;
    }
}
