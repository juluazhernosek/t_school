package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;

/**
 * Created by julua on 19.08.14.
 */
public class GetAllUsersCommandServer extends AbstractCommandClient {
    @Override
    public Object execute(Object... params) {
        ICommand command = new Command();
        command.setName("GetAllUsers");
        send(command);
        ICommand response = read();
        return response == null ? null : response.getContent();
    }
}
