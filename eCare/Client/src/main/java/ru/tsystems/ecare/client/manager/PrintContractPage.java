package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created by julua on 14.08.14.
 */
public class PrintContractPage extends AbstractFrame {

    public PrintContractPage(Callable callable) {
        super(false, callable, null);;
    }

    protected void initWindow() {
        setSize(370, 210);
        setTitle("eCare");

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {

    }

    @Override
    protected void addActionsButton() {

    }

    protected void addContent() {

        JLabel printContractLabel = new JLabel("Print contract:");
        printContractLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        printContractLabel.setBounds(10, 20, 290, 30);

        JLabel countCopiesLabel = new JLabel("Count contract copies:");
        printContractLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        countCopiesLabel.setBounds(90, 60, 250, 30);

        JTextField countContractCopiesTextField = new JTextField();
        countContractCopiesTextField.setBounds(130, 100, 80, 30);

        JButton printButton = new JButton("Print");
        printButton.setBounds(120, 140, 100, 30);

        mainPanel.add(printContractLabel);

        mainPanel.add(countCopiesLabel);

        mainPanel.add(countContractCopiesTextField);

        mainPanel.add(printButton);
    }
}
