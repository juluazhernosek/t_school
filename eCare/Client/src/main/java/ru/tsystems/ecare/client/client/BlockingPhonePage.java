package ru.tsystems.ecare.client.client;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

/**
 * Created by julua on 10.08.14.
 */
public class BlockingPhonePage extends AbstractFrame {

    private JButton blockButton;
    private JButton unlockButton;

    public BlockingPhonePage(Callable callable, UserCommon user) {
        super(true, callable, user);
    }

    @Override
    protected void initWindow() {
        setSize(470, 120);
        setTitle("eCare");
        width = 420;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    @Override
    protected void initCommands() {
    }

    @Override
    protected void addActionsButton() {


    }


    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(270, 3, 250, 39);
        height += 60;
        mainPanel.add(labelImage);
    }

    protected void addContent() {

        int count_numbers = (obj == null || ((UserCommon)obj).getContracts() == null) ? 0 :(((UserCommon)obj).getContracts()).size();

        JLabel phoneNumberLabel = new JLabel("Phone number:");
        phoneNumberLabel.setBounds(10, 20, 150, 30);
        phoneNumberLabel.setFont(new Font("Serif", Font.PLAIN, 16));

        for( int i = 0; i < count_numbers; i++) {

            final ContractCommon contract = ((UserCommon)obj).getContracts().get(i);

            JTextField numberField = new JTextField();
            numberField.setBounds(10, 70 + 40*i, 190, 20);
            numberField.setEnabled(false);
            numberField.setText(contract.getPhoneNumber());
            numberField.setDisabledTextColor(Color.black);

            blockButton = new JButton("Block");
            blockButton.setBounds(220, 70 + 40*i, 100, 20);

            unlockButton = new JButton("Unlock");
            unlockButton.setBounds(330, 70 + 40*i, 100, 20);

            blockButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    final ContractCommon c = contract;
                    c.setActive(false);
                    c.setWhoBlocked(false);
                    ContractCommon blockedContract = (ContractCommon) newTask(new java.util.concurrent.Callable<Object>() {
                        @Override
                        public Object call() throws Exception {
                            return commands.get("BlockContract").execute(c);
                        }
                    },3);
                    if(blockedContract == null || blockedContract.isActive()) {
                        JOptionPane.showMessageDialog(BlockingPhonePage.this,"Cann't blocked contract","Error",JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    JOptionPane.showMessageDialog(BlockingPhonePage.this,"Contract blocked","Message",JOptionPane.INFORMATION_MESSAGE);
                    blockButton.setEnabled(false);
                    unlockButton.setEnabled(true);
                    dispose();
                }
            });

            unlockButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    final ContractCommon c = contract;
                    c.setActive(true);
                    ContractCommon blockedContract = (ContractCommon) newTask(new java.util.concurrent.Callable<Object>() {
                        @Override
                        public Object call() throws Exception {
                            return commands.get("BlockContract").execute(c);
                        }
                    },3);
                    if(blockedContract == null || !blockedContract.isActive()) {
                        JOptionPane.showMessageDialog(BlockingPhonePage.this,"Cann't unblocked contract","Error",JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    JOptionPane.showMessageDialog(BlockingPhonePage.this,"Contract unlocked","Message",JOptionPane.INFORMATION_MESSAGE);
                    blockButton.setEnabled(true);
                    unlockButton.setEnabled(false);
                    dispose();
                }
            });

            if(!contract.isActive()){
                if(contract.isWhoBlocked()){
                    unlockButton.setEnabled(false);
                }
                blockButton.setEnabled(false);
            }

            height += 40;

            mainPanel.add(numberField);
            mainPanel.add(blockButton);
            mainPanel.add(unlockButton);
        }

        mainPanel.add(phoneNumberLabel);

    }
}
