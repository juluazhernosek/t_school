package ru.tsystems.ecare.client.client;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.client.WindowClientOptionsCommandClient;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by julua on 11.08.14.
 */
public class NumberSelectionPage extends AbstractFrame {

    protected JButton numberButton;
    protected JButton bucketButton;
    protected  JButton acceptButton;
    protected JComboBox<ContractCommon> numberComboBox;

    public NumberSelectionPage(Callable callable, UserCommon user) {
        super(false, callable, user);
    }

    protected void initWindow() {
        setSize(370, 160);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(170, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("SeeOptions", new WindowClientOptionsCommandClient());
    }

    @Override
    protected void addActionsButton() {
        numberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ContractCommon contract = null;
                for(ContractCommon c : ((UserCommon)obj).getContracts()){
                    if(c.getPhoneNumber().equals(numberComboBox.getSelectedItem()))
                        contract = c;
                }
                commands.get("SeeOptions").execute(NumberSelectionPage.this, contract);
            }
        });
    }

    protected void addContent() {
        List<String> numbers = new ArrayList<>();
        for(ContractCommon contract : ((UserCommon)obj).getContracts()){
            numbers.add(contract.getPhoneNumber());
        }

        JLabel selectNumberLabel = new JLabel("Select number:");
        selectNumberLabel.setBounds(20, 30, 200, 30);

        numberComboBox = new JComboBox(numbers.toArray());
        numberComboBox.setBounds(20, 70, 200, 20);

        numberButton = new JButton("Select");
        numberButton.setBounds(70, 100, 100, 30);

        acceptButton = new JButton("Accept");
        acceptButton.setBounds(190, 100, 100, 30);

        numberButton.setEnabled(!((UserCommon)obj).getContracts().isEmpty());
        acceptButton.setEnabled(!((UserCommon)obj).getContracts().isEmpty());

        bucketButton = new JButton();
        bucketButton.setBounds(340,0,30,30);
        bucketButton.setIcon(new ImageIcon(AbstractFrame.class.getResource("/bucket.png")));

        mainPanel.add(selectNumberLabel);
        mainPanel.add(numberComboBox);
        mainPanel.add(numberButton);
        mainPanel.add(bucketButton);
        mainPanel.add(acceptButton);
    }
}
