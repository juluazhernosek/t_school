package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;

/**
 * Created by julua on 24.08.14.
 */
public class RemoveOptionCommand extends AbstractCommandClient {
    @Override
    public Object execute(Object... params) {
        ICommand command = new Command();
        command.setContent((java.io.Serializable) params[0]);
        command.setName("RemoveOption");
        send(command);
        ICommand response = read();
        return response == null ? null : response.getContent();
    }
}
