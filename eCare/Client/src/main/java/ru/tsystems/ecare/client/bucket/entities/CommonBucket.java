package ru.tsystems.ecare.client.bucket.entities;

import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 21.08.14.
 */
public class CommonBucket {

    private UserCommon user;

    private List<AddedContract> addedContracts;
    private List<MergedContract> mergedContracts;
    private List<RemovedContract> removedContracts;

    public CommonBucket(UserCommon user) {
        this.user = user;
        addedContracts = new ArrayList<>();
        mergedContracts = new ArrayList<>();
        removedContracts = new ArrayList<>();
    }

    public AddedContract addedContainsRemoved(RemovedContract contract) {
        for(AddedContract addedContract : addedContracts) {
            if(addedContract.getContract().equals(contract.getContract()))
                return addedContract;
        }
        return null;
    }

    public MergedContract getMergeByContract(ContractCommon contractCommon) {
        for(MergedContract mergedContract : mergedContracts) {
            if(mergedContract.getContract().equals(contractCommon))
                return mergedContract;
        }
        return null;
    }



    public List<AddedContract> getAddedContracts() {
        return addedContracts;
    }

    public List<MergedContract> getMergedContracts() {
        return mergedContracts;
    }

    public List<RemovedContract> getRemovedContracts() {
        return removedContracts;
    }

    public UserCommon getUser() {
        return user;
    }
}
