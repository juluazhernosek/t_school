package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.server.AddOptionCommand;
import ru.tsystems.ecare.client.commands.server.GetOptionsForTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.IsUniqueOptionCommand;
import ru.tsystems.ecare.client.commands.server.RemoveOptionCommand;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.swing.*;
import javax.swing.text.html.Option;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Created by julua on 13.08.14.
 */
public class AddOptionPage extends AbstractFrame {

    private JTextField nameOptionTextField;
    private JTextField priceOptionTextField;
    private JTextField costOptionTextField;
    private JButton addOptionButton;
    private TariffCommon tariff;
    private JComboBox<OptionCommon> optionsComboBox;
    private JButton deleteOptionButton;
    private java.util.List<OptionCommon> options;

    public AddOptionPage(Callable callable, Object tariff) {
        super(false, callable, tariff);
    }


    protected void initWindow() {
        setSize(370, 330);
        setTitle("eCare");
        tariff = ((TariffCommon)obj);
        options = (List<OptionCommon>) newTask(new java.util.concurrent.Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return commands.get("GetOptions").execute(tariff);
            }
        },3);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands.put("GetOptions",new GetOptionsForTariffCommandServer());
        commands.put("IsUniqueOption",new IsUniqueOptionCommand());
        commands.put("AddOption",new AddOptionCommand());
        commands.put("RemoveOption", new RemoveOptionCommand());
    }

    @Override
    protected void addActionsButton() {
        addOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final OptionCommon optionCommon = new OptionCommon();
                optionCommon.setTariff(tariff);
                optionCommon.setName(nameOptionTextField.getText());
                try {
                    optionCommon.setPrice(Double.parseDouble(priceOptionTextField.getText()));
                    optionCommon.setCost(Double.parseDouble(costOptionTextField.getText()));
                }catch (Exception e) {
                    JOptionPane.showMessageDialog(AddOptionPage.this,"Wrong price/cost","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Boolean unique = (Boolean) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("IsUniqueOption").execute(optionCommon);
                    }
                },3);
                if(unique == null || !unique) {
                    JOptionPane.showMessageDialog(AddOptionPage.this,"Option already exists","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                OptionCommon response = (OptionCommon) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("AddOption").execute(optionCommon);
                    }
                },3);
                if(response == null) {
                    JOptionPane.showMessageDialog(AddOptionPage.this,"Cann't save option","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                else
                    JOptionPane.showMessageDialog(AddOptionPage.this,"Option saved","Message",JOptionPane.INFORMATION_MESSAGE);
                nameOptionTextField.setText("");priceOptionTextField.setText("");costOptionTextField.setText("");
                optionsComboBox.addItem(response);
                optionsComboBox.validate();
                optionsComboBox.repaint();
            }
        });
        deleteOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int index = optionsComboBox.getSelectedIndex();
                final OptionCommon removeTariff = optionsComboBox.getItemAt(index);
                Boolean response = (Boolean) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("RemoveOption").execute(removeTariff.getId());
                    }
                },3);
                if(response == null || !response) {
                    JOptionPane.showMessageDialog(AddOptionPage.this,"Cann't remove option","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                else
                    JOptionPane.showMessageDialog(AddOptionPage.this, "Option removed", "Message", JOptionPane.INFORMATION_MESSAGE);
                nameOptionTextField.setText("");priceOptionTextField.setText("");costOptionTextField.setText("");
                optionsComboBox.removeItem(removeTariff);
                optionsComboBox.validate();
                optionsComboBox.repaint();
            }
        });
    }

    protected void addContent() {

        JLabel addOptionLabel = new JLabel("Add option:");
        addOptionLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        addOptionLabel.setBounds(30, 10, 200, 30);

        JLabel nameOptionLabel = new JLabel("Name");
        nameOptionLabel.setBounds(10, 60, 200, 30);

        nameOptionTextField = new JTextField();
        nameOptionTextField.setBounds(150, 60, 200, 30);

        JLabel priceOptionLabel = new JLabel("Price");
        priceOptionLabel.setBounds(10, 100, 200, 30);

        priceOptionTextField = new JTextField();
        priceOptionTextField.setBounds(150, 100, 200, 30);

        JLabel costOptionLabel = new JLabel("Cost");
        costOptionLabel.setBounds(10, 140, 200, 30);

        costOptionTextField = new JTextField();
        costOptionTextField.setBounds(150, 140, 200, 30);

        addOptionButton = new JButton("Add option");
        addOptionButton.setBounds(110, 180, 150, 30);

        JLabel deleteTariffLabel = new JLabel("Delete option:");
        deleteTariffLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        deleteTariffLabel.setBounds(20, 220, 200, 30);

        if(options == null)
            optionsComboBox = new JComboBox();
        else
            optionsComboBox = new JComboBox(options.toArray());
        optionsComboBox.setBounds(10, 260, 190, 30);

        deleteOptionButton = new JButton("Delete option");
        deleteOptionButton.setBounds(210, 260, 150, 30);

        mainPanel.add(addOptionLabel);

        mainPanel.add(nameOptionLabel);
        mainPanel.add(nameOptionTextField);

        mainPanel.add(priceOptionLabel);
        mainPanel.add(priceOptionTextField);

        mainPanel.add(costOptionLabel);
        mainPanel.add(costOptionTextField);

        mainPanel.add(addOptionButton);

        mainPanel.add(deleteTariffLabel);

        mainPanel.add(optionsComboBox);
        mainPanel.add(deleteOptionButton);
    }
}
