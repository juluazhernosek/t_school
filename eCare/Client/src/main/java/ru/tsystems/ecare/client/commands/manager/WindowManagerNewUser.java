package ru.tsystems.ecare.client.commands.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.manager.AddNewUserPage;

import javax.swing.*;

/**
 * Created by julua on 16.08.14.
 */
public class WindowManagerNewUser implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame)params[0]).setEnabled(false);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AddNewUserPage(new Callable() {
                    @Override
                    public void call() {
                        ((JFrame)params[0]).setEnabled(true);
                    }
                });
            }
        });
        return null;
    }
}
