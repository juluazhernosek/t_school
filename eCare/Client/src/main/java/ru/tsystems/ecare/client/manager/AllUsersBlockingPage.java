package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.manager.WindowManagerSeeBlocking;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * Created by julua on 16.08.14.
 */
public class AllUsersBlockingPage extends AllUsersPage {
    public AllUsersBlockingPage(Callable callable, Object obj) {
        super(callable,obj);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("SeeBlocking", new WindowManagerSeeBlocking());
    }

    @Override
    protected void addActionsButton() {
        moreInfoButton.setText("Select");
        moreInfoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getSelectedItem().setContracts(getAllUnblockingContracts(getSelectedItem()));
                commands.get("SeeBlocking").execute(AllUsersBlockingPage.this,getSelectedItem());
            }
        });
    }
}
