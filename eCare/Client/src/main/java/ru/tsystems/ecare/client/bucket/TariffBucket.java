package ru.tsystems.ecare.client.bucket;

import ru.tsystems.ecare.client.commands.server.GetCompatibleOptions;
import ru.tsystems.ecare.client.commands.server.GetIncompatibleOptions;
import ru.tsystems.ecare.common.entities.OptionCommon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 24.08.14.
 */
public class TariffBucket {
    private List<OptionCommon>  options;

    public TariffBucket() {
        options = new ArrayList<>();
    }

    public STATE_OPTION isCan(OptionCommon optionCommon) {
        for(OptionCommon option : options) {
            if(option.getId() == optionCommon.getId())
                return STATE_OPTION.ALREADY_EXISTS;
            if(option.getIncompatibleOptions() != null) {
                for(OptionCommon incOption : option.getIncompatibleOptions() ) {
                    if(optionCommon.getId() == incOption.getId())
                        return STATE_OPTION.NOT_COMPATIBLE;
                }
            }
            List<OptionCommon> optionsCompatible = (List<OptionCommon>) new GetCompatibleOptions().execute(optionCommon);
            optionCommon.setCompatibleOptions(optionsCompatible);
            if(optionsCompatible != null)
                for(OptionCommon cOption : optionsCompatible) {
                    for(OptionCommon opt : options) {
                        if(opt.getId() == cOption.getId())
                            return STATE_OPTION.COMPATIBLE;
                    }
                }
        }
        List<OptionCommon> optionsIncompatible = (List<OptionCommon>) new GetIncompatibleOptions().execute(optionCommon);
        optionCommon.setIncompatibleOptions(optionsIncompatible);
        return STATE_OPTION.SUCCESS;
    }

    public void addOption(OptionCommon optionCommon) {
        options.add(optionCommon);
    }
    public void removeOption(OptionCommon optionCommon) {
        options.remove(optionCommon);
    }

    public static enum STATE_OPTION {
        SUCCESS, NOT_COMPATIBLE, ALREADY_EXISTS,COMPATIBLE
    }
}
