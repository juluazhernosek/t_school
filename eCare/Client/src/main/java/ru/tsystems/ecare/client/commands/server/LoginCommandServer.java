package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.UserCommon;

/**
 * Created by julua on 17.08.14.
 */
public class LoginCommandServer extends AbstractCommandClient {

    @Override
    public Object execute(Object... params) {
        String login = (String) params[0];
        String password = (String) params[1];
        ICommand command = new Command();
        UserCommon user = new UserCommon();
        user.setEmail(login);
        user.setPassword(password);
        command.setName("SignIn");
        command.setContent(user);
        send(command);
        ICommand response = read();
        return response.getContent();

    }
}
