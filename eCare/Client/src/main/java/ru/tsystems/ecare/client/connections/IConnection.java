package ru.tsystems.ecare.client.connections;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by julua on 17.08.14.
 */
public interface IConnection {
    ObjectInputStream getObjectInputStream();
    ObjectOutputStream getObjectOutputStream();
    void close();
}
