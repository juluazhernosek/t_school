package ru.tsystems.ecare.client.bucket.entities;


import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 21.08.14.
 */
public class MergedContract {
    private List<OptionCommon> removedOptions;
    private List<OptionCommon> addedOptions;
    private TariffCommon tariff;
    private ContractCommon contract;

    public MergedContract(ContractCommon contract) {
        this.contract = contract;
        this.tariff = contract.getTariff();
        removedOptions = new ArrayList<>();
        addedOptions = new ArrayList<>();
    }


    public MergedContract() {
        this(null);
    }

    public List<OptionCommon> getRemovedOptions() {
        return removedOptions;
    }

    public List<OptionCommon> getAddedOptions() {
        return addedOptions;
    }

    public TariffCommon getTariff() {
        return tariff;
    }

    public void setTariff(TariffCommon tariff) {
        this.tariff = tariff;
    }

    public ContractCommon getContract() {
        return contract;
    }

    public void setContract(ContractCommon contract) {
        this.contract = contract;
    }
}
