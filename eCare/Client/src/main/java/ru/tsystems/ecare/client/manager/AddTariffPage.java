package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.server.AddTariffCommand;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.IsUniqueTariffName;
import ru.tsystems.ecare.client.commands.server.RemoveTariffCommand;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by julua on 12.08.14.
 */
public class AddTariffPage extends AbstractFrame {

    private java.util.List<TariffCommon> tariffs;

    public AddTariffPage(Callable callable) {
        super(false, callable, null);
    }
    private JTextField nameTariffTextField;
    private JTextField priceTariffTextField;
    private JButton addTariffButton;
    private JButton deleteTariffButton;
    private JButton okButton;
    private JComboBox<TariffCommon> tariffComboBox;


    protected void initWindow() {
        setSize(370, 330);
        setTitle("eCare");

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        tariffs = (java.util.List<TariffCommon>) commands.get("GetAllTariff").execute();
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<>();
        commands.put("GetAllTariff", new GetAllTariffCommandServer());
        commands.put("AddTariff",new AddTariffCommand());
        commands.put("RemoveTariff", new RemoveTariffCommand());
        commands.put("IsUniqueName", new IsUniqueTariffName());
    }

    @Override
    protected void addActionsButton() {
        addTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final TariffCommon tariffCommon = new TariffCommon();
                tariffCommon.setName(nameTariffTextField.getText());
                try {
                    tariffCommon.setPrice(Double.parseDouble(priceTariffTextField.getText()));
                }catch (Exception e) {
                    JOptionPane.showMessageDialog(AddTariffPage.this,"Wrong price","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Boolean unique = (Boolean) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("IsUniqueName").execute(tariffCommon.getName());
                    }
                },3);
                if( !isValidTariffName()) {
                    JOptionPane.showMessageDialog(AddTariffPage.this, "Uncorrect format");
                    return;
                }
                if(unique == null || !unique){
                    JOptionPane.showMessageDialog(AddTariffPage.this, "Tariff already exists", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                TariffCommon response = (TariffCommon) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("AddTariff").execute(tariffCommon);
                    }
                },3);
                if(response == null) {
                    JOptionPane.showMessageDialog(AddTariffPage.this,"Cann't save tariff","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                else
                    JOptionPane.showMessageDialog(AddTariffPage.this,"Tariff saved","Message",JOptionPane.INFORMATION_MESSAGE);
                nameTariffTextField.setText(""); priceTariffTextField.setText("");
                tariffComboBox.addItem(response);
                tariffComboBox.validate();
                tariffComboBox.repaint();
            }
        });

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        });

        deleteTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int index = tariffComboBox.getSelectedIndex();
                final TariffCommon removeTariff = tariffComboBox.getItemAt(index);
                Boolean response = (Boolean) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("RemoveTariff").execute(removeTariff.getId());
                    }
                },3);
                if(response == null && !response) {
                    JOptionPane.showMessageDialog(AddTariffPage.this,"Cann't remove tariff","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                else
                    JOptionPane.showMessageDialog(AddTariffPage.this, "Tariff removed", "Message", JOptionPane.INFORMATION_MESSAGE);
                nameTariffTextField.setText(""); priceTariffTextField.setText("");
                tariffComboBox.removeItem(removeTariff);
                tariffComboBox.validate();
                tariffComboBox.repaint();
            }
        });
    }

    protected void addContent() {
        JLabel addTariffLabel = new JLabel("Add tariff:");
        addTariffLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        addTariffLabel.setBounds(30, 10, 200, 30);

        JLabel nameTariffLabel = new JLabel("Name");
        nameTariffLabel.setBounds(10, 60, 200, 30);

        nameTariffTextField = new JTextField();
        nameTariffTextField.setBounds(150, 60, 200, 30);

        JLabel priceTariffLabel = new JLabel("Price");
        priceTariffLabel.setBounds(10, 100, 200, 30);

        priceTariffTextField = new JTextField();
        priceTariffTextField.setBounds(150, 100, 200, 30);

        addTariffButton = new JButton("Add tariff");
        addTariffButton.setBounds(110, 140, 150, 30);

        JLabel deleteTariffLabel = new JLabel("Delete tariff:");
        deleteTariffLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        deleteTariffLabel.setBounds(20, 190, 200, 30);

        tariffComboBox = new JComboBox(tariffs.toArray());
//        tariffComboBox.setSelectedIndex();
        tariffComboBox.setBounds(10, 230, 190, 30);

        deleteTariffButton = new JButton("Delete tariff");
        deleteTariffButton.setBounds(210, 230, 150, 30);

        okButton = new JButton("Ok");
        okButton.setBounds(130, 270, 100, 30);

        mainPanel.add(addTariffLabel);

        mainPanel.add(nameTariffLabel);
        mainPanel.add(nameTariffTextField);

        mainPanel.add(priceTariffLabel);
        mainPanel.add(priceTariffTextField);

        mainPanel.add(addTariffButton);

        mainPanel.add(deleteTariffLabel);

        mainPanel.add(tariffComboBox);
        mainPanel.add(deleteTariffButton);

        mainPanel.add(okButton);
    }


    private final Pattern regTariffName = Pattern.compile("\\w{1,20}");
    private boolean isValidTariffName(){
        return regTariffName.matcher(nameTariffTextField.getText()).matches();
    }
}
