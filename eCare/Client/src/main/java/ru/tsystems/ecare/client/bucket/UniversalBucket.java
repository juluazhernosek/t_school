package ru.tsystems.ecare.client.bucket;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;


import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created by julua on 21.08.14.
 */
public class UniversalBucket extends AbstractFrame {

    public UniversalBucket(Callable callable, Object newObj, Object oldObj) {
        super(true, callable, newObj, oldObj);
    }

    protected void initWindow() {
        setSize(670, 200);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(490, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
    }

    @Override
    protected void addActionsButton() {
    }

    protected void addContent() {

        JLabel beforeLabel = new JLabel("Before:");
        beforeLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        beforeLabel.setBounds(20, 20, 200, 30);

        JLabel afterLabel = new JLabel("After:");
        afterLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        afterLabel.setBounds(400, 20, 200, 30);

//        for (int m = 0; m < countContracts; m++) {
//
//            Contract contract = ((User) obj).getContracts().get(m);
//
//            JLabel contractLabel = new JLabel((m + 1) + ". Contract:");
//            contractLabel.setBounds(10, 50 + 150 * m + m * countOptions * 40, 290, 30);
//
//            JLabel numberLabel = new JLabel("Number:");
//            numberLabel.setBounds(10, 80 + 150 * m + m * countOptions * 40, 290, 30);
//
//            JTextField numberField = new JTextField();
//            numberField.setBounds(100, 80 + 150 * m + m * countOptions * 40, 290, 30);
//            numberField.setText(contract.getPhoneNumber());
//
//            JLabel tariffLabel = new JLabel("Tariff:");
//            tariffLabel.setBounds(10, 120 + 150 * m + m * countOptions * 40, 290, 30);
//
//            JTextField tariffField = new JTextField();
//            tariffField.setBounds(100, 120 + 150 * m + m * countOptions * 40, 290, 30);
//            tariffField.setText(contract.getTariff() == null ? "" : contract.getTariff().getName());
//
//            JLabel optionsLabel = new JLabel("Options:");
//            optionsLabel.setBounds(10, 160 + 150 * m + m * countOptions * 40, 290, 30);
//
//            int countOptionsContract = ((User) obj).getContracts().get(m).getSelectedOptions().size();
//            height += 150 + countOptions * 40;
//            for (int i = 0; i < countOptionsContract; i++) {
//                JTextField optionsField = new JTextField();
//                optionsField.setBounds(10, (200 + i * 40) + 150 * m + m * countOptions * 40, 290, 30);
//                optionsField.setText(contract.getSelectedOptions().get(i).toString());
//                mainPanel.add(optionsField);
//            }
//        }

        mainPanel.add(beforeLabel);
        mainPanel.add(afterLabel);


        JLabel deleteOptionLabel = new JLabel("Delete options:");
        deleteOptionLabel.setBounds(10, 180, 200, 30);

        JTextField deleteOptionField = new JTextField();
        deleteOptionField.setBounds(10, 220, 200, 30);
        deleteOptionField.setEnabled(false);
        deleteOptionField.setDisabledTextColor(Color.black);
        mainPanel.add(deleteOptionLabel);
        mainPanel.add(deleteOptionField);

//        mainPanel.add(changesLabel);

        width = 350;
    }
}
