package ru.tsystems.ecare.client.commands.client;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.client.BlockingPhonePage;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;

/**
 * Created by julua on 16.08.14.
 */
public class WindowClientBlockNumber implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame)params[0]).setEnabled(false);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new BlockingPhonePage(new Callable() {
                    @Override
                    public void call() {
                        ((JFrame)params[0]).setEnabled(true);
                    }
                }, (UserCommon) params[1]);
            }
        });
        return null;
    }
}
