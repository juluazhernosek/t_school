package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.manager.WindowManagerNewOption;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created by julua on 13.08.14.
 */
public class TariffSelectionPage extends AbstractFrame {

    private java.util.List<TariffCommon> tariffs;

    protected JComboBox<TariffCommon> numberComboBox;
    protected JButton numberButton;
    public TariffSelectionPage(Callable callable) {
        super(false, callable, null);
    }

    protected void initWindow() {
        setSize(370, 180);
        setTitle("eCare");

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        tariffs = (List<TariffCommon>) commands.get("GetAllTariff").execute();
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("NewOption", new WindowManagerNewOption());
        commands.put("GetAllTariff", new GetAllTariffCommandServer());
    }

    @Override
    protected void addActionsButton() {
        numberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TariffCommon tariff = numberComboBox.getItemAt(numberComboBox.getSelectedIndex());
                commands.get("NewOption").execute(TariffSelectionPage.this,tariff);
            }
        });
    }

    protected void addContent() {

        JLabel selectNumberLabel = new JLabel("Select tariff:");
        selectNumberLabel.setBounds(20, 30, 200, 30);

        numberComboBox = new JComboBox(tariffs.toArray());
        numberComboBox.setBounds(20, 70, 200, 20);

        numberButton = new JButton("Ok");
        numberButton.setBounds(140, 100, 100, 30);

        mainPanel.add(selectNumberLabel);
        mainPanel.add(numberComboBox);
        mainPanel.add(numberButton);
    }
}
