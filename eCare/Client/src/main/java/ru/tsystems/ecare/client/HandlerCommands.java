package ru.tsystems.ecare.client;

import org.apache.log4j.Logger;
import ru.tsystems.ecare.client.connections.IConnection;
import ru.tsystems.ecare.client.connections.SocketConnection;
import ru.tsystems.ecare.common.commands.ICommand;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by julua on 17.08.14.
 */
public class HandlerCommands implements IHandlerObjects {

    private static final String HOST = "localhost";
    private static final int PORT = 2222;
    private IConnection connection;

    private static HandlerCommands instance;

    private final static Logger logger = Logger.getLogger(HandlerCommands.class);

    public static HandlerCommands getInstance() {
        return instance == null ? (instance = new HandlerCommands()) : instance;
    }

    private HandlerCommands() {

    }

    @Override
    public ICommand getResponse() {
        ICommand response = null;
        try {
            response = (ICommand) getConnection().getObjectInputStream().readObject();
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            logger.error("Cann't receive object. Message : " + e.getMessage());
        }
        finally {
            closeConnection();
        }
        return response;
    }

    @Override
    public void sendRequest(ICommand command) {
        ObjectOutputStream outputStream = getConnection().getObjectOutputStream();
        try {
            outputStream.writeObject(command);
            outputStream.flush();
        } catch (IOException e) {
            logger.error("Cann't send object. Message : " + e.getMessage());
        }
    }

    private IConnection getConnection() {
        return connection == null ? (connection = new SocketConnection(HOST,PORT)) : connection;
    }

    private void closeConnection() {
        connection.close();
        connection = null;
    }
}
