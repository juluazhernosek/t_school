package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.manager.WindowManagerSeeContracts;
import ru.tsystems.ecare.client.commands.server.GetContractsAllCommand;
import ru.tsystems.ecare.client.commands.server.GetContractsForUserCommand;
import ru.tsystems.ecare.client.model.table.AllUsersTableModel;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created by julua on 12.08.14.
 */
public class AllUsersPage extends AbstractFrame {

    protected JButton moreInfoButton;
    private JTable usersTable;

    public AllUsersPage(Callable callable, Object obj) {
        super(false, callable, obj);
    }


    protected void initWindow() {
        setSize(700, 450);
        setTitle("eCare");

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }


    protected List<ContractCommon> getAllContracts(final UserCommon user) {
        List<ContractCommon> contracts = (List<ContractCommon>) newTask(new java.util.concurrent.Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return new GetContractsForUserCommand().execute(user);
            }
        }, 3);
        return contracts;
    }

    protected List<ContractCommon> getAllUnblockingContracts(final UserCommon user) {
        List<ContractCommon> contracts = (List<ContractCommon>) newTask(new java.util.concurrent.Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return new GetContractsAllCommand().execute(user);
            }
        }, 3);
        return contracts;
    }



    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(520, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("SeeContracts", new WindowManagerSeeContracts());
    }

    @Override
    protected void addActionsButton() {
        moreInfoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getSelectedItem().setContracts(getAllContracts(getSelectedItem()));
                commands.get("SeeContracts").execute(AllUsersPage.this,getSelectedItem());
            }
        });
    }

    protected void addContent() {

        JLabel allUsersLabel = new JLabel("All users:");
        allUsersLabel.setFont(new Font("Serif", Font.PLAIN, 25));
        allUsersLabel.setBounds(20,20,300,30);

        usersTable = new JTable(new AllUsersTableModel((List<UserCommon>) obj));
        usersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane pane = new JScrollPane(usersTable);
        usersTable.setPreferredScrollableViewportSize(new Dimension(650,300));
        pane.setBounds(20, 70, 650, 300);

        moreInfoButton = new JButton("More Info");
        moreInfoButton.setBounds(520, 380, 160, 30);

        mainPanel.add(moreInfoButton);
        mainPanel.add(allUsersLabel);
        mainPanel.add(pane);
    }

    protected UserCommon getSelectedItem() {
        return ((AllUsersTableModel)usersTable.getModel()).getSelectedItem(usersTable.getSelectedRow());
    }
}
