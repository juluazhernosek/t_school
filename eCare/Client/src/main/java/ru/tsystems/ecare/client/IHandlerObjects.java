package ru.tsystems.ecare.client;

import ru.tsystems.ecare.common.commands.ICommand;

/**
 * Created by julua on 17.08.14.
 */
public interface IHandlerObjects {
    ICommand getResponse();
    void sendRequest(ICommand command);
}
