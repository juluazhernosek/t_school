package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;

/**
 * Created by julua on 20.08.14.
 */
public class IsUniqueEmailCommand extends AbstractCommandClient {
    @Override
    public Object execute(Object... params) {
        ICommand command = new Command();
        command.setName("IsUniqueEmailCommand");
        command.setContent((String) params[0]);
        send(command);
        ICommand response = read();
        return response == null ? null : response.getContent();
    }
}
