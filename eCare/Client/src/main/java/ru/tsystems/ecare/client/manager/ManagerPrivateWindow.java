package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.manager.*;
import ru.tsystems.ecare.client.commands.server.GetAllUsersCommandServer;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Created by julua on 11.08.14.
 */
public class ManagerPrivateWindow extends AbstractFrame {

    private JButton addNewClientButton;
    private JButton seeAllUsersButton;
    private JButton blockingUsersButton;
    private JButton changeContractButton;
    private JButton searchUserButton;
    private JButton addTariffButton;
    private JButton addOptionsButton;
    private JButton addRulesOptionsButton;
    private JButton addContractButton;
    private JButton printContractsButton;


    public ManagerPrivateWindow(Callable callable, Object obj) {
        super(false, callable, obj);;
    }

    protected void initWindow() {
        setSize(650, 540);
        setTitle("eCare");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private List<UserCommon> getAllUsers() {
        List<UserCommon> users = (List<UserCommon>) commands.get("GetAllUser").execute();
        return users;
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(450, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("NewUser", new WindowManagerNewUser());
        commands.put("SeeAllUsers", new WindowManagerSeeAllUsers());
        commands.put("BlockUser", new WindowManagerSeeAllUsersBlocking());
        commands.put("ChangeUserContract", new WindowManagerSeeAllUsersChangeContract());
        commands.put("SearchUser", new WindowManagerSearchUser());
        commands.put("AddTariff", new WindowManagerAddTariff());
        commands.put("AddOption", new WindowManagerSelectTariff());
        commands.put("AddRulesOption", new WindowManagerSelectTariffForRules());
        commands.put("AddContract", new WindowManagerSeeAllUsersAddContract());
        commands.put("GetAllUser",new GetAllUsersCommandServer());
    }

    @Override
    protected void addActionsButton() {
        addNewClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("NewUser").execute(ManagerPrivateWindow.this);
            }
        });
        seeAllUsersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("SeeAllUsers").execute(ManagerPrivateWindow.this,getAllUsers());
            }
        });
        blockingUsersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("BlockUser").execute(ManagerPrivateWindow.this,getAllUsers());
            }
        });
        changeContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("ChangeUserContract").execute(ManagerPrivateWindow.this,getAllUsers());
            }
        });
        searchUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("SearchUser").execute(ManagerPrivateWindow.this);
            }
        });
        addTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("AddTariff").execute(ManagerPrivateWindow.this);
            }
        });
        addOptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("AddOption").execute(ManagerPrivateWindow.this);
            }
        });
        addRulesOptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("AddRulesOption").execute(ManagerPrivateWindow.this);
            }
        });
        addContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("AddContract").execute(ManagerPrivateWindow.this,getAllUsers());
            }
        });
    }

    protected void addContent() {

        JLabel welcomeLabel = new JLabel("Welcome, our manager!");
        welcomeLabel.setFont(new Font("Serif", Font.PLAIN, 25));
        welcomeLabel.setBounds(70, 3, 330, 50);

        JSeparator mainSeparator = new JSeparator(SwingConstants.VERTICAL);
        mainSeparator.setBackground(new Color(190, 190, 190));
        mainSeparator.setBounds(400, 80, 40, 450);

        JLabel privateInformationLabel = new JLabel("Your private information");
        privateInformationLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        privateInformationLabel.setBounds(80, 70, 300, 30);

        JLabel chooseActionLabel = new JLabel("Choose Action");
        chooseActionLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        chooseActionLabel.setBounds(460, 70, 300, 30);

        JLabel nameLabel = new JLabel("Name");
        nameLabel.setBounds(10, 120, 200, 30);

        JTextField nameField = new JTextField();
        nameField.setBounds(90, 120, 300, 30);
        nameField.setEnabled(false);
        nameField.setText(((UserCommon)obj).getName() == null ? "" : ((UserCommon)obj).getName());
        nameField.setDisabledTextColor(Color.black);

        JLabel surnameLabel = new JLabel("Surname");
        surnameLabel.setBounds(10, 170, 200, 30);

        JTextField surnameField = new JTextField();
        surnameField.setBounds(90, 170, 300, 30);
        surnameField.setEnabled(false);
        surnameField.setText(((UserCommon)obj).getSurname() == null ? "" : ((UserCommon)obj).getSurname());
        surnameField.setDisabledTextColor(Color.black);

        JLabel birthdayLabel = new JLabel("Birthday");
        birthdayLabel.setBounds(10, 220, 200, 30);

        JTextField birthdayField = new JTextField();
        birthdayField.setBounds(90, 220, 300, 30);
        birthdayField.setEnabled(false);
        birthdayField.setText(((UserCommon)obj).getBirthdayDate() == null ? "" : ((UserCommon)obj).getBirthdayDate().toString());
        birthdayField.setDisabledTextColor(Color.black);

        JLabel passportLabel = new JLabel("Passport");
        passportLabel.setBounds(10, 270, 200, 30);

        JTextField passportField = new JTextField();
        passportField.setBounds(90, 270, 300, 30);
        passportField.setEnabled(false);
        passportField.setText(((UserCommon)obj).getPassport() == null ? "" : ((UserCommon)obj).getPassport());
        passportField.setDisabledTextColor(Color.black);

        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(10, 320, 200, 30);

        JTextField addressField = new JTextField();
        addressField.setBounds(90, 320, 300, 30);
        addressField.setText(((UserCommon)obj).getAddress() == null ? "" : ((UserCommon)obj).getAddress());
        addressField.setEnabled(false);
        addressField.setDisabledTextColor(Color.black);

        JLabel emailLabel = new JLabel("Email");
        emailLabel.setBounds(10, 370, 200, 30);

        JTextField emailField = new JTextField();
        emailField.setBounds(90, 370, 300, 30);
        emailField.setEnabled(false);
        emailField.setText(((UserCommon)obj).getEmail() == null ? "" : ((UserCommon)obj).getEmail());
        emailField.setDisabledTextColor(Color.black);

        JLabel regDateLabel = new JLabel("Date");
        regDateLabel.setBounds(10, 420, 200, 30);

        JTextField regDateField = new JTextField();
        regDateField.setBounds(90, 420, 300, 30);
        regDateField.setEnabled(false);
        regDateField.setText(((UserCommon)obj).getRegDate() == null ? "" : ((UserCommon)obj).getRegDate());
        regDateField.setDisabledTextColor(Color.black);

        addNewClientButton = new JButton("Add new client/manager");
        addNewClientButton.setBounds(420, 120 , 210, 30);

        seeAllUsersButton = new JButton("See all users/contracts");
        seeAllUsersButton.setBounds(420, 160, 210, 30);

        blockingUsersButton = new JButton("Block/Unblock users");
        blockingUsersButton.setBounds(420, 200, 210, 30);

        changeContractButton = new JButton("Change user`s contract");
        changeContractButton.setBounds(420, 240, 210, 30);

        searchUserButton = new JButton("Search user by phone");
        searchUserButton.setBounds(420, 280, 210, 30);

        addTariffButton = new JButton("Add/Delete tariff");
        addTariffButton.setBounds(420, 320, 210, 30);

        addOptionsButton = new JButton("Add/Delete options");
        addOptionsButton.setBounds(420, 360, 210, 30);

        addRulesOptionsButton = new JButton("Add option`s rules");
        addRulesOptionsButton.setBounds(420, 400, 210, 30);

        addContractButton = new JButton("Add/Delete contract");
        addContractButton.setBounds(420, 440, 210, 30);

        printContractsButton = new JButton("Print user`s contract");
        printContractsButton.setBounds(420, 480, 210, 30);

        mainPanel.add(welcomeLabel);

        mainPanel.add(mainSeparator);

        mainPanel.add(privateInformationLabel);

        mainPanel.add(chooseActionLabel);

        mainPanel.add(nameLabel);
        mainPanel.add(nameField);

        mainPanel.add(surnameLabel);
        mainPanel.add(surnameField);

        mainPanel.add(birthdayLabel);
        mainPanel.add(birthdayField);

        mainPanel.add(passportLabel);
        mainPanel.add(passportField);

        mainPanel.add(addressLabel);
        mainPanel.add(addressField);

        mainPanel.add(emailLabel);
        mainPanel.add(emailField);

        mainPanel.add(regDateLabel);
        mainPanel.add(regDateField);

        mainPanel.add(addNewClientButton);
        mainPanel.add(seeAllUsersButton);
        mainPanel.add(blockingUsersButton);
        mainPanel.add(changeContractButton);
        mainPanel.add(searchUserButton);
        mainPanel.add(addTariffButton);
        mainPanel.add(addOptionsButton);
        mainPanel.add(addRulesOptionsButton);
        mainPanel.add(addContractButton);

    }
}
