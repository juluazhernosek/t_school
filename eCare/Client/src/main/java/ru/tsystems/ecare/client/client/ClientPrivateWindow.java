package ru.tsystems.ecare.client.client;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.client.*;
import ru.tsystems.ecare.client.commands.server.GetContractsAllCommand;
import ru.tsystems.ecare.client.commands.server.GetContractsForUserCommand;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Created by julua on 07.08.14.
 */
public class ClientPrivateWindow extends AbstractFrame{
    private JButton contractButton;
    private JButton tariffButton;
    private JButton optionsButton;
    private JButton numberButton;

    public ClientPrivateWindow(Callable callable, UserCommon user) {
        super(false,callable, user);
    }

    @Override
    protected void initWindow() {
        setSize(650, 500);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    @Override
    protected void initCommands() {
        commands = new HashMap<>();
        commands.put("Contract", new WindowClientContractCommandClient());
        commands.put("GetContracts",new GetContractsAllCommand());
        commands.put("ChangeTariff", new WindowClientChangeTariffCommandClient());
        commands.put("Options", new WindowClientSelectNumber());
        commands.put("BlockNumber", new WindowClientBlockNumber());
    }

    protected void addActionsButton() {
        contractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                List<ContractCommon> contracts = newTask(new java.util.concurrent.Callable<List<ContractCommon>>() {
                    @Override
                    public List<ContractCommon> call() throws Exception {
                        return (List<ContractCommon>) commands.get("GetContracts").execute(obj);
                    }
                }, 3);
                commands.get("Contract").execute(ClientPrivateWindow.this, contracts);
            }
        });
        tariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                List<ContractCommon> contracts = newTask(new java.util.concurrent.Callable<List<ContractCommon>>() {
                    @Override
                    public List<ContractCommon> call() throws Exception {
                        return (List<ContractCommon>) commands.get("GetContracts").execute(obj);
                    }
                }, 3);
                ((UserCommon)obj).setContracts(contracts);
                commands.get("ChangeTariff").execute(ClientPrivateWindow.this, obj);
            }
        });
        optionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                List<ContractCommon> contracts = newTask(new java.util.concurrent.Callable<List<ContractCommon>>() {
                    @Override
                    public List<ContractCommon> call() throws Exception {
                        return (List<ContractCommon>) commands.get("GetContracts").execute(obj);
                    }
                }, 3);
                ((UserCommon)obj).setContracts(contracts);

                commands.get("Options").execute(ClientPrivateWindow.this, obj);
            }
        });
        numberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("BlockNumber").execute(ClientPrivateWindow.this, obj);
            }
        });
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(450, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    protected void addContent() {

        JLabel welcomeLabel = new JLabel("Welcome, dear client!");
        welcomeLabel.setFont(new Font("Serif", Font.PLAIN, 25));
        welcomeLabel.setBounds(140, 3, 290, 50);

        JSeparator mainSeparator = new JSeparator(SwingConstants.VERTICAL);
        mainSeparator.setBackground(new Color(190, 190, 190));
        mainSeparator.setBounds(400, 80, 40, 400);

        JLabel privateInformationLabel = new JLabel("Your private information");
        privateInformationLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        privateInformationLabel.setBounds(80, 70, 300, 30);

        JLabel chooseActionLabel = new JLabel("Choose Action");
        chooseActionLabel.setFont(new Font("Serif", Font.PLAIN, 18));
        chooseActionLabel.setBounds(460, 70, 300, 30);

        JLabel nameLabel = new JLabel("Name");
        nameLabel.setBounds(10, 120, 200, 30);

        JTextField nameField = new JTextField();
        nameField.setBounds(90, 120, 300, 30);
        nameField.setText(((UserCommon) obj).getName());
        nameField.setEnabled(false);
        nameField.setDisabledTextColor(Color.black);

        JLabel surnameLabel = new JLabel("Surname");
        surnameLabel.setBounds(10, 170, 200, 30);

        JTextField surnameField = new JTextField();
        surnameField.setBounds(90, 170, 300, 30);
        surnameField.setText(((UserCommon) obj).getSurname());
        surnameField.setEnabled(false);
        surnameField.setDisabledTextColor(Color.black);

        JLabel birthdayLabel = new JLabel("Birthday");
        birthdayLabel.setBounds(10, 220, 200, 30);

        JTextField birthdayField = new JTextField();
        birthdayField.setBounds(90, 220, 300, 30);
        birthdayField.setEnabled(false);
        birthdayField.setText(((UserCommon) obj).getBirthdayDate() == null ? "" :                String.valueOf(((UserCommon) obj).getBirthdayDate()));
        birthdayField.setDisabledTextColor(Color.black);

        JLabel passportLabel = new JLabel("Passport");
        passportLabel.setBounds(10, 270, 200, 30);

        JTextField passportField = new JTextField();
        passportField.setBounds(90, 270, 300, 30);
        passportField.setText(((UserCommon) obj).getPassport());
        passportField.setEnabled(false);
        passportField.setDisabledTextColor(Color.black);

        JLabel addressLabel = new JLabel("Address");
        addressLabel.setBounds(10, 320, 200, 30);

        JTextField addressField = new JTextField();
        addressField.setBounds(90, 320, 300, 30);
        addressField.setText(((UserCommon) obj).getAddress());
        addressField.setEnabled(false);
        addressField.setDisabledTextColor(Color.black);

        JLabel emailLabel = new JLabel("Email");
        emailLabel.setBounds(10, 370, 200, 30);

        JTextField emailField = new JTextField();
        emailField.setBounds(90, 370, 300, 30);
        emailField.setText(((UserCommon) obj).getEmail());
        emailField.setEnabled(false);
        emailField.setDisabledTextColor(Color.black);

        JLabel regDateLabel = new JLabel("Date");
        regDateLabel.setBounds(10, 420, 200, 30);

        JTextField regDateField = new JTextField();
        regDateField.setBounds(90, 420, 300, 30);
        regDateField.setText(((UserCommon) obj).getRegDate());
        regDateField.setEnabled(false);
        regDateField.setDisabledTextColor(Color.black);

        contractButton = new JButton("See contract");
        contractButton.setBounds(420, 120, 210, 60);

        tariffButton = new JButton("See/Change tariff");
        tariffButton.setBounds(420, 200, 210, 60);

        optionsButton = new JButton("See/Add/Remove options");
        optionsButton.setBounds(420, 280, 210, 60);

        numberButton = new JButton("Lock/Unlock number");
        numberButton.setBounds(420, 360, 210, 60);

        mainPanel.add(welcomeLabel);

        mainPanel.add(mainSeparator);

        mainPanel.add(privateInformationLabel);

        mainPanel.add(chooseActionLabel);

        mainPanel.add(nameLabel);
        mainPanel.add(nameField);

        mainPanel.add(surnameLabel);
        mainPanel.add(surnameField);

        mainPanel.add(birthdayLabel);
        mainPanel.add(birthdayField);

        mainPanel.add(passportLabel);
        mainPanel.add(passportField);

        mainPanel.add(addressLabel);
        mainPanel.add(addressField);

        mainPanel.add(emailLabel);
        mainPanel.add(emailField);

        mainPanel.add(regDateLabel);
        mainPanel.add(regDateField);

        mainPanel.add(contractButton);
        mainPanel.add(tariffButton);
        mainPanel.add(optionsButton);
        mainPanel.add(numberButton);
    }
}
