package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.manager.WindowManagerNewRulesOption;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * Created by julua on 17.08.14.
 */
public class TariffSelectionPageForRules extends TariffSelectionPage {

    public TariffSelectionPageForRules(Callable callable) {
        super(callable);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("NewRulesOption", new WindowManagerNewRulesOption());
        commands.put("GetAllTariff", new GetAllTariffCommandServer());
    }

    @Override
    protected void addActionsButton() {
        numberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("NewRulesOption").execute(TariffSelectionPageForRules.this,numberComboBox.getSelectedItem());
            }
        });
    }
}
