package ru.tsystems.ecare.client.connections;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;

/**
 * Created by julua on 17.08.14.
 */
public class SocketConnection implements IConnection {

    private Socket socket;
    private final static Logger logger = Logger.getLogger(SocketConnection.class);

    public SocketConnection(String host, int port) {
        try {
            socket = new Socket(host,port);
        } catch (IOException e) {
            logger.error("Cann't bind to " + host + ":" + port + " Message: " + e.getMessage());
        }
    }

    @Override
    public ObjectInputStream getObjectInputStream() {
        try {
            return new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e) {
            logger.error("Cann't get input stream. Message: " + e.getMessage());
            return null;
        }
    }

    @Override
    public ObjectOutputStream getObjectOutputStream() {
        try {
            return new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            logger.error("Cann't get output stream. Message: " + e.getMessage());
            return null;
        }
    }

    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            logger.error("Cann't close socket. Message : " + e.getMessage());
        }
    }
}
