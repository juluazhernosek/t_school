package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;

import java.io.Serializable;

/**
 * Created by julua on 23.08.14.
 */
public class UpdateUserCommand extends AbstractCommandClient {
    @Override
    public Object execute(Object... params) {
        ICommand command = new Command();
        command.setContent((Serializable) params[0]);
        command.setName("UpdateUser");
        send(command);
        ICommand response = read();
        return response == null ? null : response.getContent();
    }
}
