package ru.tsystems.ecare.client.client;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.TariffBucket;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.GetOptionsForTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.UpdateUserCommand;
import ru.tsystems.ecare.client.model.table.OptionListModel;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;

import javax.swing.*;
import javax.swing.text.html.Option;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Created by julua on 11.08.14.
 */
public class OptionsPage extends AbstractFrame {
    private List<OptionCommon> tariffOptions;
    private JList<OptionCommon> optionList;
    private OptionListModel listModel;
    private JComboBox<OptionCommon> optionsComboBox;
    private JComboBox<OptionCommon> deleteComboBox;
    private JButton addOptionsButton;
    private JButton deleteOptionsButton;
    private JButton okButton;
    private TariffBucket tariffBucket = new TariffBucket();


    public OptionsPage(Callable callable, ContractCommon contract) {
        super(true, callable, contract);
    }


    protected void initWindow() {
        setSize(380, 270);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        tariffOptions = (List<OptionCommon>) commands.get("GetOptionsForTariff").execute(((ContractCommon)obj).getTariff());
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
        width = 340;
        height += 42;
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("GetOptionsForTariff", new GetOptionsForTariffCommandServer());
        commands.put("UpdateUser",new UpdateUserCommand());
    }

    @Override
    protected void addActionsButton() {
        addOptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                OptionCommon selectOption = optionsComboBox.getItemAt(optionsComboBox.getSelectedIndex());
                if(selectOption == null)
                    return;
                if(listModel.contains(selectOption))
                    return;
                if(((ContractCommon)obj).getSelectedOptions().contains(selectOption))
                    return;
                listModel.add(selectOption);
                deleteComboBox.addItem(selectOption);
                deleteComboBox.validate();
                ((ContractCommon)obj).getSelectedOptions().add(selectOption);

            }
        });
        deleteOptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                OptionCommon selectOption = deleteComboBox.getItemAt(deleteComboBox.getSelectedIndex());
                deleteComboBox.removeItem(selectOption);
                deleteComboBox.validate();
                listModel.remove(selectOption);
                ((ContractCommon)obj).getSelectedOptions().remove(selectOption);
            }
        });

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Object response = newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("UpdateUser").execute(obj);
                    }
                },3);
                if(response == null) {
                    JOptionPane.showMessageDialog(OptionsPage.this, "Cann't saved options");
                    return;
                }
                else
                    dispose();
            }
        });
    }

    protected void addContent() {

        int count_options = 0;
        JLabel tariffLabel = new JLabel("Your tariff:");
        tariffLabel.setBounds(20, 50, 200, 30);

        JTextField tariffField = new JTextField();
        tariffField.setBounds(120, 55, 200, 20);
        tariffField.setText(((ContractCommon)obj).getTariff() == null ? "" : ((ContractCommon)obj).getTariff().getName());
        tariffField.setEnabled(false);
        tariffField.setDisabledTextColor(Color.black);


        JLabel optionsLabel = new JLabel("Connectivity options:");
        optionsLabel.setBounds(20, 80, 200, 30);

        JLabel addOptionsLabel = new JLabel("Add option:");
        addOptionsLabel.setBounds(20, 210, 200, 30);

        if(tariffOptions == null)
            optionsComboBox = new JComboBox();
        else
            optionsComboBox = new JComboBox(tariffOptions.toArray());
        optionsComboBox.setBounds(20, 240, 180, 20);

        addOptionsButton = new JButton("Add");
        addOptionsButton.setBounds(220, 240, 130, 20);

        JLabel deleteOptionsLabel = new JLabel("Delete option:");
        deleteOptionsLabel.setBounds(20, 270, 200, 30);

        deleteComboBox = new JComboBox(((ContractCommon)obj).getSelectedOptions().toArray());
        deleteComboBox.setBounds(20, 300, 180, 20);

        deleteOptionsButton = new JButton("Delete");
        deleteOptionsButton.setBounds(220, 300, 130, 20);

        height += 300;

        listModel = new OptionListModel(((ContractCommon)obj).getSelectedOptions());
        optionList = new JList<>(listModel);
        optionList.setLayoutOrientation(JList.VERTICAL);
        JScrollPane scrollPaneList = new JScrollPane(optionList);
        scrollPaneList.setBounds(20, 110, 200, 100);

        okButton = new JButton("OK");
        okButton.setBounds(180,320,130,20);

        mainPanel.add(okButton);

        mainPanel.add(tariffLabel);
        mainPanel.add(tariffField);
        mainPanel.add(optionsLabel);
        mainPanel.add(addOptionsLabel);
        mainPanel.add(scrollPaneList);
        mainPanel.add(optionsComboBox);
        mainPanel.add(addOptionsButton);
        mainPanel.add(deleteOptionsLabel);
        mainPanel.add(deleteComboBox);
        mainPanel.add(deleteOptionsButton);
    }
}
