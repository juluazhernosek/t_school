package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.server.BlockUserCommand;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

/**
 * Created by julua on 12.08.14.
 */
public class BlockingUsersPage extends AbstractFrame {

    private JButton blockButton;
    private JComboBox<ContractCommon> numberComboBox;
    private JButton unlockButton;
    private UserCommon user;

    public BlockingUsersPage(Callable callable, Object obj) {
        super(false, callable, obj);
    }

    protected void initWindow() {
        setSize(370, 170);
        setTitle("eCare");
        user = (UserCommon) obj;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(190, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands.put("BlockContract",new BlockUserCommand());
    }

    @Override
    protected void addActionsButton() {
        blockButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final ContractCommon contract = numberComboBox.getItemAt(numberComboBox.getSelectedIndex());
                contract.setActive(false);
                contract.setWhoBlocked(true);
                ContractCommon blockedContract = (ContractCommon) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("BlockContract").execute(contract);
                    }
                },3);
                if(blockedContract == null || blockedContract.isActive()) {
                    JOptionPane.showMessageDialog(BlockingUsersPage.this,"Cann't blocked contract","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(BlockingUsersPage.this,"Contract blocked","Message",JOptionPane.INFORMATION_MESSAGE);
                blockButton.setEnabled(false);
                unlockButton.setEnabled(true);
                dispose();
            }
        });

        unlockButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final ContractCommon contract = numberComboBox.getItemAt(numberComboBox.getSelectedIndex());
                contract.setActive(true);
                ContractCommon blockedContract = (ContractCommon) newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("BlockContract").execute(contract);
                    }
                },3);
                if(blockedContract == null || !blockedContract.isActive()) {
                    JOptionPane.showMessageDialog(BlockingUsersPage.this,"Cann't unblocked contract","Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(BlockingUsersPage.this,"Contract unlocked","Message",JOptionPane.INFORMATION_MESSAGE);
                blockButton.setEnabled(true);
                unlockButton.setEnabled(false);
                dispose();
            }
        });
    }

    protected void addContent() {

        JLabel blockLabel = new JLabel("Block/Unlock number:");
        blockLabel.setFont(new Font("Serif", Font.PLAIN, 16));
        blockLabel.setBounds(10, 20, 200, 50);

        JLabel numberLabel = new JLabel("Number phone:");
        numberLabel.setBounds(10, 70, 200, 30);



        blockButton = new JButton("Block");
        blockButton.setBounds(20, 110, 150, 30);

        unlockButton = new JButton("Unlock");
        unlockButton.setBounds(190, 110, 150, 30);

        if(user == null || user.getContracts() == null || user.getContracts().isEmpty()) {
            numberComboBox = new JComboBox();
            blockButton.setEnabled(false);
            unlockButton.setEnabled(false);
        }
        else {
            numberComboBox = new JComboBox(user.getContracts().toArray());
            numberComboBox.setSelectedIndex(0);
            if(user.getContracts().get(0).isActive())
                unlockButton.setEnabled(false);
            else {
                blockButton.setEnabled(false);
            }
        }
        numberComboBox.setBounds(160, 70, 200, 30);

        mainPanel.add(blockLabel);

        mainPanel.add(numberLabel);
        mainPanel.add(numberComboBox);

        mainPanel.add(blockButton);
        mainPanel.add(unlockButton);
    }
}
