package ru.tsystems.ecare.client;

/**
 * Created by julua on 16.08.14.
 */
public interface Callable {
    void call();
}
