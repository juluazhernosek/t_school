package ru.tsystems.ecare.client.bucket;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.entities.CommonBucket;
import ru.tsystems.ecare.client.bucket.entities.MergedContract;
import ru.tsystems.ecare.common.entities.ContractCommon;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created by julua on 20.08.14.
 */
public class Bucket extends AbstractFrame {

    public CommonBucket bucket;

    public Bucket(Callable callable, Object bucket) {
        super(true, callable, bucket);
    }

    protected void initWindow() {
        setSize(370, 200);
        setTitle("eCare");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        bucket = (CommonBucket) obj;
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(170, 3, 250, 39);
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
    }

    @Override
    protected void addActionsButton() {
    }

    protected void addContent() {
        JLabel changesLabel = new JLabel("Your changes:");
        changesLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        changesLabel.setBounds(20, 20, 200, 30);
        int count_options = 0;
        height = 40;
        if (bucket.getAddedContracts() != null) {
            for (int i = 0; i < bucket.getAddedContracts().size(); i++) {
                ContractCommon newContract = bucket.getAddedContracts().get(i).getContract();
                JLabel newContractLabel = new JLabel((i + 1) + ". New contract:");
                newContractLabel.setBounds(10, 70 + 100 * i + 40 * count_options, 200, 30);
                mainPanel.add(newContractLabel);

                JLabel newTariffLabel = new JLabel("New tariff:");
                newTariffLabel.setBounds(10, 110 + 100 * i + 40 * count_options, 200, 30);

                JTextField newTariffField = new JTextField();
                newTariffField.setBounds(140, 110 + 100 * i + 40 * count_options, 200, 30);
                newTariffField.setEnabled(false);
                newTariffField.setDisabledTextColor(Color.black);
                newTariffField.setText(newContract.getTariff().getName());
                mainPanel.add(newTariffLabel);
                mainPanel.add(newTariffField);
                if (newContract.getSelectedOptions() != null && newContract.getSelectedOptions().size()!=0) {
                    JLabel newOptionLabel = new JLabel("New options:");
                    newOptionLabel.setBounds(10, 150 + 100 * i + 40 * count_options, 200, 30);
                    height += 30;
                    mainPanel.add(newOptionLabel);
                    for (int m = 0; m < newContract.getSelectedOptions().size(); m++) {
                        JTextField newOptionField = new JTextField();
                        newOptionField.setBounds(10, 180 + 100 * i + 40 * count_options, 200, 30);
                        newOptionField.setEnabled(false);
                        newOptionField.setDisabledTextColor(Color.black);
                        newOptionField.setText(newContract.getSelectedOptions().get(m).toString());
                        mainPanel.add(newOptionField);
                        count_options++;
                        height+=30;
                    }
                }
                height += 110;
            }
        }
        if(bucket.getRemovedContracts() != null){
            int heightForRemovedContract = height;
            for (int i = 0; i < bucket.getRemovedContracts().size(); i++) {
                ContractCommon deleteContract = bucket.getRemovedContracts().get(i).getContract();
                JLabel deleteContractLabel = new JLabel((i + 1) + ". Delete contract:");
                deleteContractLabel.setBounds(10, (heightForRemovedContract)+100*i + 40 * count_options, 200, 30);

                JLabel deleteNumberLabel = new JLabel("Delete number:");
                deleteNumberLabel.setBounds(10, (heightForRemovedContract+40)+100*i + 40 * count_options, 200, 30);

                JTextField deleteNumberField= new JTextField();
                deleteNumberField.setBounds(10, (heightForRemovedContract+80)+100*i + 40 * count_options, 200, 30);
                deleteNumberField.setText(deleteContract.getPhoneNumber());
                deleteNumberField.setEnabled(false);
                deleteNumberField.setDisabledTextColor(Color.black);

                height += 190;

                mainPanel.add(deleteContractLabel);
                mainPanel.add(deleteNumberLabel);
                mainPanel.add(deleteNumberField);
            }
        }

        if(bucket.getMergedContracts() != null){
            for(int i = 0; i < bucket.getMergedContracts().size(); i++){
                JLabel changedContractLabel = new JLabel((i + 1) + ". Changed contract:");
                changedContractLabel.setBounds(10, 70 + 40 * count_options, 200, 30);
                mainPanel.add(changedContractLabel);
                MergedContract mergedContract = bucket.getMergedContracts().get(i);
                if(mergedContract.getTariff() != null){
                    height+=40;
                    JLabel tariffLabel = new JLabel("New tariff:");
                    tariffLabel.setBounds(10, 110 + 40 * count_options, 200, 30);
                    JTextField tariffField = new JTextField();
                    tariffField.setBounds(140, 110 + 40 * count_options, 200, 30);
                    tariffField.setEnabled(false);
                    tariffField.setDisabledTextColor(Color.black);
                    tariffField.setText(mergedContract.getTariff().getName());
                    mainPanel.add(tariffLabel);
                    mainPanel.add(tariffField);
                    count_options++;
                }
                if(mergedContract.getAddedOptions()!= null && mergedContract.getAddedOptions().size()!=0){
                    for (int m = 0; m < mergedContract.getAddedOptions().size(); m++) {
                        JLabel newOptionLabel = new JLabel("New option:");
                        newOptionLabel.setBounds(10, 110 + 40 * count_options, 200, 30);
                        JTextField newOptionField = new JTextField();
                        newOptionField.setBounds(140, 110 + 40 * count_options, 200, 30);
                        newOptionField.setEnabled(false);
                        newOptionField.setDisabledTextColor(Color.black);
                        newOptionField.setText(mergedContract.getAddedOptions().get(m).toString());
                        mainPanel.add(newOptionField);
                        count_options++;
                        height+=40;
                    }
                }
                if(mergedContract.getRemovedOptions()!= null && mergedContract.getRemovedOptions().size()!=0){
                    for (int m = 0; m < mergedContract.getRemovedOptions().size(); m++) {
                        JLabel deleteOptionLabel = new JLabel("Remove option:");
                        deleteOptionLabel.setBounds(10, 110 + 40 * count_options, 200, 30);
                        JTextField deleteOptionField = new JTextField();
                        deleteOptionField.setBounds(140, 110 + 40 * count_options, 200, 30);
                        deleteOptionField.setEnabled(false);
                        deleteOptionField.setDisabledTextColor(Color.black);
                        deleteOptionField.setText(mergedContract.getRemovedOptions().get(m).toString());
                        mainPanel.add(deleteOptionField);
                        count_options++;
                        height+=40;
                    }
                }
            }
        }
        mainPanel.add(changesLabel);

        width = 350;

    }
}