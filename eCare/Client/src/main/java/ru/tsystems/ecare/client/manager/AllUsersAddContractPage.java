package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.manager.WindowManagerAddContract;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * Created by julua on 17.08.14.
 */
public class AllUsersAddContractPage extends AllUsersPage {
    public AllUsersAddContractPage(Callable callable, Object obj) {
        super(callable,obj);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("AddContract", new WindowManagerAddContract());
    }

    @Override
    protected void addActionsButton() {
        moreInfoButton.setText("Select");
        moreInfoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getSelectedItem().setContracts(getAllContracts(getSelectedItem()));
                commands.get("AddContract").execute(AllUsersAddContractPage.this,getSelectedItem());
            }
        });
    }
}
