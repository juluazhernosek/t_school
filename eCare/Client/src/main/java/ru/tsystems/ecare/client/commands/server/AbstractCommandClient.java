package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.client.HandlerCommands;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.common.commands.ICommand;

/**
 * Created by julua on 17.08.14.
 */
public abstract class AbstractCommandClient implements ICommandClient {

    protected void send(ICommand command) {
        HandlerCommands.getInstance().sendRequest(command);
    }

    protected ICommand read() {
        return HandlerCommands.getInstance().getResponse();
    }

}
