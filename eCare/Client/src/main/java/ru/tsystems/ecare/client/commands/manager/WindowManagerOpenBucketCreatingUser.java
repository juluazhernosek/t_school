package ru.tsystems.ecare.client.commands.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.bucket.Bucket;

import javax.swing.*;

/**
 * Created by julua on 20.08.14.
 */
public class WindowManagerOpenBucketCreatingUser implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame)params[0]).setEnabled(false);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Bucket(new Callable() {
                    @Override
                    public void call() {
                        ((JFrame)params[0]).setEnabled(true);
                    }
                }, params[1]);
            }
        });
        return null;
    }
}
