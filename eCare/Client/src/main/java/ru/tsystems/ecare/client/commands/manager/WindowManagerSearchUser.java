package ru.tsystems.ecare.client.commands.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.manager.SearchByPhonePage;

import javax.swing.*;

/**
 * Created by julua on 17.08.14.
 */
public class WindowManagerSearchUser implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame)params[0]).setEnabled(false);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SearchByPhonePage(new Callable() {
                    @Override
                    public void call() {
                        ((JFrame)params[0]).setEnabled(true);
                    }
                });
            }
        });
        return null;
    }
}
