package ru.tsystems.ecare.client.commands.server;

import ru.tsystems.ecare.common.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.OptionCommon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 18.08.14.
 */
public class GetOptionsForTariffCommandServer extends AbstractCommandClient {
    @Override
    public Object execute(final Object... params) {
        ICommand command = new Command();
        command.setName("GetOptionsForTariff");
        command.setContent((java.io.Serializable) params[0]);
        send(command);
        List<OptionCommon> options = (List<OptionCommon>) read().getContent();
        return options;
    }
}
