package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.commands.manager.WindowManagerPrintContract;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * Created by julua on 17.08.14.
 */
public class NumberSelectionPrintContract extends NumberSelectionPage {

    public NumberSelectionPrintContract(Callable callable) {
        super(callable);
    }
    protected void initCommands() {
        commands = new HashMap<String, ICommandClient>();
        commands.put("PrintContract", new WindowManagerPrintContract());
    }

    @Override
    protected void addActionsButton() {
        numberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                commands.get("PrintContract").execute(NumberSelectionPrintContract.this);
            }
        });
    }
}
