package ru.tsystems.ecare.client.commands.manager;

import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.client.manager.PrintContractPage;

import javax.swing.*;

/**
 * Created by julua on 17.08.14.
 */
public class WindowManagerPrintContract implements ICommandClient {
    @Override
    public Object execute(final Object... params) {
        ((JFrame)params[0]).dispose();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PrintContractPage(null);
            }
        });
        return null;
    }
}
