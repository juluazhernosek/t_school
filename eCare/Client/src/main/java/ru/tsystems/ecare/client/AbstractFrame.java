package ru.tsystems.ecare.client;

import ru.tsystems.ecare.client.commands.ICommandClient;
import ru.tsystems.ecare.common.IObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by julua on 16.08.14.
 */
public abstract class AbstractFrame extends JFrame {

    protected JPanel mainPanel;
    protected int width;
    protected int height;
    protected Map<String,ICommandClient> commands;
    protected Object obj;
    protected Object oldObj;
    private ExecutorService service;


    protected AbstractFrame(boolean isUseScroll, final Callable callable, Object ...objs) {
        commands = new HashMap<String, ICommandClient>();
        service = Executors.newFixedThreadPool(3);
        initCommands();
        setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/3,
                Toolkit.getDefaultToolkit().getScreenSize().height/3);
        this.obj = objs == null ? null : objs[0];
        oldObj = objs != null && objs.length > 1 ? objs[1] : null;
        initWindow();
        setResizable(false);
        mainPanel = new JPanel();
        mainPanel.setLayout(null);
        addLogo(AbstractFrame.class.getResource("/Deutsche.png"));
        addContent();
        addActionsButton();
        if(isUseScroll) {
            mainPanel.setPreferredSize(new Dimension(width, height));
            JScrollPane scrollPane = new JScrollPane(mainPanel);
            setContentPane(scrollPane);
        }
        else {
            setContentPane(mainPanel);
        }
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosed(WindowEvent e) {
                if(callable != null)
                    callable.call();
            }
        });
        setVisible(true);
    }

    protected <T> T newTask(java.util.concurrent.Callable<T> callable, long time) {
        service = Executors.newFixedThreadPool(1);
        Future<T> future = service.submit(callable);
        try {
            return future.get(time, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            service.shutdownNow();
            return null;
        }
        finally {
            service.shutdownNow();
        }
    }

    protected abstract void initCommands();

    protected abstract void addActionsButton();

    protected abstract void addContent();

    protected abstract void addLogo(URL path);

    protected abstract void initWindow();
}
