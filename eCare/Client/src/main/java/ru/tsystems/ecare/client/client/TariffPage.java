package ru.tsystems.ecare.client.client;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.entities.CommonBucket;
import ru.tsystems.ecare.client.bucket.entities.MergedContract;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.UpdateUserCommand;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created by julua on 09.08.14.
 */
public class TariffPage extends AbstractFrame {
    private JButton changeButton;
    private List<TariffCommon> tariffs;
    private JComboBox<ContractCommon> numberComboBox;
    private JComboBox<TariffCommon> tariffComboBox;
    private CommonBucket bucket;

    public TariffPage(Callable callable, Object object) {
        super(true, callable, object);
    }

    protected void initWindow() {
        setSize(570, 270);
        setTitle("eCare");
        width = 540;
        tariffs = (List<TariffCommon>) commands.get("GetAllTariff").execute();
        bucket = new CommonBucket((UserCommon)obj);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(370, 3, 250, 39);
        height += 60;
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<>();
        commands.put("GetAllTariff", new GetAllTariffCommandServer());
        commands.put("UpdateUser", new UpdateUserCommand());
    }

    @Override
    protected void addActionsButton() {
        changeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((UserCommon)obj).getContracts().get(numberComboBox.getSelectedIndex()).
                        setTariff(tariffComboBox.getItemAt(tariffComboBox.getSelectedIndex()));
                ((UserCommon)obj).getContracts().get(numberComboBox.getSelectedIndex()).getSelectedOptions().clear();
                Object response = newTask(new java.util.concurrent.Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return commands.get("UpdateUser").execute(obj);
                    }
                },3);
                if(response == null) {
                    JOptionPane.showMessageDialog(TariffPage.this,"Cann't change tariff","Error",JOptionPane.ERROR_MESSAGE);
                }
                MergedContract mergedContract = bucket.getMergeByContract( ((UserCommon)obj).getContracts().get(numberComboBox.getSelectedIndex()));
                if(mergedContract == null)
                    bucket.getMergedContracts().add(new MergedContract(((UserCommon)obj).getContracts().get(numberComboBox.getSelectedIndex())));
                else
                    mergedContract.setTariff(tariffComboBox.getItemAt(tariffComboBox.getSelectedIndex()));
                dispose();
            }
        });

    }

    protected void addContent() {
        List<ContractCommon> contracts = ((UserCommon)obj).getContracts();

        JLabel clientLabel = new JLabel("Dear client!");
        clientLabel.setFont(new Font("Serif", Font.PLAIN, 25));
        clientLabel.setBounds(130, 10, 290, 50);

        for(int i = 0; i < contracts.size(); i++){
            JLabel numberLabel = new JLabel("Number:");
            numberLabel.setBounds(10, 30 + 40 *i, 100, 100);

            JTextField numberField = new JTextField();
            numberField.setBounds(100, 70 + 40 *i, 190, 20);
            numberField.setText(contracts.get(i).getPhoneNumber());
            numberField.setEnabled(false);
            numberField.setDisabledTextColor(Color.black);


            JLabel tariffLabel = new JLabel("Tariff:");
            tariffLabel.setBounds(300, 30 + 40 *i, 200, 100);

            final JTextField tariffField = new JTextField();
            tariffField.setBounds(350, 70 + 40 *i, 190, 20);
            tariffField.setEnabled(false);
            tariffField.setDisabledTextColor(Color.black);
            tariffField.setText(contracts.get(i).getTariff().getName());

            height += 40;

            mainPanel.add(numberLabel);
            mainPanel.add(numberField);

            mainPanel.add(tariffLabel);
            mainPanel.add(tariffField);
        }

        JLabel changeTariffLabel = new JLabel("Change tariff:");
        changeTariffLabel.setBounds(220, 100 + 40 * (contracts.size()-1), 200, 40);
        changeTariffLabel.setFont(new Font("Serif", Font.PLAIN, 18));

        JLabel numberLabel = new JLabel("Number:");
        numberLabel.setBounds(60, 100 + 40 * (contracts.size()-1), 100, 100);

        JLabel tariffLabel = new JLabel("Tariff:");
        tariffLabel.setBounds(370, 100 + 40 * (contracts.size()-1), 200, 100);

        numberComboBox = new JComboBox(contracts.toArray());
        numberComboBox.setBounds(40, 170 + 40 * (contracts.size()-1), 200, 20);

        tariffComboBox = new JComboBox(tariffs.toArray());
        tariffComboBox.setBounds(310, 170 + 40 * (contracts.size()-1), 200, 20);

        changeButton = new JButton("Change");
        changeButton.setEnabled(!contracts.isEmpty());
        changeButton.setBounds(240, 220 + 40 * (contracts.size()-1), 100, 30);

        height += 160;

        mainPanel.add(clientLabel);
        mainPanel.add(changeTariffLabel);
        mainPanel.add(tariffLabel);
        mainPanel.add(numberLabel);
        mainPanel.add(numberComboBox);
        mainPanel.add(tariffComboBox);
        mainPanel.add(changeButton);

    }
}
