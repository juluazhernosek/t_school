package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.client.bucket.entities.CommonBucket;
import ru.tsystems.ecare.client.bucket.entities.MergedContract;
import ru.tsystems.ecare.client.commands.server.GetAllTariffCommandServer;
import ru.tsystems.ecare.client.commands.server.UpdateContractCommand;
import ru.tsystems.ecare.client.model.table.OptionListModel;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Created by julua on 12.08.14.
 */
public class ChangeUsersContractPage extends AbstractFrame {
    private List<TariffCommon> tariffs;
    private JComboBox<TariffCommon> tariffComboBox;
    private JButton addOptionsButton;
    private JComboBox<OptionCommon> optionsComboBox;
    private OptionListModel listModel;
    private JList<OptionCommon> optionList;
    private JButton deleteOptionButton;
    private CommonBucket bucket;
    private JTextField tariffField;
    private JButton changeTariffButton;
    private ContractCommon contract;
    private JButton okButton;
    private MergedContract mergedContract;


    public ChangeUsersContractPage(Callable callable, Object obj, Object bucket) {
        super(true, callable, obj,bucket);
    }


    protected void initWindow() {
        setTitle("eCare");
        setSize(600, 300);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        tariffs = (List<TariffCommon>) commands.get("GetAllTariff").execute();
        bucket = (CommonBucket) oldObj;
        contract =(ContractCommon) obj;
        mergedContract = new MergedContract(contract);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(400, 3, 250, 39);
        width = 400;
        height += 42;
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {
        commands = new HashMap<>();
        commands.put("GetAllTariff", new GetAllTariffCommandServer());
    }

    @Override
    protected void addActionsButton() {
        addOptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (((ContractCommon) obj).getSelectedOptions() == null)
                    ((ContractCommon) obj).setSelectedOptions(new ArrayList<OptionCommon>());
                OptionCommon selectedOption = optionsComboBox.getItemAt(optionsComboBox.getSelectedIndex());
                if (!((ContractCommon) obj).getSelectedOptions().contains(selectedOption)) {
                    ((ContractCommon) obj).getSelectedOptions().add(selectedOption);
                }
                mergedContract.getAddedOptions().add(selectedOption);
                if(listModel.contains(selectedOption))
                    return;
                listModel.add(selectedOption);

            }
        });

        tariffComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                TariffCommon tariff = (TariffCommon) itemEvent.getItem();
                if (tariff == null || tariff.getOptions().isEmpty())
                    optionsComboBox.removeAllItems();
                else {
                    optionsComboBox.removeAllItems();
                    for (OptionCommon option : tariff.getOptions()) {
                        optionsComboBox.addItem(option);
                    }
                }
                optionsComboBox.validate();
                optionsComboBox.repaint();
                if (((ContractCommon) obj).getSelectedOptions() == null)
                    ((ContractCommon) obj).setSelectedOptions(new ArrayList<OptionCommon>());
                mergedContract.getRemovedOptions().addAll(contract.getSelectedOptions());
                ((ContractCommon) obj).getSelectedOptions().clear();
                listModel.clear();
            }
        });
        changeTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TariffCommon selectedTariff = tariffComboBox.getItemAt(tariffComboBox.getSelectedIndex());
                if(selectedTariff == null) {
                    return;
                }
                tariffField.setText(selectedTariff.getName());
                contract.setTariff(selectedTariff);
                mergedContract.setTariff(selectedTariff);
            }
        });

        deleteOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(optionList.getSelectedValue() == null)
                    return;
                contract.getSelectedOptions().remove(optionList.getSelectedValue());
                mergedContract.getRemovedOptions().add(optionList.getSelectedValue());
            }
        });

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                bucket.getMergedContracts().add(mergedContract);
                dispose();
            }
        });


    }

    protected void addContent() {
        ContractCommon contract = ((ContractCommon) obj);
        JLabel allContractsLabel = new JLabel("Your contracts:");
        allContractsLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        allContractsLabel.setBounds(10, 5, 290, 30);

        JLabel contractLabel = new JLabel("Contract:");
        contractLabel.setBounds(10, 50, 290, 30);

        JLabel numberLabel = new JLabel("Number:");
        numberLabel.setBounds(10, 80, 290, 30);

        JTextField numberField = new JTextField();
        numberField.setText(contract.getPhoneNumber());
        numberField.setBounds(100, 80, 290, 30);
        numberField.setEnabled(false);
        numberField.setDisabledTextColor(Color.black);

        JLabel tariffLabel = new JLabel("Tariff:");
        tariffLabel.setBounds(10, 120, 290, 30);

        tariffField = new JTextField();
        tariffField.setBounds(70, 120, 200, 30);
        tariffField.setText(contract.getTariff() == null ? "" : contract.getTariff().getName());
        tariffField.setEnabled(false);
        tariffField.setDisabledTextColor(Color.black);

        JLabel changeTariffLabel = new JLabel("Change tariff:");
        changeTariffLabel.setBounds(280, 120, 290, 30);

        if (tariffs == null || tariffs.isEmpty())
            tariffComboBox = new JComboBox<>();
        else {
            TariffCommon[] ts = new TariffCommon[tariffs.size()];
            for (int i = 0; i < ts.length; i++) {
                ts[i] = tariffs.get(i);
            }
            tariffComboBox = new JComboBox<>(ts);
            tariffComboBox.setSelectedIndex(0);
        }

        tariffComboBox.setBounds(390, 120, 190, 30);

        changeTariffButton = new JButton("Change");
        changeTariffButton.setBounds(400, 160, 150, 30);

        JLabel addOptionsLabel = new JLabel("Add option:");
        addOptionsLabel.setBounds(10, 200, 150, 30);


        List<OptionCommon> options = tariffComboBox.getItemAt(tariffComboBox.getSelectedIndex()).getOptions();
        optionsComboBox = new JComboBox(options.toArray());
        optionsComboBox.setBounds(150, 200, 180, 30);

        addOptionsButton = new JButton("Add");
        addOptionsButton.setBounds(340, 200, 120, 30);

        JLabel optionsLabel = new JLabel("Connectivity options:");
        optionsLabel.setBounds(10, 230, 290, 30);

        height += 260;

        listModel = new OptionListModel(contract.getSelectedOptions());
        optionList = new JList<>(listModel);
        optionList.setLayoutOrientation(JList.VERTICAL);
        JScrollPane scrollPaneList = new JScrollPane(optionList);
        scrollPaneList.setBounds(10, 260, 290, 100);
        deleteOptionButton = new JButton("Delete");
        deleteOptionButton.setBounds(330, 260, 100, 30);
        height += 100;

        mainPanel.add(allContractsLabel);

        mainPanel.add(contractLabel);

        mainPanel.add(numberLabel);
        mainPanel.add(numberField);

        mainPanel.add(tariffLabel);
        mainPanel.add(tariffField);
        mainPanel.add(changeTariffLabel);
        mainPanel.add(tariffComboBox);
        mainPanel.add(deleteOptionButton);
        mainPanel.add(changeTariffButton);
        mainPanel.add(scrollPaneList);
        mainPanel.add(addOptionsLabel);
        mainPanel.add(optionsComboBox);
        mainPanel.add(addOptionsButton);

        mainPanel.add(optionsLabel);

        okButton = new JButton("Ok");
        okButton.setBounds(230, height - 40, 100, 40);

        mainPanel.add(okButton);

    }
}
