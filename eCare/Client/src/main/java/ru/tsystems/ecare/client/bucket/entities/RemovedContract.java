package ru.tsystems.ecare.client.bucket.entities;

import ru.tsystems.ecare.common.entities.ContractCommon;

/**
 * Created by julua on 21.08.14.
 */
public class RemovedContract {

    private ContractCommon contract;

    public RemovedContract(ContractCommon contract) {
        this.contract = contract;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && contract != null && o instanceof RemovedContract && contract.equals(((RemovedContract)o).contract);
    }

    public ContractCommon getContract() {
        return contract;
    }
}
