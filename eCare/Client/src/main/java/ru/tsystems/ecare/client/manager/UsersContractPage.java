package ru.tsystems.ecare.client.manager;

import ru.tsystems.ecare.client.AbstractFrame;
import ru.tsystems.ecare.client.Callable;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.List;

/**
 * Created by julua on 12.08.14.
 */
public class UsersContractPage extends AbstractFrame {

    public UsersContractPage(Callable callable,Object obj) {
        super(true, callable, obj);
    }

    protected void initWindow() {

        setTitle("eCare");setSize(420, 250);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    protected void addLogo(URL path) {
        ImageIcon image = new ImageIcon(path);
        JLabel labelImage = new JLabel();
        labelImage.setIcon(image);
        labelImage.setBounds(230, 3, 250, 39);
        width = 400;
        height += 42;
        mainPanel.add(labelImage);
    }

    @Override
    protected void initCommands() {

    }

    @Override
    protected void addActionsButton() {

    }

    protected void addContent() {

        JLabel allContractsLabel = new JLabel("User's contracts:");
        allContractsLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        allContractsLabel.setBounds(10, 5, 290, 30);

        List<ContractCommon> contracts = ((UserCommon) obj).getContracts();
        int countOptions = 0;
        int countContracts = 0;
        if(contracts != null) {
            countContracts = contracts.size();
            for (ContractCommon contract : contracts) {
                if (contract.getSelectedOptions() != null)
                    countOptions += contract.getSelectedOptions().size();
            }
        }

        JLabel userEmaiLabel = new JLabel("User`s email:");
        userEmaiLabel.setBounds(10, 50, 150, 30);

        JTextField userEmailTextField = new JTextField();
        userEmailTextField.setBounds(110, 50, 200, 30);
        userEmailTextField.setEnabled(false);
        userEmailTextField.setDisabledTextColor(Color.black);
        userEmailTextField.setText(((UserCommon) obj).getEmail());

        for (int m = 0; m < countContracts; m++) {

            JLabel contractLabel = new JLabel((m+1)+". Contract:");
            contractLabel.setBounds(10, 90 + 150 * m + m*countOptions*40, 290, 30);

            JLabel numberLabel = new JLabel("Number:");
            numberLabel.setBounds(10, 130 +150 * m + m*countOptions*40, 290, 30);

            JTextField numberField = new JTextField();
            numberField.setBounds(100, 130 +150 * m + m*countOptions*40, 290, 30);
            numberField.setText(contracts.get(m).getPhoneNumber());
            numberField.setEnabled(false);
            numberField.setDisabledTextColor(Color.black);

            JLabel tariffLabel = new JLabel("Tariff:");
            tariffLabel.setBounds(10, 180 +150 * m + m*countOptions*40, 290, 30);

            JTextField tariffField = new JTextField();
            tariffField.setBounds(100, 180 +150 * m + m*countOptions*40, 290, 30);
            tariffField.setText(contracts.get(m).getTariff() == null ? "" : contracts.get(m).getTariff().getName());
            tariffField.setEnabled(false);
            tariffField.setDisabledTextColor(Color.black);

            JLabel optionsLabel = new JLabel("Options:");
            optionsLabel.setBounds(10, 220 +150 * m + m*countOptions*40, 290, 30);

            height += 200 + countOptions*40;

            int countOptionsContract = contracts.get(m).getSelectedOptions().size();

            for (int i = 0; i < countOptionsContract; i++) {
                JTextField optionsField = new JTextField();
                optionsField.setBounds(10, (250 + i * 40)+150*m + m*countOptions*40, 290, 30);
                optionsField.setText(contracts.get(m).getSelectedOptions().get(i).getName());
                optionsField.setEnabled(false);
                optionsField.setDisabledTextColor(Color.black);
                mainPanel.add(optionsField);
            }

            mainPanel.add(allContractsLabel);

            mainPanel.add(userEmaiLabel);
            mainPanel.add(userEmailTextField);

            mainPanel.add(contractLabel);

            mainPanel.add(numberLabel);
            mainPanel.add(numberField);

            mainPanel.add(tariffLabel);
            mainPanel.add(tariffField);

            mainPanel.add(optionsLabel);
        }
    }
}
