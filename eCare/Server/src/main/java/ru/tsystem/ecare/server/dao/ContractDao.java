package ru.tsystem.ecare.server.dao;

import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Tariff;

import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public interface ContractDao {
    Contract getContractByNumber(String number);
    Contract createContract(Contract contract);
    List<Contract> getContractsByTariff(Tariff tariff);
}
