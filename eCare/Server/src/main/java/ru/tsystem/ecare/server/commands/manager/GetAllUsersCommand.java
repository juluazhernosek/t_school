package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 24.08.14.
 */
public class GetAllUsersCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        List<User> users = FactoryService.getUserService(em).getAllClients();
        List<UserCommon> userCommons = EntityScan.scanServerUsers(users);
        command.setContent((java.io.Serializable) userCommons);
        return command;
    }
}
