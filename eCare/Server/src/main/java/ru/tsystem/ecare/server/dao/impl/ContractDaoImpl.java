package ru.tsystem.ecare.server.dao.impl;

import ru.tsystem.ecare.server.dao.ContractDao;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Tariff;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class ContractDaoImpl extends JpaDao implements ContractDao{
    public ContractDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public Contract getContractByNumber(String number) {
        return getSingleResult(em.createQuery("SELECT c FROM Contract c WHERE c.phoneNumber=:number",Contract.class)
                .setParameter("number",number).getResultList());
    }

    @Override
    public Contract createContract(Contract contract) {
        em.persist(contract);
        em.refresh(contract);
        return contract;
    }

    @Override
    public List<Contract> getContractsByTariff(Tariff tariff) {
        return em.createQuery("SELECT c FROM Contract c WHERE c.tariff=:tariff",Contract.class)
                .setParameter("tariff",tariff)
                .getResultList();
    }
}
