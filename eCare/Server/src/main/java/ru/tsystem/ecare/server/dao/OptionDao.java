package ru.tsystem.ecare.server.dao;

import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;

import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public interface OptionDao {
    List<Option> getOptionsByTariff(Tariff tariff);
    Option findOptionByTariff(String name,Tariff tariff);
}
