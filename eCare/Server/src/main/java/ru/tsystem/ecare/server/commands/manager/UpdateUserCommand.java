package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 23.08.14.
 */
public class UpdateUserCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        UserCommon userCommon = (UserCommon) command.getContent();
        User user = EntityScan.scanCommonUser(userCommon);
        user.setContracts(EntityScan.scanCommonContracts(userCommon.getContracts()));
        for(int i = 0; i < user.getContracts().size(); i++) {
            user.getContracts().get(i).setUser(user);
            user.getContracts().get(i).setTariff(EntityScan.scanCommonTariff(userCommon.getContracts().get(i).getTariff()));
            user.getContracts().get(i).setSelectedOptions(EntityScan.scanCommonOptions(userCommon.getContracts().get(i).getSelectedOptions()));
        }
        User newUser = FactoryService.getUserService(em).updateUser(user);
        command.setContent(EntityScan.scanServerUser(newUser));
        return command;
    }

}
