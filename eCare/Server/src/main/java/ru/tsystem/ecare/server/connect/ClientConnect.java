package ru.tsystem.ecare.server.connect;

import org.apache.log4j.Logger;
import ru.tsystem.ecare.server.commands.Command;
import ru.tsystems.ecare.common.commands.ICommand;

import javax.persistence.EntityManager;
import java.io.*;
import java.net.Socket;
import java.util.HashMap;

/**
 * Created by julua on 22.08.14.
 */
public class ClientConnect implements Runnable {
    private static Logger logger = Logger.getLogger(ClientConnect.class);

    private Socket client;
    private EntityManager em;
    private HashMap<String,Command> commands;

    public ClientConnect(Socket client, EntityManager em,HashMap<String,Command> commands) {
        this.client = client;
        this.em = em;
        this.commands = commands;
    }

    public void run() {
        try {
            logger.info("Connection from "+client.getLocalSocketAddress());
            ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
            ICommand objectFromClient = (ICommand) input.readObject();
            ICommand response = commands.get(objectFromClient.getName()).execute(objectFromClient,em);
            ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
            output.writeObject(response);
            output.flush();
        }
        catch(Exception e) {
            logger.error(e.getMessage());
        }
    }
}
