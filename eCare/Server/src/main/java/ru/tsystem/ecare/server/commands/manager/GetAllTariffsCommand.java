package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 23.08.14.
 */
public class GetAllTariffsCommand implements Command{
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        List<Tariff> tariffs = FactoryService.getTariffService(em).getAllTariffs();
        List<TariffCommon> tariffCommons = new ArrayList<>();
        for(Tariff tariff : tariffs) {
            TariffCommon tariffCommon = EntityScan.scanServerTariff(tariff);
            tariffCommon.setOptions(EntityScan.scanServerOptions(tariff.getOptions()));
            tariffCommons.add(tariffCommon);
        }
        command.setContent((Serializable) tariffCommons);
        command.setStatus(true);
        return command;
    }

}
