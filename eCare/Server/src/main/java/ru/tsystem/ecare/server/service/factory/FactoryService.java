package ru.tsystem.ecare.server.service.factory;

import ru.tsystem.ecare.server.service.ContractService;
import ru.tsystem.ecare.server.service.OptionService;
import ru.tsystem.ecare.server.service.TariffService;
import ru.tsystem.ecare.server.service.UserService;
import ru.tsystem.ecare.server.service.impl.ContractServiceImpl;
import ru.tsystem.ecare.server.service.impl.OptionServiceImpl;
import ru.tsystem.ecare.server.service.impl.TariffServiceImpl;
import ru.tsystem.ecare.server.service.impl.UserServiceImpl;

import javax.persistence.EntityManager;

/**
 * Created by julua on 23.08.14.
 */
public class FactoryService {
    public static UserService getUserService(EntityManager em) {
        return new UserServiceImpl(em);
    }
    public static OptionService getOptionService(EntityManager em) {
        return new OptionServiceImpl(em);
    }
    public static TariffService getTariffService(EntityManager em) {
        return new TariffServiceImpl(em);
    }
    public static ContractService getContractService(EntityManager em) {
        return new ContractServiceImpl(em);
    }
}
