package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;

import javax.persistence.EntityManager;

/**
 * Created by julua on 23.08.14.
 */
public class UniqueContractCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        command.setContent(FactoryService.getContractService(em)
                .isUnique((String)command.getContent()));
        return command;
    }

}
