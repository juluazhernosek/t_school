package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 24.08.14.
 */
public class BlockContractCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        ContractCommon contractCommon = (ContractCommon) command.getContent();
        Contract contract = FactoryService.getContractService(em).blockUnblockContract(contractCommon.getId(),
                contractCommon.isActive(),contractCommon.isWhoBlocked());
        ContractCommon response = EntityScan.scanServerContract(contract);
        command.setContent(response);
        return command;
    }
}
