package ru.tsystem.ecare.server.service.impl;

import javax.persistence.EntityManager;

/**
 * Created by julua on 22.08.14.
 */
public abstract class AbstractService {
    protected EntityManager entityManager;

    public AbstractService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
