package ru.tsystem.ecare.server;

import ru.tsystem.ecare.server.connect.ServerConnect;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by julua on 22.08.14.
 */
public class ServerGui extends JFrame implements ActionListener {

    private boolean isStarted = false;
    private JPanel mainPanel;
    private JLabel status;
    private JButton startServer, stopServer;

    /**
     * Default class constructor
     */
    public ServerGui() {
        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLabels();
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(250, 300);
        this.setResizable(false);
        this.setLocation(800, 100);
        this.setTitle("eCare");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add fields for login and password
     */
    private void addLabels() {
        status = new JLabel("STATUS: Server is not start");
        status.setBounds(10, 230, 250, 20);
        mainPanel.add(status);

    }

    /**
     * Add buttons on frame
     */
    private void addButtons() {
        stopServer = new JButton("Stop server") ;
        stopServer.setBounds(50, 160, 150, 40);

        startServer = new JButton("Start server") ;
        startServer.setBounds(50, 90, 150, 40);

        mainPanel.add(stopServer);
        mainPanel.add(startServer);

        stopServer.addActionListener(this);
        startServer.addActionListener(this);
    }

    /**
     * Action for buttons
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == stopServer) {
            if(isStarted) {
                System.exit(0);
                isStarted = false;
                status.setText("STATUS: Server is not start");
            }
            else {
                JOptionPane.showMessageDialog(this, "Server is not started", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getSource() == startServer) {
            if(!isStarted){
                new Thread(new ServerConnect()).start();
                status.setText("STATUS: Server started");
                isStarted = true;
            }
            else
                JOptionPane.showMessageDialog(this, "Server has already started", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
