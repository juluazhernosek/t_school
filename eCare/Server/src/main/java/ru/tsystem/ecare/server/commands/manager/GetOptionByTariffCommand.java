package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 24.08.14.
 */
public class GetOptionByTariffCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        List<Option> serverOptions = FactoryService.getTariffService(em).getOptions(EntityScan.scanCommonTariff((TariffCommon) command.getContent()));
        List<OptionCommon> options = EntityScan.scanServerOptions(serverOptions);
        command.setContent((java.io.Serializable) options);
        return command;
    }

}
