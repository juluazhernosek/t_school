package ru.tsystem.ecare.server.connect;

import org.apache.log4j.Logger;
import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.manager.*;
import ru.tsystem.ecare.server.commands.signin.SignInCommand;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class ServerConnect implements Runnable {

    private static Logger logger = Logger.getLogger(ServerConnect.class);

    private ServerSocket serverSocket;
    private Thread t;
    private EntityManager em;
    private EntityManagerFactory emf;
    private Socket client;
    private HashMap<String,Command> commands;


    public ServerConnect() {
        emf = Persistence.createEntityManagerFactory("eCare");
        em = emf.createEntityManager();
        initCommands();
    }


    public void run() {
        try {
            serverSocket = new ServerSocket(2222);
            while (true) {
                client = serverSocket.accept();
                synchronized (em) {
                    logger.info("New connection to server");
                    new Thread(new ClientConnect(client, em,commands)).start();
                }
            }
        }
        catch(IOException e) {
            logger.error(e.getMessage());
            System.exit(-1);
        }
        finally {
            try {
                serverSocket.close();
                em.close();
                emf.close();
            }
            catch (IOException e){
                logger.error(e.getMessage());
            }
        }
    }

    private void initCommands() {
        commands = new HashMap<>();
        commands.put("SignIn",new SignInCommand());
        commands.put("CreateUser",new CreateUserCommand());
        commands.put("GetAllTariffs",new GetAllTariffsCommand());
        commands.put("IsUniqueEmailCommand", new UniqueUserCommand());
        commands.put("IsUniqueNumberContractCommand",new UniqueContractCommand());
        commands.put("GetContractsByUser",new GetContractsByUser());
        commands.put("UpdateUser",new UpdateUserCommand());
        commands.put("GetOptionsForTariff",new GetOptionByTariffCommand());
        commands.put("GetIncompatibleOptions",new GetIncompatibleOptionsCommand());
        commands.put("GetCompatibleOptions",new GetCompatibleOptionsCommand());
        commands.put("GetAllUsers",new GetAllUsersCommand());
        commands.put("BlockUnblockContract",new BlockContractCommand());
        commands.put("SearchUser",new SearchUserCommand());
        commands.put("RemoveTariff",new RemoveTariffCommand());
        commands.put("AddTariff",new AddTariffCommand());
        commands.put("IsUniqueTariffName",new IsUniqueTariffNameCommand());
        commands.put("IsUniqueOption",new IsUniqueOptionCommand());
        commands.put("AddOption",new AddOptionCommand());
        commands.put("RemoveOption",new RemoveOptionCommand());
        commands.put("AddContract",new AddContractCommand());
        commands.put("RemoveContract",new RemoveContractCommand());
        commands.put("SetIncompatibleOption",new SetIncompatibleOptionsCommand());
        commands.put("SetCompatibleOption",new SetCompatibleOptionsCommand());
        commands.put("UpdateContract",new UpdateContractCommand());
        commands.put("GetContractsAll",new GetContractsAllCommand());
    }
}
