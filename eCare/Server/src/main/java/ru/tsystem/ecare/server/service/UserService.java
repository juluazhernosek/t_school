package ru.tsystem.ecare.server.service;

import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystems.ecare.common.entities.UserCommon;

import java.util.List;
import java.util.Map;

/**
 * Created by julua on 22.08.14.
 */
public interface UserService {
    User signIn(String login, String password);
    User createUser(User user);
    List<User> getAllClients();
    List<User> getAllUnblockedClient();
    boolean isUnique(String login);
    List<Contract> getContracts(User user);
    List<Contract> getUnblockedContract(User user);
    User updateUser(User user);
}
