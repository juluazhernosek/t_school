package ru.tsystem.ecare.server.commands;

import ru.tsystems.ecare.common.commands.ICommand;

import javax.persistence.EntityManager;

/**
 * Created by julua on 22.08.14.
 */
public interface Command {
    ICommand execute(ICommand command, EntityManager em);
}
