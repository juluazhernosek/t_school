package ru.tsystem.ecare.server.service.impl;

import ru.tsystem.ecare.server.dao.FactoryDao;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.service.TariffService;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class TariffServiceImpl extends AbstractService implements TariffService {
    public TariffServiceImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Tariff> getAllTariffs() {
        return FactoryDao.getTariffDao(entityManager).getAllTariffs();
    }

    @Override
    public List<Option> getOptions(Tariff tariff) {
        return FactoryDao.getOptionDao(entityManager).getOptionsByTariff(tariff);
    }

    @Override
    public boolean removeTariff(long id) {
        entityManager.getTransaction().begin();
        Tariff tariff = entityManager.find(Tariff.class,id);
        List<Contract> contracts = FactoryDao.getContractDao(entityManager).getContractsByTariff(tariff);
        for(Contract contract :contracts) {
            contract.setTariff(entityManager.find(Tariff.class,1L));
            entityManager.merge(contract);
        }
        entityManager.remove(tariff);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public Tariff createTariff(Tariff tariff) {
        entityManager.getTransaction().begin();
        Tariff newTariff = entityManager.merge(tariff);
        entityManager.getTransaction().commit();
        return newTariff;
    }

    @Override
    public boolean isUnique(String name) {
        return FactoryDao.getTariffDao(entityManager).findByName(name) == null;
    }
}
