package ru.tsystem.ecare.server.service;

import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;

import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public interface TariffService {
    List<Tariff> getAllTariffs();
    List<Option> getOptions(Tariff tariff);
    boolean removeTariff(long id);
    Tariff createTariff(Tariff tariff);
    boolean isUnique(String name);
}
