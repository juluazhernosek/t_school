package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.OptionCommon;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 24.08.14.
 */
public class GetIncompatibleOptionsCommand implements Command{
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        OptionCommon optionCommon = (OptionCommon) command.getContent();
        Option option = EntityScan.scanCommonOption(optionCommon);
        List<Option> options = FactoryService.getOptionService(em).getIncompatibleOptions(option);
        List<OptionCommon> optionCommons = EntityScan.scanServerOptions(options);
        command.setContent((java.io.Serializable) optionCommons);
        return command;
    }
}
