package ru.tsystem.ecare.server.commands.signin;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 22.08.14.
 */
public class SignInCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        UserCommon userCommon = EntityScan.scanServerUser(FactoryService.getUserService(em)
                .signIn(((UserCommon)command.getContent()).getEmail(),((UserCommon)command.getContent()).getPassword()));
        command.setContent(userCommon);
        command.setStatus(true);
        return command;
    }

}
