package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.TariffCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 24.08.14.
 */
public class AddTariffCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        TariffCommon tariffCommon = (TariffCommon) command.getContent();
        Tariff tariff = EntityScan.scanCommonTariff(tariffCommon);
        Tariff response = FactoryService.getTariffService(em).createTariff(tariff);
        command.setContent(EntityScan.scanServerTariff(response));
        return command;
    }
}
