package ru.tsystem.ecare.server.dao.impl;

import ru.tsystem.ecare.server.dao.OptionDao;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class OptionDaoImpl extends JpaDao implements OptionDao {
    public OptionDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public List<Option> getOptionsByTariff(Tariff tariff) {
        tariff = em.find(Tariff.class,tariff.getId());
        return tariff.getOptions();
    }

    @Override
    public Option findOptionByTariff(String name, Tariff tariff) {
        return getSingleResult(em.createQuery("SELECT o FROM Option o WHERE o.name=:name AND o.tariff=:tariff",Option.class)
                .setParameter("name",name)
                .setParameter("tariff",tariff)
                .getResultList());
    }
}
