package ru.tsystem.ecare.server.dao.impl;

import ru.tsystem.ecare.server.dao.UserDao;
import ru.tsystem.ecare.server.entities.User;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class UserDaoImpl extends JpaDao implements UserDao {
    public UserDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public User login(String login, String password) {
        return getSingleResult(em.createQuery("SELECT u FROM User u WHERE u.email=:login AND u.password=:password",User.class)
                .setParameter("login",login)
                .setParameter("password",password)
                .getResultList());
    }

    @Override
    public User createUser(User user) {
        em.persist(user);
        em.refresh(user);
        return user;
    }

    @Override
    public User findUserByLogin(String login) {
        return getSingleResult(em.createQuery("SELECT u FROM User u WHERE u.email = :login", User.class)
                .setParameter("login", login)
                .getResultList());
    }

    @Override
    public User updateUser(User user) {
        return em.merge(user);
    }

    @Override
    public List<User> getAllUserByRole(boolean role) {
        return em.createQuery("SELECT u FROM User u WHERE u.role=:role", User.class)
                .setParameter("role",role).getResultList();
    }

}
