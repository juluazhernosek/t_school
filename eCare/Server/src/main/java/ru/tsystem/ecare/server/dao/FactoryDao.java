package ru.tsystem.ecare.server.dao;

import ru.tsystem.ecare.server.dao.impl.ContractDaoImpl;
import ru.tsystem.ecare.server.dao.impl.OptionDaoImpl;
import ru.tsystem.ecare.server.dao.impl.TariffDaoImpl;
import ru.tsystem.ecare.server.dao.impl.UserDaoImpl;

import javax.persistence.EntityManager;

/**
 * Created by julua on 22.08.14.
 */
public class FactoryDao {
    public static ContractDao getContractDao(EntityManager em) {
        return new ContractDaoImpl(em);
    }
    public static TariffDao getTariffDao(EntityManager em) {
        return new TariffDaoImpl(em);
    }
    public static OptionDao getOptionDao(EntityManager em) {
        return new OptionDaoImpl(em);
    }
    public static UserDao getUserDao(EntityManager em) {
        return new UserDaoImpl(em);
    }
}
