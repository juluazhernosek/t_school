package ru.tsystem.ecare.server.service.impl;

import ru.tsystem.ecare.server.dao.FactoryDao;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.service.OptionService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class OptionServiceImpl extends AbstractService implements OptionService {
    public OptionServiceImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Option> getCompatibleOptions(Option option) {
        option = entityManager.find(Option.class,option.getId());
        return option.getCompatibleOptions();
    }

    @Override
    public List<Option> getIncompatibleOptions(Option option) {
        option = entityManager.find(Option.class,option.getId());
        return option.getIncompatibleOptions();
    }

    @Override
    public boolean isUnique(Option option) {
        Tariff tariff = entityManager.find(Tariff.class,option.getTariff().getId());
        return FactoryDao.getOptionDao(entityManager).findOptionByTariff(option.getName(),tariff) == null;
    }

    @Override
    public Option createOption(Option option) {
        entityManager.getTransaction().begin();
        Tariff tariff = entityManager.find(Tariff.class,option.getTariff().getId());
        option.setTariff(tariff);
        option = entityManager.merge(option);
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return option;
    }

    @Override
    public boolean removeOption(long id) {
        entityManager.getTransaction().begin();
        Option option = entityManager.find(Option.class,id);
        entityManager.remove(option);
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return true;
    }

    @Override
    public boolean setCompatible(Option option, Option optionCompatible) {
        entityManager.getTransaction().begin();
        option = entityManager.find(Option.class,option.getId());
        optionCompatible = entityManager.find(Option.class,optionCompatible.getId());
        List<Option> incompatibleOptions = option.getIncompatibleOptions();
        if(option.getCompatibleOptions() == null) {
            option.setCompatibleOptions(new ArrayList<Option>());
        }
        for(Option opt : option.getCompatibleOptions()) {
            if(opt.getId() == optionCompatible.getId()) {
                entityManager.getTransaction().rollback();
                return false;
            }
        }
        if(incompatibleOptions != null) {
            for(Option opt : incompatibleOptions)
                if(opt.getId() == optionCompatible.getId()) {
                    entityManager.getTransaction().rollback();
                    return false;
                }
            option.getCompatibleOptions().add(optionCompatible);
        }
        else {
            option.getCompatibleOptions().add(optionCompatible);
        }
        entityManager.merge(option);
        entityManager.flush();
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean setIncompatible(Option option, Option optionIncompatible) {
        entityManager.getTransaction().begin();
        option = entityManager.find(Option.class,option.getId());
        optionIncompatible = entityManager.find(Option.class,optionIncompatible.getId());
        List<Option> compatibleOptions = option.getCompatibleOptions();
        if(option.getIncompatibleOptions() == null) {
            option.setIncompatibleOptions(new ArrayList<Option>());
        }
        for(Option opt : option.getIncompatibleOptions()) {
            if(opt.getId() == optionIncompatible.getId()) {
                entityManager.getTransaction().rollback();
                return false;
            }
        }
        if(compatibleOptions != null) {
            for(Option opt : compatibleOptions)
                if(opt.getId() == optionIncompatible.getId()) {
                    entityManager.getTransaction().rollback();
                    return false;
                }
            option.getIncompatibleOptions().add(optionIncompatible);
        }
        else {
            option.getIncompatibleOptions().add(optionIncompatible);
        }
        entityManager.merge(option);
        entityManager.flush();
        entityManager.getTransaction().commit();
        return true;
    }
}
