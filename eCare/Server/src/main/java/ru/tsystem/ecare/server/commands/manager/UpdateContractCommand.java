package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.ContractCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 25.08.14.
 */
public class UpdateContractCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        ContractCommon contractCommon = (ContractCommon) command.getContent();
        Contract contract = EntityScan.scanCommonContract(contractCommon);
        contract.setTariff(EntityScan.scanCommonTariff(contractCommon.getTariff()));
        contract.setSelectedOptions(EntityScan.scanCommonOptions(contractCommon.getSelectedOptions()));
        contract.setUser(EntityScan.scanCommonUser(contractCommon.getUser()));
        command.setContent(EntityScan.scanServerContract(FactoryService.getContractService(em).updateContract(contract)));
        return command;
    }
}
