package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.OptionCommon;

import javax.persistence.EntityManager;
import java.util.ArrayList;

/**
 * Created by julua on 25.08.14.
 */
public class SetCompatibleOptionsCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        OptionCommon requestOption = (OptionCommon) command.getContent();
        Option serverOption = EntityScan.scanCommonOption(requestOption);
        serverOption.setCompatibleOptions(new ArrayList<Option>());
        Option optionCompatible = EntityScan.scanCommonOption(requestOption.getCompatibleOptions().get(0));
        command.setContent(FactoryService.getOptionService(em).setCompatible(serverOption,optionCompatible));
        return command;
    }
}
