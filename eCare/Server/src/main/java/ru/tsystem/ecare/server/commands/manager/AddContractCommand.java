package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.ContractCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 25.08.14.
 */
public class AddContractCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        ContractCommon contractCommon = (ContractCommon) command.getContent();
        Contract contract = EntityScan.scanCommonContract(contractCommon);
        contract.setUser(EntityScan.scanCommonUser(contractCommon.getUser()));
        contract.setTariff(EntityScan.scanCommonTariff(contractCommon.getTariff()));
        contract.setSelectedOptions(EntityScan.scanCommonOptions(contractCommon.getSelectedOptions()));
        Contract serverContract = FactoryService.getContractService(em).createContract(contract);
        ContractCommon response = EntityScan.scanServerContract(serverContract);
        command.setContent(response);
        return command;
    }
}
