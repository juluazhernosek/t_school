package ru.tsystem.ecare.server.dao;

import ru.tsystem.ecare.server.entities.Tariff;

import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public interface TariffDao {
    List<Tariff> getAllTariffs();
    Tariff findByName(String name);
}
