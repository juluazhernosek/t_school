package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.OptionCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 24.08.14.
 */
public class AddOptionCommand implements Command{
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        OptionCommon optionCommon = (OptionCommon) command.getContent();
        Option option = EntityScan.scanCommonOption(optionCommon);
        option.setTariff(EntityScan.scanCommonTariff(optionCommon.getTariff()));
        Option response = FactoryService.getOptionService(em).createOption(option);
        OptionCommon responseCommon = EntityScan.scanServerOption(response);
        command.setContent(responseCommon);
        return command;
    }
}
