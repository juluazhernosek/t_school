package ru.tsystem.ecare.server.commands;

import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.OptionCommon;
import ru.tsystems.ecare.common.entities.TariffCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import java.util.*;

/**
 * Created by julua on 22.08.14.
 */
public class EntityScan {

    // USER

    public static User scanCommonUser(UserCommon userService) {
        if(userService == null) return null;
        User newUser = new User();
        newUser.setId(userService.getId()); newUser.setName(userService.getName()); newUser.setAddress(userService.getAddress());
        newUser.setBirthdayDate(userService.getBirthdayDate()); newUser.setEmail(userService.getEmail());
        newUser.setPassword(userService.getPassword()); newUser.setPassport(userService.getPassport());
        newUser.setSurname(userService.getSurname());
        newUser.setRole("Manager".equals(userService.getRole()));
        return newUser;
    }

    public static UserCommon scanServerUser(User userService) {
        if(userService == null) return null;
        UserCommon newUser = new UserCommon();
        newUser.setId(userService.getId()); newUser.setName(userService.getName()); newUser.setAddress(userService.getAddress());
        newUser.setBirthdayDate(userService.getBirthdayDate()); newUser.setEmail(userService.getEmail());
        newUser.setPassword(userService.getPassword()); newUser.setPassport(userService.getPassport());
        newUser.setSurname(userService.getSurname());
        newUser.setRole(userService.isRole() ? "Manager" : "Client");
        return newUser;
    }


    public static List<User> scanCommonUsers (List<UserCommon> optionCommons) {
        if(optionCommons == null) return null;
        List<User> newUsers = new ArrayList<>();
        for(UserCommon userCommon : optionCommons) {
            newUsers.add(scanCommonUser(userCommon));
        }
        return newUsers;
    }

    public static List<UserCommon> scanServerUsers (List<User> optionCommons) {
        if(optionCommons == null) return null;
        List<UserCommon> newUsers = new ArrayList<>();
        for(User userCommon : optionCommons) {
            newUsers.add(scanServerUser(userCommon));
        }
        return newUsers;
    }



    // END USER


    // TARIFF

    public static Tariff scanCommonTariff(TariffCommon tariffCommon) {
        if(tariffCommon == null) return null;
        Tariff tariff = new Tariff();
        tariff.setId(tariffCommon.getId());
        tariff.setName(tariffCommon.getName());
        tariff.setPrice(tariffCommon.getPrice());
        return tariff;
    }

    public static TariffCommon scanServerTariff(Tariff tariff) {
        if(tariff == null) return null;
        TariffCommon tariffCommon = new TariffCommon();
        tariffCommon.setId(tariff.getId());
        tariffCommon.setName(tariff.getName());
        tariffCommon.setPrice(tariff.getPrice());
        return tariffCommon;
    }


    // END TARIFF

    // CONTRACTS

    public static List<ContractCommon> scanServerContracts(List<Contract> contractCommons) {
        if(contractCommons == null) return null;
        List<ContractCommon> newContracts = new ArrayList<>();
        for(Contract contractCommon : contractCommons) {
            newContracts.add(scanServerContract(contractCommon));
        }
        return newContracts;
    }

    public static ContractCommon scanServerContract(Contract contract) {
        if(contract == null) return null;
        ContractCommon newContract = new ContractCommon();
        newContract.setId(contract.getId());
        newContract.setActive(contract.isActive());
        newContract.setPhoneNumber(contract.getPhoneNumber());
        newContract.setWhoBlocked(contract.isWhoBlocked());
        return newContract;
    }

    public static List<Contract> scanCommonContracts(List<ContractCommon> contractCommons) {
        if(contractCommons == null) return null;
        List<Contract> newContracts = new ArrayList<>();
        for(ContractCommon contractCommon : contractCommons) {
            newContracts.add(scanCommonContract(contractCommon));
        }
        return newContracts;
    }

    public static Contract scanCommonContract(ContractCommon contract) {
        if(contract == null) return null;
        Contract newContract = new Contract();
        newContract.setId(contract.getId());
        newContract.setActive(contract.isActive());
        newContract.setPhoneNumber(contract.getPhoneNumber());
        newContract.setWhoBlocked(contract.isWhoBlocked());
        return newContract;
    }


    // END CONTRACTS


    // OPTIONS

   public static List<Option> scanCommonOptions (List<OptionCommon> optionCommons) {
       if(optionCommons == null) return null;
       List<Option> newOptions = new ArrayList<>();
       for(OptionCommon optionCommon : optionCommons) {
           newOptions.add(scanCommonOption(optionCommon));
       }
       return newOptions;
   }


    public static Option scanCommonOption(OptionCommon option) {
        if(option == null) return null;
        Option opt = new Option();
        opt.setId(option.getId());
        opt.setName(option.getName());
        opt.setCost(option.getCost());
        opt.setPrice(option.getPrice());
        return opt;
    }

    public static List<OptionCommon> scanServerOptions (List<Option> options) {
        if(options == null) return null;
        List<OptionCommon> newOptions = new ArrayList<>();
        for(Option option : options) {
            newOptions.add(scanServerOption(option));
        }
        return newOptions;
    }

    public static OptionCommon scanServerOption(Option option) {
        if(option == null) return null;
        OptionCommon opt = new OptionCommon();
        opt.setId(option.getId());
        opt.setName(option.getName());
        opt.setCost(option.getCost());
        opt.setPrice(option.getPrice());
        return opt;
    }

    //END OPTIONS
}
