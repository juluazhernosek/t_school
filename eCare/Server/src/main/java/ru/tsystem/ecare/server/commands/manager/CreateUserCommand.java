package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 23.08.14.
 */
public class CreateUserCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        UserCommon userCommon = (UserCommon) command.getContent();
        User userScan = EntityScan.scanCommonUser(userCommon);
        List<Contract> contracts = EntityScan.scanCommonContracts(userCommon.getContracts());
        for(int i = 0; i< contracts.size(); i++) {
            contracts.get(i).setSelectedOptions(
                    EntityScan.scanCommonOptions(userCommon.getContracts().get(i).getSelectedOptions()));
            contracts.get(i).setTariff(EntityScan.scanCommonTariff(userCommon.getContracts().get(i).getTariff()));
        }
        userScan.setContracts(contracts);
        User user = FactoryService.getUserService(em).createUser(userScan);
        command.setContent(user!=null);
        command.setStatus(true);
        return command;
    }

}
