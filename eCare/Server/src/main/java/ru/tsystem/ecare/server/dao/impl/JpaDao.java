package ru.tsystem.ecare.server.dao.impl;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class JpaDao {

    protected EntityManager em;

    public JpaDao (EntityManager em) {
        this.em = em;
    }

    protected <T> T getSingleResult(List<T> list) {
        return list == null || list.isEmpty() ? null : list.get(0);
    }
}
