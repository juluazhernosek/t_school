package ru.tsystem.ecare.server.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by julua on 14.08.14.
 */
@Entity
@Table(name = "options")
public class Option {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column (name = "cost")
    private double cost;

    @Column (name = "price")
    private double price;

    @ManyToOne
    @JoinColumn(name = "id_tariff")
    private Tariff tariff;

    @JoinTable(name = "compatible_options", joinColumns = {
            @JoinColumn(name = "id_option", referencedColumnName = "id", nullable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "id_compatible_option", referencedColumnName = "id", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Option> compatibleOptions;


    @JoinTable(name = "incompatible_options", joinColumns = {
            @JoinColumn(name = "id_option", referencedColumnName = "id", nullable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "id_incompatible_option", referencedColumnName = "id", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Option> incompatibleOptions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Option> getCompatibleOptions() {
        return compatibleOptions;
    }

    public void setCompatibleOptions(List<Option> compatibleOptions) {
        this.compatibleOptions = compatibleOptions;
    }

    public List<Option> getIncompatibleOptions() {
        return incompatibleOptions;
    }

    public void setIncompatibleOptions(List<Option> incompatibleOptions) {
        this.incompatibleOptions = incompatibleOptions;
    }

    public boolean equals (Object o) {
        return o!= null && o instanceof Option && ((Option)o).getId() == id &&
                ((Option)o).name != null && ((Option)o).name.equals(name)
                && ((Option)o).price == price && ((Option)o).cost == cost;
    }

    public int hashCode() {
        return (int) id;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }
}
