package ru.tsystem.ecare.server.dao;

import ru.tsystem.ecare.server.entities.User;

import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public interface UserDao {
    User login(String login, String password);
    User createUser(User user);
    User findUserByLogin(String login);
    User updateUser(User user);
    List<User> getAllUserByRole(boolean role);
}
