package ru.tsystem.ecare.server.dao.impl;

import ru.tsystem.ecare.server.dao.TariffDao;
import ru.tsystem.ecare.server.entities.Tariff;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class TariffDaoImpl extends JpaDao implements TariffDao {
    public TariffDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public List<Tariff> getAllTariffs() {
        return em.createQuery("SELECT t FROM Tariff t",Tariff.class).getResultList();
    }

    @Override
    public Tariff findByName(String name) {
        return getSingleResult(em.createQuery("SELECT t FROM Tariff t WHERE name=:name",Tariff.class)
                                .setParameter("name",name).getResultList());
    }
}
