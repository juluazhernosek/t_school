package ru.tsystem.ecare.server.service.impl;

import ru.tsystem.ecare.server.dao.FactoryDao;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystem.ecare.server.service.ContractService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public class ContractServiceImpl extends AbstractService implements ContractService {
    public ContractServiceImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public boolean isUnique(String number) {
        return FactoryDao.getContractDao(entityManager).getContractByNumber(number) == null;
    }

    @Override
    public Contract blockUnblockContract(long id, boolean status, boolean whoBlocked) {
        entityManager.getTransaction().begin();
        Contract contract = entityManager.find(Contract.class,id);
        contract.setWhoBlocked(whoBlocked);
        contract.setActive(status);
        contract = entityManager.merge(contract);
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return contract;

    }

    @Override
    public Contract findByNumber(String number) {
        return FactoryDao.getContractDao(entityManager).getContractByNumber(number);
    }

    @Override
    public Contract createContract(Contract contract) {
        entityManager.getTransaction().begin();
        Tariff tariff = entityManager.find(Tariff.class,contract.getTariff().getId());
        List<Option> options = new ArrayList<>();
        for(Option option : contract.getSelectedOptions())
            options.add(entityManager.find(Option.class,option.getId()));
        contract.setSelectedOptions(options);
        User user = entityManager.find(User.class, contract.getUser().getId());
        contract.setUser(user);
        Contract newContract = entityManager.merge(contract);
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return newContract;
    }

    @Override
    public boolean removeContract(long id) {
        entityManager.getTransaction().begin();
        Contract contract = entityManager.find(Contract.class,id);
        entityManager.remove(contract);
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return true;
    }



    @Override
    public Contract updateContract(Contract contract) {
        entityManager.getTransaction().begin();
        Contract serverContract = entityManager.find(Contract.class,contract.getId());
        serverContract.setTariff(entityManager.find(Tariff.class,contract.getTariff().getId()));
        if(serverContract.getSelectedOptions() == null)
            serverContract.setSelectedOptions(new ArrayList<Option>());
        else
            serverContract.getSelectedOptions().clear();
        for(Option option : contract.getSelectedOptions()) {
            Option findOption = entityManager.find(Option.class,option.getId());
            if(findOption == null)
                findOption = entityManager.merge(option);
            serverContract.getSelectedOptions().add(findOption);
        }
        for(Option option : serverContract.getSelectedOptions()) {
            if(option.getCompatibleOptions() != null && !option.getCompatibleOptions().isEmpty()) {
                for(Option o : serverContract.getSelectedOptions()) {
                    if(!option.getCompatibleOptions().contains(o)) {
                        entityManager.getTransaction().rollback();
                        return null;
                    }
                }
            }
            if(option.getIncompatibleOptions() != null) {
                for(Option o : serverContract.getSelectedOptions()) {
                    if(option.getIncompatibleOptions().contains(o)) {
                        entityManager.getTransaction().rollback();
                        return null;
                    }
                }
            }
        }
        Contract response = entityManager.merge(serverContract);
        entityManager.getTransaction().commit();
        return response;
    }


}
