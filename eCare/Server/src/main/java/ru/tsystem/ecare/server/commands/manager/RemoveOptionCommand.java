package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;

import javax.persistence.EntityManager;

/**
 * Created by julua on 24.08.14.
 */
public class RemoveOptionCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        long id = (long) command.getContent();
        boolean response = FactoryService.getOptionService(em).removeOption(id);
        command.setContent(response);
        return command;
    }
}
