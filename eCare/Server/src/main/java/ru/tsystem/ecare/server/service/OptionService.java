package ru.tsystem.ecare.server.service;

import ru.tsystem.ecare.server.entities.Option;

import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public interface OptionService {
    List<Option> getCompatibleOptions(Option option);
    List<Option> getIncompatibleOptions(Option option);
    boolean isUnique(Option option);
    Option createOption(Option option);
    boolean removeOption(long id);
    boolean setCompatible(Option option, Option optionCompatible);
    boolean setIncompatible(Option option, Option optionIncompatible);
}
