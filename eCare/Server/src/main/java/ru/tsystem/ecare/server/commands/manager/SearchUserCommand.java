package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.persistence.EntityManager;

/**
 * Created by julua on 24.08.14.
 */
public class SearchUserCommand implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        String number = (String) command.getContent();
        Contract contract = FactoryService.getContractService(em).findByNumber(number);
        User user = contract == null ? null : contract.getUser();
        UserCommon response = EntityScan.scanServerUser(user);
        command.setContent(response);
        return command;
    }
}
