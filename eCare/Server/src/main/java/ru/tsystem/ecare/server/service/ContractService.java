package ru.tsystem.ecare.server.service;

import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.entities.User;

import java.util.List;

/**
 * Created by julua on 22.08.14.
 */
public interface ContractService {
    boolean isUnique(String number);
    Contract blockUnblockContract(long id, boolean status,boolean whoBlocked);
    Contract findByNumber(String number);
    Contract createContract(Contract contract);
    boolean removeContract(long id);
    Contract updateContract(Contract contract);
}
