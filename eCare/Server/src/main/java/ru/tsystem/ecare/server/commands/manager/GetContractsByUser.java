package ru.tsystem.ecare.server.commands.manager;

import ru.tsystem.ecare.server.commands.Command;
import ru.tsystem.ecare.server.commands.EntityScan;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystem.ecare.server.service.factory.FactoryService;
import ru.tsystems.ecare.common.commands.ICommand;
import ru.tsystems.ecare.common.entities.ContractCommon;
import ru.tsystems.ecare.common.entities.UserCommon;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * Created by julua on 23.08.14.
 */
public class GetContractsByUser implements Command {
    @Override
    public ICommand execute(ICommand command, EntityManager em) {
        User user = EntityScan.scanCommonUser((UserCommon) command.getContent());
        List<Contract> contracts = FactoryService.getUserService(em).getUnblockedContract(user);
        List<ContractCommon> contractsCommon = EntityScan.scanServerContracts(contracts);
        for(int i=0; i<contracts.size(); i++) {
            contractsCommon.get(i).setSelectedOptions(EntityScan.scanServerOptions(contracts.get(i).getSelectedOptions()));
            contractsCommon.get(i).setTariff(EntityScan.scanServerTariff(contracts.get(i).getTariff()));
        }
        command.setContent((Serializable) contractsCommon);
        return command;
    }

}
