package ru.tsystem.ecare.server.service.impl;

import org.hibernate.Hibernate;
import ru.tsystem.ecare.server.dao.ContractDao;
import ru.tsystem.ecare.server.dao.FactoryDao;
import ru.tsystem.ecare.server.dao.TariffDao;
import ru.tsystem.ecare.server.dao.UserDao;
import ru.tsystem.ecare.server.entities.Contract;
import ru.tsystem.ecare.server.entities.Option;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.entities.User;
import ru.tsystem.ecare.server.service.UserService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by julua on 22.08.14.
 */
public class UserServiceImpl extends AbstractService implements UserService {
    public UserServiceImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public User signIn(String login,String password) {
        return FactoryDao.getUserDao(entityManager).login(login,password);
    }

    @Override
    public User createUser(User user) {
        entityManager.getTransaction().begin();
        UserDao userDao = FactoryDao.getUserDao(entityManager);
        if(userDao.findUserByLogin(user.getEmail()) != null) {
            entityManager.getTransaction().rollback();
            return null;
        }
        if(user.getContracts() != null) {
            ContractDao contractDao = FactoryDao.getContractDao(entityManager);
            for(Contract contract : user.getContracts()) {
                if(contractDao.getContractByNumber(contract.getPhoneNumber()) != null) {
                    entityManager.getTransaction().rollback();
                    return null;
                }
                Contract serverContract = new Contract();
                if(serverContract.getSelectedOptions() == null)
                    serverContract.setSelectedOptions(new ArrayList<Option>());
                else
                    serverContract.getSelectedOptions().clear();
                for(Option option : contract.getSelectedOptions()) {
                    Option findOption = entityManager.find(Option.class,option.getId());
                    if(findOption == null)
                        findOption = entityManager.merge(option);
                    serverContract.getSelectedOptions().add(findOption);
                }
                for(Option option : serverContract.getSelectedOptions()) {
                    if(option.getCompatibleOptions() != null && !option.getCompatibleOptions().isEmpty()) {
                        for(Option o : serverContract.getSelectedOptions()) {
                            if(!option.getCompatibleOptions().contains(o)) {
                                entityManager.getTransaction().rollback();
                                return null;
                            }
                        }
                    }
                    if(option.getIncompatibleOptions() != null) {
                        for(Option o : serverContract.getSelectedOptions()) {
                            if(option.getIncompatibleOptions().contains(o)) {
                                entityManager.getTransaction().rollback();
                                return null;
                            }
                        }
                    }
                }
//                entityManager.merge(contract);
                contract.setUser(user);
            }
        }
        User newUser = userDao.updateUser(user);
        entityManager.getTransaction().commit();
        return newUser;
    }

    @Override
    public List<User> getAllClients() {
        return FactoryDao.getUserDao(entityManager).getAllUserByRole(false);
    }

    @Override
    public List<User> getAllUnblockedClient() {
        return null;
    }

    @Override
    public boolean isUnique(String login) {
        return FactoryDao.getUserDao(entityManager).findUserByLogin(login) == null;
    }

    @Override
    public List<Contract> getContracts(User user) {
        user = entityManager.find(User.class,user.getId());
        return user.getContracts();

    }

    @Override
    public List<Contract> getUnblockedContract(User user) {
        user = entityManager.find(User.class,user.getId());
        List<Contract> contracts = new ArrayList<>();
        if(user.getContracts() != null) {
            for(Contract contract : user.getContracts()) {
                if(contract.isActive())
                    contracts.add(contract);
            }
        }
        return contracts;
    }

    @Override
    public User updateUser(User user) {
        entityManager.getTransaction().begin();
        User newUser = entityManager.merge(user);
        entityManager.getTransaction().commit();
        return newUser;
    }

}
