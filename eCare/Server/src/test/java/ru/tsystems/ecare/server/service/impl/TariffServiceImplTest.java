package ru.tsystems.ecare.server.service.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.tsystem.ecare.server.entities.Tariff;
import ru.tsystem.ecare.server.service.impl.TariffServiceImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by dsing on 25.08.14.
 */

public class TariffServiceImplTest {

    private EntityManager em;
    private EntityManagerFactory emf;
    private TariffServiceImpl tariffServiceImpl;

    public TariffServiceImplTest(){
        emf = Persistence.createEntityManagerFactory("eCareTest");
        em = emf.createEntityManager();
        tariffServiceImpl = new TariffServiceImpl(em);
    }

    @Before
    public void insertIntoDb(){
        em.getTransaction().begin();
        Tariff tariff = new Tariff();
        tariff.setName("firstTariff");
        tariff.setPrice(200);
        em.merge(tariff);
        em.flush();
        em.getTransaction().commit();
    }

    @Test
    public void getAllTariffsNotNullTest(){
        assertNotNull(tariffServiceImpl.getAllTariffs());
    }

//    @Test
//    public void isUniqueTest(){
//        assertTrue(tariffServiceImpl.isUnique("olo-lo-lo"));
//    }
//
//    @Test
//    public void negativeIsUniqueTest(){
//        Tariff tariff = new Tariff();
//        tariff.setName("firstTariff");
//        tariff.setPrice(200);
//        assertFalse(tariffServiceImpl.isUnique(tariff.getName()));
//    }

   @After
    public void removeFromDb(){
        Tariff tariff = em.find(Tariff.class, 1L);
        em.remove(tariff);
    }
}
