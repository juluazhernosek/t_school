DROP database IF EXISTS eCare;

CREATE DATABASE eCare;

use eCare;

CREATE TABLE tariff (
	id INT NOT NULL AUTO_INCREMENT ,
	name VARCHAR(20) NOT NULL UNIQUE,
	price DOUBLE NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE options (
	id  INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL,
	cost DOUBLE NOT NULL,
	price DOUBLE NOT NULL,
  id_tariff INT DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_tariff) REFERENCES tariff(id)
);

CREATE TABLE compatible_options (
  id_option INT NOT NULL,
  id_compatible_option INT NOT NULL,
  FOREIGN KEY (id_option) REFERENCES options(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (id_compatible_option) REFERENCES options(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE incompatible_options (
  id_option INT NOT NULL,
  id_incompatible_option INT NOT NULL,
  FOREIGN KEY (id_option) REFERENCES options(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (id_incompatible_option) REFERENCES options(id) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT ,
	name VARCHAR(20) NOT NULL,
	surname VARCHAR(20) NOT NULL,
	birthday DATE,
	passport VARCHAR(50),
	address VARCHAR(50),
	email VARCHAR(25) NOT NULL UNIQUE,
	password VARCHAR(25) NOT NULL,
	role BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
) ENGINE=InnoDB CHARACTER SET=UTF8;


CREATE TABLE contracts(
	id INT NOT NULL AUTO_INCREMENT,
	phone_number VARCHAR(20) NOT NULL UNIQUE ,
	is_active BOOLEAN DEFAULT TRUE,
  who_blocked BOOLEAN,
	id_tariff INT DEFAULT NULL,
  id_user INT DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (id_tariff) REFERENCES tariff(id) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE selected_options(
	id_contract INT NOT NULL,
	id_option INT NOT NULL,
	FOREIGN KEY (id_contract) REFERENCES contracts(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (id_option) REFERENCES options(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARACTER SET=UTF8;