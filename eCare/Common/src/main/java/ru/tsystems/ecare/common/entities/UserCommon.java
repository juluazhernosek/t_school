package ru.tsystems.ecare.common.entities;

import ru.tsystems.ecare.common.IObject;

import java.util.Date;
import java.util.List;

/**
 * Created by julua on 15.08.14.
 */

public class UserCommon implements IObject, Cloneable {
    private long id;

    private String name;

    private String surname;

    private Date birthdayDate;

    private String passport;

    private String address;

    private String email;

    private String password;

    private String role;

    private String regDate;

    private List<ContractCommon> contracts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Date birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<ContractCommon> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractCommon> contracts) {
        this.contracts = contracts;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
