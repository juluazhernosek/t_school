package ru.tsystems.ecare.common.entities;

import ru.tsystems.ecare.common.IObject;

import java.util.List;

/**
 * Created by julua on 15.08.14.
 */
public class ContractCommon implements IObject {
    private long id;

    private String phoneNumber;

    private boolean isActive;

    private boolean whoBlocked;

    private UserCommon user;

    private TariffCommon tariff;


    private List<OptionCommon> selectedOptions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isWhoBlocked() {
        return whoBlocked;
    }

    public void setWhoBlocked(boolean whoBlocked) {
        this.whoBlocked = whoBlocked;
    }


    public UserCommon getUser() {
        return user;
    }

    public void setUser(UserCommon user) {
        this.user = user;
    }


    public List<OptionCommon> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(List<OptionCommon> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public TariffCommon getTariff() {
        return tariff;
    }

    public void setTariff(TariffCommon tariff) {
        this.tariff = tariff;
    }

    @Override
    public String toString() {
        return phoneNumber;
    }

    public boolean equals(Object c) {
        return c != null && c instanceof ContractCommon && ((ContractCommon)c).getPhoneNumber() != null
                && ((ContractCommon)c).getPhoneNumber().equals(this.phoneNumber);
    }
}
