package ru.tsystems.ecare.common.entities;

import ru.tsystems.ecare.common.IObject;

import java.util.List;

/**
 * Created by julua on 14.08.14.
 */

public class OptionCommon implements IObject{

    private long id;


    private String name;

    private double cost;

    private double price;

    private TariffCommon tariff;


    private List<OptionCommon> compatibleOptions;

    private List<OptionCommon> incompatibleOptions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<OptionCommon> getCompatibleOptions() {
        return compatibleOptions;
    }

    public void setCompatibleOptions(List<OptionCommon> compatibleOptions) {
        this.compatibleOptions = compatibleOptions;
    }

    public List<OptionCommon> getIncompatibleOptions() {
        return incompatibleOptions;
    }

    public void setIncompatibleOptions(List<OptionCommon> incompatibleOptions) {
        this.incompatibleOptions = incompatibleOptions;
    }


    public boolean equals (Object o) {
        return o!= null && o instanceof OptionCommon && ((OptionCommon)o).getId() == id &&
                ((OptionCommon)o).name != null && ((OptionCommon)o).name.equals(name)
                && ((OptionCommon)o).price == price && ((OptionCommon)o).cost == cost;
    }

    public int hashCode() {
        return (int) id;
    }

    @Override
    public  String toString(){
        return name;
    }

    public TariffCommon getTariff() {
        return tariff;
    }

    public void setTariff(TariffCommon tariff) {
        this.tariff = tariff;
    }
}
