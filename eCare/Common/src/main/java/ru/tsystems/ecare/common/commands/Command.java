package ru.tsystems.ecare.common.commands;

import java.io.Serializable;

/**
 * Created by julua on 17.08.14.
 */
public class Command implements ICommand {
    
    private String name;
    private Serializable content;
    private boolean status;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean getStatus() {
        return status;
    }

    @Override
    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public Serializable getContent() {
        return content;
    }

    @Override
    public void setContent(Serializable content) {
        this.content = content;
    }
}
