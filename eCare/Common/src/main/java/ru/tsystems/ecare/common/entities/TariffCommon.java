package ru.tsystems.ecare.common.entities;

import ru.tsystems.ecare.common.IObject;

import java.util.List;

/**
 * Created by julua on 14.08.14.
 */

public class TariffCommon implements IObject {

    private long id;

    private String name;

    private double price;

    private List<OptionCommon> options;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<OptionCommon> getOptions() {
        return options;
    }

    public void setOptions(List<OptionCommon> options) {
        this.options = options;
    }

    public boolean equals(Object c) {
        return c != null && c instanceof TariffCommon
                && ((TariffCommon)c).id == id
                && ((TariffCommon)c).name != null
                && ((TariffCommon)c).name.equals(name)
                && ((TariffCommon)c).price == price;
    }

    @Override
    public String toString() {
        return name;
    }
}
