package ru.tsystems.ecare.common.commands;

import java.io.Serializable;

/**
 * Created by julua on 17.08.14.
 */
public interface ICommand extends Serializable{

    String getName();

    void setName(String name);

    boolean getStatus();

    void setStatus(boolean status);

    Serializable getContent();

    void setContent(Serializable content);
}
