package ru.tsystems.ecare.dto;

import ru.tsystems.ecare.entities.Option;
import ru.tsystems.ecare.entities.Tariff;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO for tariffs
 */
public class TariffDTO {


    public TariffDTO() {}

    public TariffDTO(Tariff tariff) {
        this(tariff,false);
    }
    
    public TariffDTO(Tariff tariff, boolean initOptions) {
        id = tariff.getId();
        name = tariff.getName();
        price = tariff.getPrice();
        if(initOptions)
            options = getInitOptions(tariff.getOptions());
    }

    private long id;

    @Size(min = 1)
    private String name;

    private double price;

    private List<OptionDTO> options;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<OptionDTO> getOptions() {
        return options;
    }

    public void setOptions(List<OptionDTO> options) {
        this.options = options;
    }

    private List<OptionDTO> getInitOptions(List<Option> options) {
        List<OptionDTO> optionDTOList = new ArrayList<>();
        for(Option option : options)
            optionDTOList.add(new OptionDTO(option));
        return optionDTOList;
    }

}
