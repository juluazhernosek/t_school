package ru.tsystems.ecare.exceptions;

public class TariffDeleteException extends RuntimeException {

    private final long id;

    public TariffDeleteException(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
