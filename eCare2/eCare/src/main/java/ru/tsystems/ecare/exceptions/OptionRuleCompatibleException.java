package ru.tsystems.ecare.exceptions;

public class OptionRuleCompatibleException extends RuntimeException {
    private final long idOption1;
    private final long idOption2;

    public OptionRuleCompatibleException(long idOption1, long idOption2) {
        this.idOption1 = idOption1;
        this.idOption2 = idOption2;
    }

    public long getIdOption2() {
        return idOption2;
    }

    public long getIdOption1() {
        return idOption1;
    }
}
