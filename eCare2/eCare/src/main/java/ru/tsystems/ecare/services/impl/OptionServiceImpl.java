package ru.tsystems.ecare.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.ecare.dao.OptionDao;
import ru.tsystems.ecare.dao.TariffDao;
import ru.tsystems.ecare.entities.Option;
import ru.tsystems.ecare.entities.Tariff;
import ru.tsystems.ecare.exceptions.OptionNotFoundException;
import ru.tsystems.ecare.exceptions.OptionNotUniqueException;
import ru.tsystems.ecare.exceptions.OptionRuleCompatibleException;
import ru.tsystems.ecare.exceptions.OptionRuleIncompatibleException;
import ru.tsystems.ecare.services.OptionService;

@Service
public class OptionServiceImpl implements OptionService {

    @Autowired
    private OptionDao optionDao;

    @Autowired
    private TariffDao tariffDao;

    @Override
    public void createRule(long idOption1, long idOption2, boolean isCompatible) {
        Option option1 = optionDao.getOptionById(idOption1);
        if(option1 == null)
            throw new OptionNotFoundException(idOption1);
        Option option2 = optionDao.getOptionById(idOption2);
        if(option2 == null)
            throw new OptionNotFoundException(idOption2);
        if(isCompatible) {
            for (Option option : option1.getIncompatibleOptions())
                if (option.getId() == option2.getId())
                    throw new OptionRuleIncompatibleException(option.getId(),option2.getId());
            for (Option option : option2.getIncompatibleOptions())
                if (option.getId() == option1.getId())
                    throw new OptionRuleIncompatibleException(option.getId(),option1.getId());
        }
        else {
            for (Option option : option1.getCompatibleOptions())
                if (option.getId() == option2.getId())
                    throw new OptionRuleCompatibleException(option.getId(),option2.getId());
            for (Option option : option2.getCompatibleOptions())
                if (option.getId() == option1.getId())
                    throw new OptionRuleCompatibleException(option.getId(),option1.getId());
        }
        if(isCompatible) {
            option1.getCompatibleOptions().add(option2);
            option2.getCompatibleOptions().add(option1);
        } else {
            option1.getIncompatibleOptions().add(option2);
            option2.getIncompatibleOptions().add(option1);
        }
        optionDao.updateOption(option1);
        optionDao.updateOption(option2);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createOption(Option newOption, long idTariff) {
        Option option = optionDao.findByName(newOption.getName());
        if(option != null && option.getTariff() != null && option.getTariff().getId() == idTariff)
            throw new OptionNotUniqueException(newOption.getName());
        Tariff tariff = tariffDao.getById(idTariff);
        newOption.setTariff(tariff);
        optionDao.createOption(newOption);
        tariff.getOptions().add(newOption);
        tariffDao.updateTariff(tariff);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeOption(long id) {
        Option option = optionDao.getOptionById(id);
        if(option == null)
            throw  new OptionNotFoundException(id);
        for(Option o : option.getCompatibleOptions())
            o.getCompatibleOptions().remove(option);
        for(Option o : option.getIncompatibleOptions())
            o.getIncompatibleOptions().remove(option);
        optionDao.removeOption(option);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeRule(long idOption1, long idOption2, boolean isCompatible) {
        Option option1 = optionDao.getOptionById(idOption1);
        if(option1 == null)
            throw new OptionNotFoundException(idOption1);
        Option option2 = optionDao.getOptionById(idOption2);
        if(option2 == null)
            throw new OptionNotFoundException(idOption2);
        if(isCompatible) {
            option1.getCompatibleOptions().remove(option2);
            option2.getCompatibleOptions().remove(option1);
            optionDao.updateOption(option1);
            optionDao.updateOption(option2);
        }
        else {
            option1.getIncompatibleOptions().remove(option2);
            option2.getIncompatibleOptions().remove(option1);
            optionDao.updateOption(option1);
            optionDao.updateOption(option2);
        }
    }
}
