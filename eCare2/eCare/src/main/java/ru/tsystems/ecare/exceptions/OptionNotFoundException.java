package ru.tsystems.ecare.exceptions;

public class OptionNotFoundException extends RuntimeException {

    private final long id;

    public OptionNotFoundException(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
