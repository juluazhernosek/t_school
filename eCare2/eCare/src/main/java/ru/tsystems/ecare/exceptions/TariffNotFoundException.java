package ru.tsystems.ecare.exceptions;

public class TariffNotFoundException extends RuntimeException {

    private final long id;

    public TariffNotFoundException(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
