package ru.tsystems.ecare.dto.request;


public class RuleRequestDTO {
    private long idOption1;
    private long idOption2;
    private boolean compatible;

    public long getIdOption1() {
        return idOption1;
    }

    public void setIdOption1(long idOption1) {
        this.idOption1 = idOption1;
    }

    public long getIdOption2() {
        return idOption2;
    }

    public void setIdOption2(long idOption2) {
        this.idOption2 = idOption2;
    }

    public boolean isCompatible() {
        return compatible;
    }

    public void setCompatible(boolean compatible) {
        this.compatible = compatible;
    }
}
