package ru.tsystems.ecare.dao.impl;

import java.util.List;


public abstract class AbstractDaoImpl {
    public <T> T getSingleResult(List<T> list) {
        return list.isEmpty() ? null : list.get(0);
    }
}
