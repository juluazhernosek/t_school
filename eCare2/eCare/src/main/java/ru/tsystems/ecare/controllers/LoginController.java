package ru.tsystems.ecare.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This class resolver for redirecting login page
 */
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String login(){
        return "redirect:index";
    }
}
