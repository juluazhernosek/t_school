package ru.tsystems.ecare.exceptions;

public class ContractNotFoundException extends RuntimeException {

    private final long id;

    public ContractNotFoundException(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
