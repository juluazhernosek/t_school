package ru.tsystems.ecare.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.ecare.dao.ContractDao;
import ru.tsystems.ecare.dao.TariffDao;
import ru.tsystems.ecare.entities.Tariff;
import ru.tsystems.ecare.exceptions.TariffNotFoundException;
import ru.tsystems.ecare.exceptions.TariffNotUniqueException;
import ru.tsystems.ecare.exceptions.TariffStandardRemoveException;
import ru.tsystems.ecare.exceptions.TariffUseException;
import ru.tsystems.ecare.services.TariffService;

import java.util.List;

@Service
public class TariffServiceImpl implements TariffService {

    @Autowired
    private TariffDao tariffDao;

    @Autowired
    private ContractDao contractDao;


    @Override
    public List<Tariff> getAllTariff() {
        return tariffDao.getAllTariffs();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeTariff(long id) {
        Tariff tariff = tariffDao.getById(id);
        if(tariff == null)
            throw  new TariffNotFoundException(id);
        if(id == 1)
            throw new TariffStandardRemoveException();
        if(!contractDao.getContractsByTariff(tariff).isEmpty())
            throw new TariffUseException(tariff.getName());
        tariffDao.removeTariff(tariff);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createTariff(Tariff tariff) {
        if(tariffDao.findTariffByName(tariff.getName()) != null)
            throw new TariffNotUniqueException(tariff.getName());
        tariffDao.createTariff(tariff);
    }
}
