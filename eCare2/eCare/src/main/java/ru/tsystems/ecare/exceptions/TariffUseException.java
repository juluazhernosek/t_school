package ru.tsystems.ecare.exceptions;

public class TariffUseException extends RuntimeException {
    private final String name;

    public TariffUseException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
