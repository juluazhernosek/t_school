package ru.tsystems.ecare.dao;


import ru.tsystems.ecare.entities.Option;

import java.util.List;

/**
 * Interface for working with options from DB
 */
public interface OptionDao {
    void createOption(Option option);
    void removeOption(Option option);
    Option getOptionById(long id);
    Option findByName(String name);
    List<Option> getOptionsByIds(long[] ids);
    void updateOption(Option option);
}
