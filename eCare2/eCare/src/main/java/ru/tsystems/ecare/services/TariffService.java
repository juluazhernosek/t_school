package ru.tsystems.ecare.services;

import ru.tsystems.ecare.entities.Tariff;

import java.util.List;

/**
 * Class for business logic for working with tariff's
 */
public interface TariffService {
    List<Tariff> getAllTariff();
    boolean removeTariff(long id);
    void createTariff(Tariff tariff);
}
