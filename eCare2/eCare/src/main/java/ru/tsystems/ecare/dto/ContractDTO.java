package ru.tsystems.ecare.dto;

import ru.tsystems.ecare.entities.Contract;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * DTO for contracts
 */
public class ContractDTO {

    public ContractDTO() {};

    public ContractDTO(Contract contract) {
        id = contract.getId();
        phoneNumber = contract.getPhoneNumber();
        isActive = contract.isActive();
        whoBlocked = contract.isWhoBlocked();
    }

    private Long id;

    @Size(min = 1)
    private String phoneNumber;

    private boolean isActive;

    private boolean whoBlocked;

    private TariffDTO tariff;

    private List<OptionDTO> selectedOptions;

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isWhoBlocked() {
        return whoBlocked;
    }

    public void setWhoBlocked(boolean whoBlocked) {
        this.whoBlocked = whoBlocked;
    }

    public List<OptionDTO> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(List<OptionDTO> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public TariffDTO getTariff() {
        return tariff;
    }

    public void setTariff(TariffDTO tariff) {
        this.tariff = tariff;
    }
}
