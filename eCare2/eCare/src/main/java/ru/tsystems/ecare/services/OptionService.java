package ru.tsystems.ecare.services;

import ru.tsystems.ecare.entities.Option;

/**
 * Class for business logic for working with options and options rules
 */
public interface OptionService {
    void createRule(long idOption1, long idOption2, boolean isCompatible);
    void createOption(Option newOption, long idTariff);
    boolean removeOption(long id);
    void removeRule(long idOption1, long idOption2, boolean isCompatible);
}
