package ru.tsystems.ecare.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tsystems.ecare.dao.ContractDao;
import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.Tariff;

import java.util.List;


@Repository
public class ContractDaoImpl extends AbstractDaoImpl implements ContractDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Contract getById(long id) {
        return (Contract) sessionFactory.getCurrentSession().get(Contract.class,id);
    }

    @Override
    public List<Contract> getContractsByPartNumber(String number) {
        return sessionFactory.getCurrentSession().createQuery("from Contract WHERE phoneNumber LIKE :number").
                setParameter("number","%" + number + "%").list();
    }

    @Override
    public Contract getContractByExactlyNumber(String number) {
        return (Contract) getSingleResult(sessionFactory.getCurrentSession().createQuery("FROM Contract WHERE phoneNumber=:phoneNumber")
        .setParameter("phoneNumber",number).list());
    }

    @Override
    public void createContract(Contract contract) {
        sessionFactory.getCurrentSession().persist(contract);
    }

    @Override
    public void updateContract(Contract contract) {
        sessionFactory.getCurrentSession().update(contract);
    }


    @Override
    public List<Contract> getContractsByTariff(Tariff tariff) {
        return sessionFactory.getCurrentSession().createQuery("FROM Contract WHERE tariff=:tariff")
                .setParameter("tariff",tariff).list();
    }

    @Override
    public void removeContract(Contract contract) {
        sessionFactory.getCurrentSession().delete(contract);
        sessionFactory.getCurrentSession().flush();
    }

}
