package ru.tsystems.ecare.dao;



import ru.tsystems.ecare.entities.User;

import java.util.List;

/**
 * Interface for working with users from DB
 */
public interface UserDao {
    void createUser(User user);
    User findUserByLogin(String login);
    User getUserById(Long id);
    List<User> getAllUserByRole(boolean role);
    void removeUser(User user);
}
