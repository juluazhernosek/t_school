package ru.tsystems.ecare.exceptions;

public class ContractNotUniqueException extends RuntimeException {
    private final String phoneNumber;

    public ContractNotUniqueException(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
