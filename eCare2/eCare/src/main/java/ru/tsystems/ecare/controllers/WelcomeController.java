package ru.tsystems.ecare.controllers;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;

/**
 * Class to redirect to a page depending on the role
 */
@Controller
public class WelcomeController {

    @RequestMapping({"/index.html","index","/"})
    public String index(Model model) {
        return getUserPage();
    }

    private String getUserPage(){
        Collection <? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for(GrantedAuthority grantedAuthority : authorities) {
            switch (grantedAuthority.getAuthority()) {
                case "ROLE_ADMIN" :
                    return "redirect:/manager";
                case "ROLE_USER" :
                    return "redirect:/user";
                default:
                    return "loginView";
            }
        }
        return null;
    }

}
