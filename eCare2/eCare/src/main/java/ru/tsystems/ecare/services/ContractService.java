package ru.tsystems.ecare.services;

import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.Option;

import java.util.List;

/**
 * Class for business logic for working with contracts
 */
public interface ContractService {
    boolean createContract(Contract contract, long idUser);
    boolean saveContract(long id, long idTariff,long[] options, boolean active,boolean isWhoUnlocked);
    void removeContract(long id);

    boolean isIncompatibleOptions(Option option1, Option option2);

    Option isCompatibleOptions(Option option1, List<Option> allOptions);
}
