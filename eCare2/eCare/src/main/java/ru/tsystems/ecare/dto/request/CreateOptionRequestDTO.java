package ru.tsystems.ecare.dto.request;


import javax.validation.constraints.Size;

public class CreateOptionRequestDTO {
    private long idTariff;

    @Size(min = 1)
    private String name;

    private double price;

    private double cost;

    public long getIdTariff() {
        return idTariff;
    }

    public void setIdTariff(long idTariff) {
        this.idTariff = idTariff;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
