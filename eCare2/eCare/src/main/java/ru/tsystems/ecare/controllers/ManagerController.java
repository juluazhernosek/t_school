package ru.tsystems.ecare.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.tsystems.ecare.dto.ContractDTO;
import ru.tsystems.ecare.dto.OptionDTO;
import ru.tsystems.ecare.dto.TariffDTO;
import ru.tsystems.ecare.dto.UserDTO;
import ru.tsystems.ecare.dto.request.*;
import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.Option;
import ru.tsystems.ecare.entities.Tariff;
import ru.tsystems.ecare.entities.User;
import ru.tsystems.ecare.services.ContractService;
import ru.tsystems.ecare.services.OptionService;
import ru.tsystems.ecare.services.TariffService;
import ru.tsystems.ecare.services.UserService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for processing manager's responses
 */
@Controller
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private UserService userService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private OptionService optionService;

    /**
     * return private information for manager
     * @param map
     * @return path to view
     */
    @RequestMapping({"","/index"})
    public String index(ModelMap map) {
        User user = userService.getUserByLogin(getUserName());
        map.put("user", user);
        return "manager/index";
    }

    /**
     * return information about tariffs and options  for manager
     * @param map
     * @return path to view
     */
    @RequestMapping("/tariffsAndOptions")
    public String tariffsAndOptions(ModelMap map) {
        map.put("allTariffs",getAllTariffsDTO(tariffService.getAllTariff(),true));
        return "manager/tariffsAndOptions";
    }

    /**
     * return view with all users
     * @param map
     * @return path to view
     */
    @RequestMapping("/allContracts")
    public String allContracts(ModelMap map) {
        List<User> users = userService.getAllClients();
        List<UserDTO> usersDTOList = new ArrayList<>();
        for(User user : users)
            usersDTOList.add(new UserDTO(user));
        map.put("users",usersDTOList);
        return "manager/allContracts";
    }

    /**
     * Return users having contacts with number
     * @param map
     * @param number of contract, which we want find
     * @return path to view
     */
    @RequestMapping(value = "/allContracts/search",method = RequestMethod.GET)
    public String allContractsByPhone(ModelMap map,@RequestParam("number") String number) {
        List<User> users = userService.getUserByPhone(number);
        List<UserDTO> usersDTOList = new ArrayList<>();
        for(User user : users)
            usersDTOList.add(new UserDTO(user));
        map.put("users",usersDTOList);
        return "manager/allContracts";
    }

    /**
     *
     * @param map
     * @param userId unique user's number for getting contracts
     * @return path to view, where we can see user`s contracts
     */
    @RequestMapping("/allContracts/user/{id}")
    public String userContract(ModelMap map,@PathVariable("id") Long userId) {
        User user = userService.getUserById(userId);
        map.put("userId",user.getId());
        map.put("userContracts",getContractDTOList(user.getContracts()));
        map.put("allTariffs",getAllTariffsDTO(tariffService.getAllTariff(),false));
        return "manager/userContracts";
    }

    /**
     * Method for saving user's contract
     * @param request Object, with nessesary data
     * @return message of operation
     */
    @RequestMapping(value = "allContracts/user/contracts/save", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody
    String saveContract(@RequestBody SaveContractRequestDTO request) {
        contractService.saveContract(request.getIdContract(),request.getIdTariff(),
                request.getOptions(),request.isActive(),true);
        return "success";
    }

    /**
     * Method for creating new tariff
     * @param tariffDTO DTO for entity Tariff
     * @return message of operation
     */
    @RequestMapping(value = "allContracts/tariffs/create", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody
    String createTariff(@RequestBody @Valid TariffDTO tariffDTO) {
        Tariff tariff = new Tariff();
        tariff.setName(tariffDTO.getName());
        tariff.setPrice(tariffDTO.getPrice());
        tariffService.createTariff(tariff);
        return "success";
    }

    @RequestMapping(value = "/allContracts/user/{id}/contracts/create", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody
    String createContract(@RequestBody @Valid ContractDTO requestDTO,@PathVariable("id") long userId) {
        Contract contract =new Contract();
        contract.setPhoneNumber(requestDTO.getPhoneNumber());
        contract.setActive(requestDTO.isActive());
        contract.setWhoBlocked(requestDTO.isWhoBlocked());
        contractService.createContract(contract, userId);
        return "success";
    }

    /**
     * CMethod for creating user
     * @param user DTO for entity user
     * @return message of operation
     */
    @RequestMapping(value = "allContracts/users/create", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody
    String createUser(@RequestBody @Valid SaveUserRequestDTO user) {
        User newUser = new User();
        newUser.setName(user.getUser().getName()); newUser.setAddress(user.getUser().getAddress());
        newUser.setBirthdayDate(user.getUser().getBirthdayDate()); newUser.setEmail(user.getUser().getEmail());
        newUser.setSurname(user.getUser().getSurname());newUser.setPassport(user.getUser().getPassport());
        newUser.setRole(user.isRole()); newUser.setRole(user.isRole());newUser.setPassword(user.getPassword());
        userService.createUser(newUser);
        return "success";
    }

    /**
     * MEthod for creating option
     * @param optionRequestDTO DTO contains data for create option
     * @return id created option
     */
    @RequestMapping(value = "allContracts/options/create", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody
    String createOption(@RequestBody @Valid CreateOptionRequestDTO optionRequestDTO) {
        Option option = new Option();
        option.setName(optionRequestDTO.getName());
        option.setPrice(optionRequestDTO.getPrice());
        option.setCost(optionRequestDTO.getCost());
        optionService.createOption(option,optionRequestDTO.getIdTariff());
        JsonObject jsonElement = new JsonObject();
        jsonElement.addProperty("status","success");
        jsonElement.addProperty("idOption",option.getId());
        Gson gson = new Gson();
        return gson.toJson(jsonElement);
    }

    /**
     * Method for creating user
     * @param request DTO with data for create rule
     * @return message of operation
     */
    @RequestMapping(value = "tariffsAndOptions/rule/create", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody
    String createRule(@RequestBody RuleRequestDTO request) {
        optionService.createRule(request.getIdOption1(),request.getIdOption2(),request.isCompatible());
        return "success";
    }

    /**
     * Method for remove option's rule
     * @param request
     * @return
     */
    @RequestMapping(value = "tariffsAndOptions/rule/remove", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody
    String removeRule(@RequestBody RuleRequestDTO request) {
        optionService.removeRule(request.getIdOption1(),request.getIdOption2(),request.isCompatible());
        return "success";
    }

    /**
     * Remove option
     * @param id unique number by option, by which will be remove
     * @return message operation
     */
    @RequestMapping(value = "tariffsAndOptions/option/remove/{id}")
    public @ResponseBody
    String removeOption(@PathVariable("id") long id) {
        optionService.removeOption(id);
        return "success";
    }

    /**
     * Remove tariff
     * @param id  unique tariff`s name
     * @return message of operation
     */
    @RequestMapping(value = "tariffsAndOptions/tariff/remove/{id}")
    public @ResponseBody
    String removeTariff(@PathVariable("id") long id) {
        tariffService.removeTariff(id);
        return "success";
    }

    /**
     * Remove user
     * @param id unique user`s number
     * @return message of operation
     */
    @RequestMapping(value = "allContracts/user/remove/{id}")
    public @ResponseBody
    String removeUser(@PathVariable("id") long id) {
        userService.removeUser(id);
        return "success";
    }

    /**
     * Removing contract
     * @param id
     * @return
     */
    @RequestMapping(value = "allContracts/user/contracts/remove/{id}",method = RequestMethod.POST)
    public @ResponseBody
    String removeContract(@PathVariable("id") long id) {
        contractService.removeContract(id);
        return "success";
    }


    private List<TariffDTO> getAllTariffsDTO(List<Tariff> allTariffs, boolean init) {
        List<TariffDTO> tariffDTOList = new ArrayList<>();
        for(Tariff tariff : allTariffs) {
            TariffDTO tariffDTO = new TariffDTO(tariff);
            List<OptionDTO> optionDTOList = new ArrayList<>();
            for(Option option : tariff.getOptions())
                optionDTOList.add(new OptionDTO(option,init));
            tariffDTO.setOptions(optionDTOList);
            tariffDTOList.add(tariffDTO);
        }
        return tariffDTOList;
    }

    private List<ContractDTO> getContractDTOList(List<Contract> contracts) {
        List<ContractDTO> contractDTOList = new ArrayList<>();
        for(Contract contract : contracts) {
            ContractDTO contractDTO = new ContractDTO(contract);
            List<OptionDTO> optionDTOList = new ArrayList<>();
            for(Option option : contract.getSelectedOptions())
                optionDTOList.add(new OptionDTO(option,true));
            contractDTO.setSelectedOptions(optionDTOList);
            TariffDTO tariff = new TariffDTO(contract.getTariff(),true);
            contractDTO.setTariff(tariff);
            contractDTOList.add(contractDTO);
        }
        return contractDTOList;
    }

    private String getUserName() {
        String userName = "";
        try{
            userName = SecurityContextHolder.getContext().getAuthentication().getName();
        } catch (NullPointerException e) {
            return userName;
        }
        return userName;
    }

}
