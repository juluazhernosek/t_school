package ru.tsystems.ecare.dto.request;

import ru.tsystems.ecare.dto.UserDTO;

import javax.validation.constraints.Size;


public class SaveUserRequestDTO {

    private UserDTO user;

    @Size(min = 1, max = 25)
    private String password;

    private boolean role;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }
}
