package ru.tsystems.ecare.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.ecare.dao.ContractDao;
import ru.tsystems.ecare.dao.OptionDao;
import ru.tsystems.ecare.dao.TariffDao;
import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.Option;
import ru.tsystems.ecare.entities.Tariff;
import ru.tsystems.ecare.entities.User;
import ru.tsystems.ecare.exceptions.*;
import ru.tsystems.ecare.services.ContractService;
import ru.tsystems.ecare.services.UserService;

import java.util.List;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private TariffDao tariffDao;

    @Autowired
    private OptionDao optionDao;

    @Autowired
    private UserService userService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean createContract(Contract contract,long idUser) {
        Tariff tariff = tariffDao.getById(1);          // standard tariff
        if(tariff == null)
            throw new TariffStandardNotFoundException();
        if(contractDao.getContractByExactlyNumber(contract.getPhoneNumber()) != null)
            throw new ContractNotUniqueException(contract.getPhoneNumber());
        User user = userService.getUserById(idUser);
        if(user == null)
            throw new UserNotFoundException(idUser);
        contract.setTariff(tariff);
        contract.setUser(user);
        contractDao.createContract(contract);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveContract(long id, long idTariff, long[] options,boolean active,boolean isWhoUnlocked) {
        Contract contract = contractDao.getById(id);
        if(contract == null)
            throw new ContractNotFoundException(id);
        Tariff tariff = tariffDao.getById(idTariff);
        if(tariff == null)
            throw new TariffNotFoundException(idTariff);
        contract.setTariff(tariff);
        List<Option> listOption = optionDao.getOptionsByIds(options);
        if(options.length != listOption.size())
            throw new OptionNotFoundException(0);
        for(Option o1 : listOption)
            for (Option o2 : listOption)
                if(isIncompatibleOptions(o1, o2))
                    throw new OptionIncompatibleException(o1,o2);
        for(Option o1 : listOption) {
            Option o2;
            if((o2 = isCompatibleOptions(o1,listOption)) != null)
                throw new OptionCompatibleException(o1,o2);
        }
        contract.setSelectedOptions(listOption);
        if(active && (contract.isWhoBlocked() && !isWhoUnlocked) && !contract.isActive())
            throw new LockContractException(id);
        contract.setActive(active);
        contract.setWhoBlocked(isWhoUnlocked);
        contractDao.updateContract(contract);
        return true;
    }

    @Override
    public void removeContract(long id) {
        Contract contract = contractDao.getById(id);
        if(contract == null)
            throw new ContractNotFoundException(id);
        contractDao.removeContract(contract);
    }


    public Option isCompatibleOptions(Option option, List<Option> allOptions) {
        for (Option o2 : option.getCompatibleOptions())
            if(!allOptions.contains(o2))
                return o2;
        return null;
    }

    public boolean isIncompatibleOptions(Option option1, Option option2) {
        for(Option o : option1.getIncompatibleOptions()) {
            if(o.getId() == option2.getId())
                return true;        }
        return false;
    }
}
