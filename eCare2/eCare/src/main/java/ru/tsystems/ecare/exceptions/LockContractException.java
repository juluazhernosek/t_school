package ru.tsystems.ecare.exceptions;

public class LockContractException extends RuntimeException {

    private final long id;

    public LockContractException(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
