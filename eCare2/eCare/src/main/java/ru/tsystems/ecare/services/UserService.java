package ru.tsystems.ecare.services;


import ru.tsystems.ecare.entities.User;

import java.util.List;

/**
 * Class for business logic for working with users
 */
public interface UserService {
    User getUserById(long id);
    User createUser(User user);
    List<User> getAllClients();
    List<User> getUserByPhone(String number);
    User getUserByLogin(String login);
    void removeUser(long id);
}
