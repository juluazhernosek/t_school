package ru.tsystems.ecare.dto;

import ru.tsystems.ecare.entities.Option;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO for options
 */
public class OptionDTO {


    public OptionDTO() {}
    public OptionDTO(Option option) {
        this(option,false);
    }

    public OptionDTO(Option option,boolean initChildOptions) {
        id = option.getId();
        name = option.getName();
        cost = option.getCost();
        price = option.getPrice();
        if(initChildOptions) {
            incompatibleOptions = new ArrayList<>();
            for(Option o: option.getIncompatibleOptions())
                incompatibleOptions.add(new OptionDTO(o));
            compatibleOptions = new ArrayList<>();
            for(Option o: option.getCompatibleOptions())
                compatibleOptions.add(new OptionDTO(o));

        }
    }

    private long id;

    @Size(min = 1)
    private String name;

    private double cost;

    private double price;

    private List<OptionDTO> compatibleOptions;

    private List<OptionDTO> incompatibleOptions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<OptionDTO> getCompatibleOptions() {
        return compatibleOptions;
    }

    public void setCompatibleOptions(List<OptionDTO> compatibleOptions) {
        this.compatibleOptions = compatibleOptions;
    }

    public List<OptionDTO> getIncompatibleOptions() {
        return incompatibleOptions;
    }

    public void setIncompatibleOptions(List<OptionDTO> incompatibleOptions) {
        this.incompatibleOptions = incompatibleOptions;
    }

    public boolean equals (Object o) {
        return o!= null && o instanceof OptionDTO && ((OptionDTO)o).getId() == id;
    }

    public int hashCode() {
        return (int) id;
    }

}
