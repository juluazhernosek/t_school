package ru.tsystems.ecare.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tsystems.ecare.dao.UserDao;
import ru.tsystems.ecare.entities.User;

import java.util.List;

@SuppressWarnings("unchecked")
@Repository
public class UserDaoImpl extends AbstractDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createUser(User user) {
        sessionFactory.getCurrentSession().persist(user);
    }

    @Override
    public User findUserByLogin(String login) {
        List<User> userList = sessionFactory.getCurrentSession().createQuery("from User WHERE email =:login")
                .setParameter("login",login)
                .list();
        return getSingleResult(userList);
    }

    @Override
    public User getUserById(Long id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }


    @Override
    public List<User> getAllUserByRole(boolean role) {
        return sessionFactory.getCurrentSession().createQuery("from User WHERE role =:role")
                .setParameter("role",role).list();
    }

    @Override
    public void removeUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
        sessionFactory.getCurrentSession().flush();
    }
}
