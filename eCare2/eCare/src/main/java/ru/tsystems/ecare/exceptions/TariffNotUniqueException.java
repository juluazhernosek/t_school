package ru.tsystems.ecare.exceptions;

public class TariffNotUniqueException extends RuntimeException {
    private final String name;

    public TariffNotUniqueException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
