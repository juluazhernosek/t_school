package ru.tsystems.ecare.dto.request;

import java.io.Serializable;


public class SaveContractRequestDTO implements Serializable {
    private long[] options;
    private long idContract;
    private long idTariff;
    private boolean active;
    private boolean isWhoBlocked;

    public long[] getOptions() {
        return options;
    }

    public void setOptions(long[] options) {
        this.options = options;
    }

    public long getIdContract() {
        return idContract;
    }

    public void setIdContract(long idContract) {
        this.idContract = idContract;
    }

    public long getIdTariff() {
        return idTariff;
    }

    public void setIdTariff(long idTariff) {
        this.idTariff = idTariff;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isWhoBlocked() {
        return isWhoBlocked;
    }

    public void setWhoBlocked(boolean isWhoBlocked) {
        this.isWhoBlocked = isWhoBlocked;
    }
}
