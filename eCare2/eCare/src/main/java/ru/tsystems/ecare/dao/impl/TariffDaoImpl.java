package ru.tsystems.ecare.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tsystems.ecare.dao.TariffDao;
import ru.tsystems.ecare.entities.Tariff;

import java.util.List;


@Repository
public class TariffDaoImpl extends AbstractDaoImpl implements TariffDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Tariff getById(long id) {
        return (Tariff) sessionFactory.getCurrentSession().get(Tariff.class,id);
    }

    @Override
    public List<Tariff> getAllTariffs() {
        return sessionFactory.getCurrentSession().createQuery("from Tariff").list();
    }

    @Override
    public void removeTariff(Tariff tariff) {
        sessionFactory.getCurrentSession().delete(tariff);
    }

    @Override
    public void createTariff(Tariff tariff) {
        sessionFactory.getCurrentSession().persist(tariff);
    }

    @Override
    public Tariff findTariffByName(String name) {
        return (Tariff) getSingleResult(sessionFactory.getCurrentSession().createQuery("FROM Tariff WHERE name=:name")
                            .setParameter("name",name).list());
    }

    @Override
    public void updateTariff(Tariff tariff) {
        sessionFactory.getCurrentSession().update(tariff);
        sessionFactory.getCurrentSession().flush();
    }
}
