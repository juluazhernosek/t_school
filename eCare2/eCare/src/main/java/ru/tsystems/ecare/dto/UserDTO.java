package ru.tsystems.ecare.dto;

import org.hibernate.validator.constraints.Email;
import ru.tsystems.ecare.entities.User;

import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * DTO for users
 */
public class UserDTO implements Serializable {


    public UserDTO(){}

    public UserDTO(User user) {
        id = user.getId();
        name = user.getName();
        surname = user.getSurname();
        birthdayDate = user.getBirthdayDate();
        passport = user.getPassport();
        address = user.getAddress();
        email = user.getEmail();
    }

    private Long id;

    @Pattern(regexp = "^[a-zA-Z\\s]{1,20}$")
    private String name;

    @Pattern(regexp = "^[a-zA-Z\\s]{1,20}$")
    private String surname;

    @Past
    private Date birthdayDate;

    @Size(min = 1, max = 50)
    private String passport;

    @Size(min = 1, max = 50)
    private String address;

    @Email
    private String email;

    private List<ContractDTO> contracts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Date birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ContractDTO> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractDTO> contracts) {
        this.contracts = contracts;
    }
}
