package ru.tsystems.ecare.exceptions;

public class OptionNotUniqueException extends RuntimeException{
    private final String name;

    public OptionNotUniqueException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
