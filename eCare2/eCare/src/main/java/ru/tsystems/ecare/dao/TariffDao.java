package ru.tsystems.ecare.dao;


import ru.tsystems.ecare.entities.Tariff;

import java.util.List;

/**
 * Interface for working with tariffs from DB
 */
public interface TariffDao {
    Tariff getById(long id);
    List<Tariff> getAllTariffs();
    void removeTariff(Tariff tariff);
    void createTariff(Tariff tariff);
    Tariff findTariffByName(String name);
    void updateTariff(Tariff tariff);
}
