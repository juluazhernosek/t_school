package ru.tsystems.ecare.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.tsystems.ecare.dto.ContractDTO;
import ru.tsystems.ecare.dto.OptionDTO;
import ru.tsystems.ecare.dto.TariffDTO;
import ru.tsystems.ecare.dto.UserDTO;
import ru.tsystems.ecare.dto.request.SaveContractRequestDTO;
import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.Option;
import ru.tsystems.ecare.entities.Tariff;
import ru.tsystems.ecare.entities.User;
import ru.tsystems.ecare.services.ContractService;
import ru.tsystems.ecare.services.TariffService;
import ru.tsystems.ecare.services.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for processing user's responses
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private ContractService contractService;

    @RequestMapping({"","index"})
    public String index(ModelMap map) {
        User user = userService.getUserByLogin(getUserName());
        UserDTO userDTO = new UserDTO(user);
        map.put("user", userDTO);
        return "user/index";
    }

    @RequestMapping(value = "/contracts")
    public String getUserContracts(ModelMap map) {
        User user = userService.getUserByLogin(getUserName());
        map.put("userContracts",getContractDTOList(user.getContracts()));
        map.put("allTariffs",getAllTariffsDTO(tariffService.getAllTariff(),true));
        return "user/contracts";
    }

    @RequestMapping("/askSpecialist")
    public String askSpecialist(ModelMap map) {
        return "user/askSpecialist";
    }

    @RequestMapping("/about")
    public String about(ModelMap map) {
        return "user/about";
    }

    @RequestMapping(value = "/contracts/save", method = RequestMethod.POST,
            consumes="application/json")
    public @ResponseBody String saveContract(@RequestBody SaveContractRequestDTO request) {
        contractService.saveContract(request.getIdContract(),request.getIdTariff(),
                request.getOptions(),request.isActive(),false);
        return "success";
    }

    @RequestMapping("/bucket")
    public String bucket(ModelMap map) {
        User user = userService.getUserByLogin(getUserName());
        map.put("userContracts",getContractDTOList(user.getContracts()));
        map.put("allTariffs",getAllTariffsDTO(tariffService.getAllTariff(),false));
        return "user/bucket";
    }

    private List<TariffDTO> getAllTariffsDTO(List<Tariff> allTariffs, boolean init) {
        List<TariffDTO> tariffDTOList = new ArrayList<>();
        for(Tariff tariff : allTariffs) {
            TariffDTO tariffDTO = new TariffDTO(tariff);
            List<OptionDTO> optionDTOList = new ArrayList<>();
            for(Option option : tariff.getOptions())
                optionDTOList.add(new OptionDTO(option,init));
            tariffDTO.setOptions(optionDTOList);
            tariffDTOList.add(tariffDTO);
        }
        return tariffDTOList;
    }

    private List<ContractDTO> getContractDTOList(List<Contract> contracts) {
        List<ContractDTO> contractDTOList = new ArrayList<>();
        for(Contract contract : contracts) {
            ContractDTO contractDTO = new ContractDTO(contract);
            List<OptionDTO> optionDTOList = new ArrayList<>();
            for(Option option : contract.getSelectedOptions())
                optionDTOList.add(new OptionDTO(option,true));
            contractDTO.setSelectedOptions(optionDTOList);
            TariffDTO tariff = new TariffDTO(contract.getTariff(),true);
            contractDTO.setTariff(tariff);
            contractDTOList.add(contractDTO);
        }
        return contractDTOList;
    }

    private String getUserName() {
        String userName = "";
        try{
            userName = SecurityContextHolder.getContext().getAuthentication().getName();
        } catch (NullPointerException e) {
            return userName;
        }
        return userName;
    }

}
