package ru.tsystems.ecare.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.ecare.dao.ContractDao;
import ru.tsystems.ecare.dao.UserDao;
import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.User;
import ru.tsystems.ecare.exceptions.UserNotFoundException;
import ru.tsystems.ecare.exceptions.UserNotUniqueException;
import ru.tsystems.ecare.services.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl  implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ContractDao contractDao;

    @Override
    public User getUserById(long id) {
        return userDao.getUserById(id);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public User createUser(User user) {
        if (userDao.findUserByLogin(user.getEmail()) != null)
            throw new UserNotUniqueException(user.getEmail());
        userDao.createUser(user);
        return user;
    }

    @Override
    public List<User> getAllClients() {
        return userDao.getAllUserByRole(false);
    }

    @Override
    public List<User> getUserByPhone(String number) {
        List<Contract> contracts = contractDao.getContractsByPartNumber(number);
        Set<User> users = new HashSet<>();
        List<User> userList = new ArrayList<>();
        for(Contract contract : contracts) {
           users.add(contract.getUser());
        }
        for(User user : users)
            userList.add(user);
        return userList;
    }

    @Override
    @Transactional
    public User getUserByLogin(String login) {
        return userDao.findUserByLogin(login);
    }

    @Override
    public void removeUser(long id) {
        User user = userDao.getUserById(id);
        if(user == null)
            throw new UserNotFoundException(id);
        userDao.removeUser(user);
    }
}
