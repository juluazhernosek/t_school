package ru.tsystems.ecare.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tsystems.ecare.dao.OptionDao;
import ru.tsystems.ecare.entities.Option;

import java.util.List;


@Repository
public class OptionDaoImpl extends AbstractDaoImpl implements OptionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createOption(Option option) {
        sessionFactory.getCurrentSession().persist(option);
    }

    @Override
    public void removeOption(Option option) {
        sessionFactory.getCurrentSession().delete(option);
    }

    @Override
    public Option getOptionById(long id) {
         return (Option) sessionFactory.getCurrentSession().get(Option.class, id);
    }

    @Override
    public Option findByName(String name) {
        return (Option) getSingleResult(sessionFactory.getCurrentSession().createQuery("FROM Option WHERE name=:name")
                .setParameter("name",name).list());
    }

    @Override
    public List<Option> getOptionsByIds(long[] ids) {
        StringBuilder idsStr = new StringBuilder();
        for(long id : ids)
            idsStr.append(id).append(',');
        int index = idsStr.lastIndexOf(",");
        if(index != -1)
            idsStr.replace(index , index + 1,"");
        return sessionFactory.getCurrentSession().createSQLQuery("SELECT {opt.*} FROM options as opt WHERE opt.id" +
                " IN (" + (index==-1 ? null : idsStr.toString()) + ")").addEntity("opt", Option.class).list();
    }

    @Override
    public void updateOption(Option option) {
        sessionFactory.getCurrentSession().update(option);
        sessionFactory.getCurrentSession().flush();
    }


}
