package ru.tsystems.ecare.exceptions;

public class UserNotUniqueException extends RuntimeException {
    private final String email;

    public UserNotUniqueException(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
