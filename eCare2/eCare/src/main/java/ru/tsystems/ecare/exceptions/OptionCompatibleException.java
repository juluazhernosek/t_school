package ru.tsystems.ecare.exceptions;

import ru.tsystems.ecare.entities.Option;


public class OptionCompatibleException extends RuntimeException {

    private final Option option1;
    private final Option option2;

    public OptionCompatibleException(Option option1, Option option2) {
        this.option1 = option1;
        this.option2 = option2;
    }

    public Option getOption1() {
        return option1;
    }

    public Option getOption2() {
        return option2;
    }
}
