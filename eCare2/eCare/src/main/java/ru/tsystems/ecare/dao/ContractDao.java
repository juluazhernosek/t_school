package ru.tsystems.ecare.dao;


import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.Tariff;

import java.util.List;

/**
 * Interface for working with contracts from DB
 */
public interface ContractDao {
    Contract getById(long id);
    List<Contract> getContractsByPartNumber(String number);
    Contract getContractByExactlyNumber(String number);
    void createContract(Contract contract);
    void removeContract(Contract contract);
    void updateContract(Contract contract);
    List<Contract> getContractsByTariff(Tariff tariff);
}
