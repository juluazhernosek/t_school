package ru.tsystems.ecare.controllers;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.tsystems.ecare.exceptions.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Handler for service`s and usually exceptions
 */
@ControllerAdvice
public class ExceptionsHandlerController {
    private static final Logger logger = Logger.getLogger(ExceptionsHandlerController.class);

    @ExceptionHandler(ContractNotFoundException.class)
    public @ResponseBody
    String handleContractNotFound(HttpServletRequest request, ContractNotFoundException ex){
        logger.info("Not Found. URL="+request.getRequestURL());
        String message = "Cann't find contract with id" + ex.getId();
        logger.error(message);
        return message;
    }

    @ExceptionHandler(OptionNotFoundException.class)
    public @ResponseBody
    String handleOptionNotFound(HttpServletRequest request, OptionNotFoundException ex){
        logger.info("Not Found. URL=" + request.getRequestURL());
        String message = "Cann't find option with id" + ex.getId();
        logger.error(message);
        return message;
    }

    @ExceptionHandler(UserNotFoundException.class)
    public @ResponseBody
    String handleUserNotFound(HttpServletRequest request, UserNotFoundException ex){
        logger.info("Not Found. URL=" + request.getRequestURL());
        String message = "Cann't find user with id" + ex.getId();
        logger.error(message);
        return message;
    }

    @ExceptionHandler(TariffNotFoundException.class)
    public @ResponseBody
    String handleTariffNotFound(HttpServletRequest request, TariffNotFoundException ex){
        logger.error("Not Found. URL=" + request.getRequestURL());
        String message = "Cann't find tariff with id" + ex.getId();
        logger.error(message);
        return "Cann't find tariff with id" + ex.getId();
    }

    @ExceptionHandler(OptionCompatibleException.class)
    public @ResponseBody
    String handleCompatibleOptions(HttpServletRequest request, OptionCompatibleException ex){
        logger.info("Bad request. URL="+request.getRequestURL());
        String message = "Cann't save contract. Option : " + ex.getOption1().getName() + " and Option : " + ex.getOption2().getName()
                + " must be together";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(OptionIncompatibleException.class)
    public @ResponseBody
    String handleIncompatibleOptions(HttpServletRequest request, OptionIncompatibleException ex){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't save contract. Option : " + ex.getOption1().getName() + " and Option : " + ex.getOption2().getName()
                + " must be together";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(LockContractException.class)
    public @ResponseBody
    String handleLockContract(HttpServletRequest request){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't save contract. You can't unlocked contract. Contract locked by manager";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(TariffDeleteException.class)
    public @ResponseBody
    String tariffDeleteExceptionHandler(HttpServletRequest request,TariffDeleteException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't delete tariff with id : " + e.getId();
        logger.error(message);
        return message;
    }

    @ExceptionHandler(TariffStandardRemoveException.class)
    public @ResponseBody
    String tariffStandardDeleteExceptionHandler(HttpServletRequest request,TariffStandardRemoveException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't delete standard tariff";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(TariffStandardNotFoundException.class)
    public @ResponseBody
    String tariffStandardExceptionHandler(HttpServletRequest request,TariffStandardNotFoundException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't create contract.Tariff \"Standard\" not found. Please create standard tariff";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(OptionNotUniqueException.class)
    public @ResponseBody
    String optionNotUniqueExceptionHandler(HttpServletRequest request,OptionNotUniqueException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't create option with name : " + e.getName() + ". Option not unique";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(TariffNotUniqueException.class)
    public @ResponseBody
    String tariffNotUniqueExceptionHandler(HttpServletRequest request,TariffNotUniqueException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't create tariff with name : " + e.getName() + ". Tariff not unique";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(ContractNotUniqueException.class)
    public @ResponseBody
    String contractNotUniqueExceptionHandler(HttpServletRequest request,ContractNotUniqueException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't create contract with number : " + e.getPhoneNumber() + ". Contract with such number already exists";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(UserNotUniqueException.class)
    public @ResponseBody
    String userNotUniqueExceptionHandler(HttpServletRequest request,UserNotUniqueException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't create user with email : " + e.getEmail() + ". User with such email already exists";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(OptionRuleCompatibleException.class)
    public @ResponseBody
    String optionRuleCompatibleExceptionHandler(HttpServletRequest request,OptionRuleCompatibleException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't create rule. Option with id :" + e.getIdOption1() +
                " already compatible with option: " + e.getIdOption2();
        logger.error(message);
        return message;
    }

    @ExceptionHandler(OptionRuleIncompatibleException.class)
    public @ResponseBody
    String optionRuleIncompatibleExceptionHandler(HttpServletRequest request,OptionRuleIncompatibleException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Cann't create rule. Option with id :" + e.getIdOption1() +
                " already incompatible with option: " + e.getIdOption2();
        logger.error(message);
        return message;
    }


    @ExceptionHandler(TariffUseException.class)
    public @ResponseBody
    String handleTariffUseException(HttpServletRequest request,TariffUseException e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "To tariff \"" + e.getName() + "\" someone connected";
        logger.error(message);
        return message;
    }

    @ExceptionHandler(Exception.class)
    public @ResponseBody
    String handleOtherExceptions(HttpServletRequest request,Exception e){
        logger.info("Bad request. URL=" + request.getRequestURL());
        String message = "Server error.";
        logger.error(e.getMessage());
        return message;
    }
}
