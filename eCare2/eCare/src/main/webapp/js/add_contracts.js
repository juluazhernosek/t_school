var contracts = [];
var allTariffs = [];
var SELECTED_CONTRACT = null;
var SELECTED_TARIFF = null;
var SELECTED_OPTIONS = [];
var IS_MANAGER = null;
var CURRENT_SELECTED_OPTION = null;

function SaveRequest(id,idTariff,options,active,isWhoBlocked) {
    this.idContract = parseInt(id);
    this.idTariff = parseInt(idTariff);
    this.options = options;
    this.active = active;
    this.whoBlocked = isWhoBlocked;
}

function saveContract() {
    var request = new SaveRequest(SELECTED_CONTRACT.id,SELECTED_TARIFF.id,[],SELECTED_CONTRACT.active,
        (IS_MANAGER == null ? false : IS_MANAGER));
    SELECTED_OPTIONS.forEach(function (value) {
        request.options.push(parseInt(value.id));
    });
    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        data : JSON.stringify(request),
        url : 'contracts/save',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                $("#info_message_text").html("Contract successfully saved");
                $("#info_block").css('display','');
                $("#close_info_message").click(function() {location.reload()});
            }
            clearSession();
        }
    })
}

function unique(list) {
    var result = [];
    list.forEach(function (e) {
        if ($.inArray(e, result) == -1)
            result.push(e);
    });
    return result;
}

function clearSession() {
    if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".tariff") != undefined)
        $.session.remove("old." + SELECTED_CONTRACT.phoneNumber + ".tariff");
    if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined)
        $.session.remove("old." + SELECTED_CONTRACT.phoneNumber + ".options");
    if ($.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".tariff") != undefined)
        $.session.remove("new." + SELECTED_CONTRACT.phoneNumber + ".tariff");
    if ($.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined)
        $.session.remove("new." + SELECTED_CONTRACT.phoneNumber + ".options");
    if ($.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined)
        $.session.remove("delete." + SELECTED_CONTRACT.phoneNumber + ".options");
}

function changeTariff() {
    if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".tariff") == undefined
        || $.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".options") == undefined) {
        $.session.set("old." + SELECTED_CONTRACT.phoneNumber + ".tariff", SELECTED_TARIFF.name);
        var oldOptions = [];
        SELECTED_OPTIONS.forEach(function (value) {
            oldOptions.push(value.id);
        });
        $.session.set("old." + SELECTED_CONTRACT.phoneNumber + ".options", (oldOptions.length == 0 ? "" : oldOptions.toString()));
    }
    var deletedOptions = [];
    if($.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined)
        deletedOptions = $.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options").split(",");
    SELECTED_OPTIONS.forEach(function (value) {
        deletedOptions.push(value.id);
    });
    deletedOptions = unique(deletedOptions);
    $.session.set("new." + SELECTED_CONTRACT.phoneNumber + ".tariff", allTariffs[$("#tariff_name").val()].name);
    $.session.set("new." + SELECTED_CONTRACT.phoneNumber + ".options", "");
    $.session.set("delete." + SELECTED_CONTRACT.phoneNumber + ".options", (deletedOptions.length == 0 ? "" : deletedOptions.toString()));
    initContract();

}

function addOption() {
    if (CURRENT_SELECTED_OPTION == null)
        return;
    if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".options") == undefined) {
        var oldOptions = [];
        SELECTED_OPTIONS.forEach(function (value) {
            oldOptions.push(value.id);
        });
        $.session.set("old." + SELECTED_CONTRACT.phoneNumber + ".options", (oldOptions.length == 0 ? "" : oldOptions.toString()));
    }
    var newOptions = $.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options");
    var deletedOption = $.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options");
    if (newOptions == undefined || newOptions == "")
        newOptions = [];
    else
        newOptions = newOptions.split(",");
    if (deletedOption != undefined) {
        deletedOption = deletedOption.split(",");
        var length = deletedOption.length;
        deletedOption = $.grep(deletedOption, function (obj) {
            return obj != CURRENT_SELECTED_OPTION.id;
        });
        if (deletedOption.length == length)
            newOptions.push(CURRENT_SELECTED_OPTION.id);
    }
    else {
        deletedOption = [];
        newOptions.push(CURRENT_SELECTED_OPTION.id);
    }
    $.session.set("new." + SELECTED_CONTRACT.phoneNumber + ".options", (newOptions.length == 0 ? "" : newOptions.toString()));
    $.session.set("delete." + SELECTED_CONTRACT.phoneNumber + ".options", (deletedOption.length == 0 ? "" : deletedOption.toString()));
    initContract();
}

function deleteOption() {
    if (CURRENT_SELECTED_OPTION == null)
        return;
    var newOptions = $.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options");
    var deletedOption = $.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options");
    if (deletedOption == undefined || deletedOption == "")
        deletedOption = [];
    else
        deletedOption = deletedOption.split(",");
    if (newOptions != undefined) {
        newOptions = newOptions.split(",");
        var length = newOptions.length;
        newOptions = $.grep(newOptions, function (obj) {
            return obj != CURRENT_SELECTED_OPTION.id;
        });
        if (newOptions.length == length)
            deletedOption.push(CURRENT_SELECTED_OPTION.id);
    }
    else {
        newOptions = [];
        deletedOption.push(CURRENT_SELECTED_OPTION.id);
    }
    SELECTED_OPTIONS = SELECTED_CONTRACT.options;
    $.session.set("new." + SELECTED_CONTRACT.phoneNumber + ".options", (newOptions.length == 0 ? "" : newOptions.toString()));
    $.session.set("delete." + SELECTED_CONTRACT.phoneNumber + ".options", (deletedOption.length == 0 ? "" : deletedOption.toString()));
    initContract();
}

function initContract() {
    $("#selected_options").html("");
    var newOptions = [];
    var deletedOptions = [];
    var tariff = "";
    var oldTariff = null;
    if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".tariff") != undefined) {
        oldTariff = allTariffs[$.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".tariff")];
    }
    if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined &&
        $.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".options") != "") {
        SELECTED_CONTRACT.options = [];
        var oldOptions = $.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".options").split(",");

        oldOptions.forEach(function (value) {
            SELECTED_CONTRACT.options.push(oldTariff == null ? SELECTED_TARIFF.options[value] : oldTariff.options[value]);
        });
        SELECTED_OPTIONS = SELECTED_CONTRACT.options;
    }
    if ($.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".tariff") != undefined) {
        tariff = allTariffs[$.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".tariff")];
    }
    else if (oldTariff != null) {
        tariff = oldTariff;
    }
    else {
        tariff = SELECTED_CONTRACT.tariff;
    }
    newOptions = $.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options");
    deletedOptions = $.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options");
    newOptions = (newOptions == undefined || newOptions == "" ? [] : newOptions.split(",") );
    deletedOptions = (deletedOptions == undefined || deletedOptions == "" ? [] : deletedOptions.split(","));
    newOptions.forEach(function (value) {
        SELECTED_OPTIONS.push(tariff.options[value]);
    });
    deletedOptions.forEach(function (value) {
        var index = null;
        SELECTED_OPTIONS.some(function(val,id) {
           if(value == val.id) {
               index = id;
               return true;
           }
        });
        if(index != null)
            SELECTED_OPTIONS.splice(index, 1);
    });
    SELECTED_TARIFF = tariff;
    SELECTED_OPTIONS = unique(SELECTED_OPTIONS);
    SELECTED_OPTIONS.forEach(function (value) {
        $("#selected_options").append("<tr option_id=\"" + value.id + "\">" +
            "<td>" + value.name + "</td>" +
            "<td>" + value.cost + "</td>" +
            "<td>" + value.price + "</td>" +
            "</tr>");
    });
    switch_blocked(SELECTED_CONTRACT.active,true);
    $("#tariff_name").val(SELECTED_TARIFF.name);
    $("#price").val(SELECTED_TARIFF.price);
}

function switch_blocked(flag,isSimpleToggle) {
    if(!flag) {
        $("#is_blocked").css('color', '#dc000d').html("Locked");
        $("#block_check").prop('checked', false);
        $("#contract_info_block").css('display','none');
        return true;
    }
    else if(isSimpleToggle || (!SELECTED_CONTRACT.whoBlocked || (IS_MANAGER != null && IS_MANAGER))) {
        $("#is_blocked").css('color', '#00dc27').html("Unlocked");
        $("#block_check").prop('checked', true);
        $("#contract_info_block").css('display','');
        return true;
    }
    return false;

}

$(document).ready(function () {

    $("#forRemove").remove();

    $("#close_message").on('click', function () {
        $("#error_block").css('display','none');
    });
    $("#error_block").css('display','none');
    $("#info_block").css('display','none');

    $("#switch_block").on('click', function() {
        var flag = !SELECTED_CONTRACT.active;
        var result = switch_blocked(flag);
        if(result) {
            SELECTED_CONTRACT.active = flag;
        } else {
            $("#error_message_text").html("Cann't unlocked contract. Contract locked manager");
            $("#error_block").css('display','');
        }
    });

    var key;

    for (key in contracts) {
        $("#contracts").append(new Option(contracts[key].phoneNumber, contracts[key].phoneNumber));
    }

    for (key in allTariffs) {
        $("#tariff_name").append(new Option(allTariffs[key].name, allTariffs[key].name))
    }

    for (key in contracts) {
        SELECTED_CONTRACT = contracts[key];
        SELECTED_TARIFF = SELECTED_CONTRACT.tariff;
        SELECTED_OPTIONS = SELECTED_CONTRACT.options;
        break;
    }

    $("#add_option").prop('disabled', true).on("click", function () {
        addOption();
        $("#addOptionsModal").modal('hide');
    });

    $("#delete_button").prop('disabled', true).on("click", function () {
        deleteOption();
    });

    $("#saveButton").button().on("click", function () {
        saveContract();
    });

    initContract();

    $('#cancel_change_tariff_button').hide();

    $("#contracts").selectpicker('show');
    $("#contracts").selectpicker().change(function (e) {
        SELECTED_CONTRACT = contracts[$(this).val()];
        SELECTED_OPTIONS = SELECTED_CONTRACT.options;
        initContract();
    });

    $('#change_tariff_button').hide();

    function createModalWindow(id) {
        $('#' + id).modal('show');
    }

    var selectedTariff = "";

    $('#change_button').on("click", function () {
        selectedTariff = $("#tariff_name").val();
        activateButton('change_tariff_button', true);
        $("#tariff_name").prop('disabled', false);
        $('#change_tariff_button').show();
        $('#cancel_change_tariff_button').show();
    });

    $('#cancel_change_tariff_button').on("click", function () {
        $("#tariff_name").val(selectedTariff);
        $('#tariff_name').prop('disabled', true);
        $('#change_tariff_button').hide();
        $('#cancel_change_tariff_button').hide();
        activateButton('change_tariff_button', false);
    });

    $('#options_table').on('click', 'tr', function () {
        var previousRows = $('#options_table').find('.highlighted');
        previousRows.removeClass('highlighted');
        $('#delete_button').prop('disabled', false);
        CURRENT_SELECTED_OPTION = SELECTED_TARIFF.options[$(this).attr("option_id")];
        $(this).addClass('highlighted');
    });

    $('#options_list').on('click', 'li', function () {
        if($(this).hasClass("incompatible") || $(this).hasClass("disabled"))
            return;
        $('#options_list').find('.highlighted').removeClass('highlighted');
        $(this).addClass('highlighted');
        CURRENT_SELECTED_OPTION = SELECTED_TARIFF.options[$(this).attr("option_id")];
        $("#add_option").prop('disabled', false);
    });


    $('#change_tariff_button').on("click", function () {
        changeTariff();
        $('#tariff_name').prop('disabled', true);
        $('#change_tariff_button').hide();
        $('#cancel_change_tariff_button').hide();
        activateButton('change_tariff_button', false);
    });


    $('#add_button').on("click", function () {
        $("#modal_characteristic_field").val(SELECTED_TARIFF.name);
        $("#options_list").html("");
        SELECTED_TARIFF.options.forEach(function (value) {
            var isDisabled = SELECTED_OPTIONS.some(function (val) {
                if (value.id == val.id)
                    return true;
            });
            var isIncompatible = SELECTED_OPTIONS.some(function (val) {
                return val.iOptions.some(function (v) {
                    if (v.id == value.id)
                        return true;
                });
            });
            isDisabled |= isIncompatible;
            var isCompatible = SELECTED_OPTIONS.some(function (val) {
                return val.cOptions.some(function (v) {
                    if (v.id == value.id)
                        return true;
                });
            });
            $("#options_list").append("<li option_id = \"" + value.id + "\" class=\"list-group-item " +
                (isCompatible ? "compatible" : "") + " " +
                (isIncompatible ? "incompatible" : "") + " " +
                (isDisabled ? "disabled" : "") + "\"" +
                (isCompatible || isIncompatible || isDisabled ? "title = \"" + (isCompatible ? "Option must be connect" :
                    (isIncompatible ? "This option is not compatible" :"Option already connected")) : "") + "\"" +
                ">" +
                "<span class=\"strong\">" + value.name + "</span>" +
                "<span>(Price: " + value.price + " ,Cost : " + value.cost + ")</span></li>");
        });
        createModalWindow('addOptionsModal');
        activateButton('change_tariff_button', false);
    });


    function activateButton(id, flagAction) {
        var buttonsId = ["change_button", "add_button", "change_tariff_button", "saveButton"];
        for (var i = 0; i < buttonsId.length; i++) {
            if (buttonsId[i] != id) {
                $("#" + buttonsId[i]).prop('disabled', flagAction);
            }
        }
    }

});






