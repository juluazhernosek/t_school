var tariffs = {};
var SELECTED_TARIFF = null;
var SELECTED_OPTION = null;
var SELECTED_COMPATIBLE_OPTION = null;
var SELECTED_INCOMPATIBLE_OPTION = null;
var SERVER_ERROR = "Server Error.";

function fillAvailableCompatibleOptions() {
    var options = $.grep(SELECTED_TARIFF.options,function(value) {
        return value != undefined && SELECTED_OPTION.id != value.id &&
            !($.inArray(value, SELECTED_OPTION.iOptions) != -1 || $.inArray(value, SELECTED_OPTION.cOptions) != -1);
    });
    var body = "";
    options.forEach(function(value) {
        body += "<tr option-id=\"" + value.id + "\"><td>" + value.name + "</td>";
        body += "<td>" + value.cost + "</td>";
        body += "<td>" + value.price + "</td></tr>";
    });
    $("#add_compatible_options").html(body);
}

function fillAvailableIncompatibleOptions() {
    var options = $.grep(SELECTED_TARIFF.options,function(value) {
        return value != undefined && SELECTED_OPTION.id != value.id &&
            !($.inArray(value, SELECTED_OPTION.iOptions) != -1 || $.inArray(value, SELECTED_OPTION.cOptions) != -1);
    });
    var body = "";
    options.forEach(function(value) {
        body += "<tr option-id=\"" + value.id + "\"><td>" + value.name + "</td>";
        body += "<td>" + value.cost + "</td>";
        body += "<td>" + value.price + "</td></tr>";
    });
    $("#add_incompatible_options").html(body);
}

function CreateOptionRequest(idTariff,name,price,cost) {
    this.idTariff = idTariff;
    this.name = name;
    this.price = price;
    this.cost = cost;
}

function isValidOption(option) {
    var pattern = /^[0-9]{1,999}(?:\.\d{1,3})?$/;
    if(option.name == "")
        return "Not valid name";
    if(!pattern.test(option.price))
        return "Not valid price";
    if(!pattern.test(option.cost))
        return "Not valid cost";
    return null;
}

function isValidTariff(tariff) {
    var pattern = /^[0-9]{1,999}(?:\.\d{1,3})?$/;
    if(tariff.name == "")
        return "Not valid name";
    if(!pattern.test(tariff.price))
        return "Not valid price";
    return null;
}

function createOption() {
    var option = new CreateOptionRequest(SELECTED_TARIFF.id,$("#option_name").val(),$("#option_price").val(),$("#option_cost").val(),[],[]);
    var isValid = isValidOption(option);
    if(isValid != null) {
        $("#error_message_text").html(isValid);
        $("#error_block").css('display','');
        return;
    }
    $.ajax({
        type: 'POST',
        contentType : 'application/json',
        async : false,
        data : JSON.stringify(option),
        dataType: 'text',
        url : 'allContracts/options/create',
        success : function(response) {
            try {
                response = JSON.parse(response);
            } catch (e) {}
            if(response != "success" && (response.status != undefined ? (response.status != "success") : true )) {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                $("#options").append("<tr option-id=\"" + response.idOption + "\">" +
                    "<td>" + option.name + "</td>" +
                    "<td>" + option.cost + "</td>" +
                    "<td>" + option.price + "</td></tr>");
                SELECTED_TARIFF.options[response.idOption] = new OptionECare(response.idOption,option.name,option.cost,
                    option.price,[],[]);
                $("#delete_option_button").prop("disabled",true);
            }
            $("#option_name").val("");$("#option_cost").val("");$("#option_price").val("");
        },
        statusCode: {
            500: function() {
                $("#error_message_text").html(SERVER_ERROR);
                $("#error_block").css('display','');
            },
            404: function() {
                $("#error_message_text").html(SERVER_ERROR);
                $("#error_block").css('display','');
            }
        }
    });
}

function createTariff() {
    var tariff = new TariffECare("0",$("#new_tariff_name").val(),$("#new_tariff_price").val());
    var isValid = isValidTariff(tariff);
    if(isValid != null) {
        $("#error_message_text").html(isValid);
        $("#error_block").css('display','');
        return;
    }
    $.ajax({
        type: 'POST',
        contentType : 'application/json',
        async : false,
        data : JSON.stringify(tariff),
        dataType: 'text',
        url : 'allContracts/tariffs/create',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else
                location.reload();
        }
    });
    $('#addNewTariffModal').modal('hide');
}

function deleteTariff() {
    $.ajax({
        type: 'POST',
        async : false,
        dataType: 'text',
        url : 'tariffsAndOptions/tariff/remove/' + SELECTED_TARIFF.id,
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                $("#tariffs").find("tr[name=" + SELECTED_TARIFF.name + "]").remove();
                $("#delete_option_button").prop("disabled",true);
            }
        }
    })
}

function deleteOption() {
    $.ajax({
        type: 'POST',
        async : false,
        dataType: 'text',
        url : 'tariffsAndOptions/option/remove/' + SELECTED_OPTION.id,
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                $("#options").find("tr[option-id=" + SELECTED_OPTION.id + "]").remove();
                $("#delete_option_button").prop("disabled",true);
            }
        }
    })
}

function createCompatibleOption() {
    var request = {};
    request["idOption1"] = SELECTED_OPTION.id;
    request["idOption2"] = SELECTED_COMPATIBLE_OPTION.id;
    request["compatible"] = true;
    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        data : JSON.stringify(request),
        url : 'tariffsAndOptions/rule/create',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                SELECTED_OPTION.cOptions[SELECTED_COMPATIBLE_OPTION.id] = SELECTED_COMPATIBLE_OPTION;
                SELECTED_COMPATIBLE_OPTION.cOptions[SELECTED_OPTION.id] = SELECTED_OPTION;
                fillRules();
                $("#add_compatible_options").prop("disabled",true);
            }
        }
    });
    $('#addNewCompatibleOptionModal').modal('hide');
}

function createIncompatibleOption() {
    var request = {};
    request["idOption1"] = SELECTED_OPTION.id;
    request["idOption2"] = SELECTED_INCOMPATIBLE_OPTION.id;
    request["compatible"] = false;
    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        data : JSON.stringify(request),
        url : 'tariffsAndOptions/rule/create',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                SELECTED_OPTION.iOptions[SELECTED_INCOMPATIBLE_OPTION.id] = SELECTED_INCOMPATIBLE_OPTION;
                SELECTED_INCOMPATIBLE_OPTION.iOptions[SELECTED_OPTION.id] = SELECTED_OPTION;
                fillRules();
                $("#add_incompatible_options").prop("disabled",true);
            }
        }
    });
    $('#addNewIncompatibleOptionModal').modal('hide');
}

function clearCompatibleOptions() {
    options ="<tr class='no-options'><td>No options</td></tr>";
    $("#compatible_options").html(options);
    $("#incompatible_options").html(options);
}

function deleteIncompatibleOption() {
    var request = {};
    request["idOption1"] = SELECTED_OPTION.id;
    request["idOption2"] = SELECTED_INCOMPATIBLE_OPTION.id;
    request["compatible"] = false;
    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        data : JSON.stringify(request),
        url : 'tariffsAndOptions/rule/remove',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                SELECTED_INCOMPATIBLE_OPTION.iOptions.splice(parseInt(SELECTED_OPTION.id),1);
                SELECTED_OPTION.iOptions.splice(parseInt(SELECTED_INCOMPATIBLE_OPTION.id),1);
                fillRules();
                $("#delete_incompatible_options_button").prop("disabled",true);
            }
        }
    })
}

function deleteCompatibleOption() {
    var request = {};
    request["idOption1"] = SELECTED_OPTION.id;
    request["idOption2"] = SELECTED_COMPATIBLE_OPTION.id;
    request["compatible"] = true;
    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        data : JSON.stringify(request),
        url : 'tariffsAndOptions/rule/remove',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                SELECTED_COMPATIBLE_OPTION.cOptions.splice(parseInt(SELECTED_OPTION.id),1);
                SELECTED_OPTION.cOptions.splice(parseInt(SELECTED_COMPATIBLE_OPTION.id),1);
                fillRules();
                $("#delete_compatible_options_button").prop("disabled",true);
            }
        }
    })
}

function initTariffs () {
    var body = "";
    for(var key in tariffs) {
        body += "<tr name=\"" + tariffs[key].name + "\"><td>" + tariffs[key].name + "</td>";
        body += "<td>" + tariffs[key].price + "</td></tr>";
    }
    $("#tariffs").html(body);
}
function activateOptionsBlock() {
    $("#big_options_block").removeClass("inactive");
    $("#delete_option_button").prop("disabled",true);
    $("#add_option_button").prop("disabled",false);
    $("#add_compatible_options_button").prop("disabled",false);
    $("#delete_compatible_options_button").prop("disabled",true);
    $("#add_incompatible_options_button").prop("disabled",false);
    $("#delete_incompatible_options_button").prop("disabled",true);
}

function deactivateRulesForOptionsBlock() {
    if(!$("#rules_for_options").hasClass("inactive"))
        $("#rules_for_options").addClass("inactive");
    $("#add_compatible_options_button").prop("disabled",true);
    $("#delete_compatible_options_button").prop("disabled",true);
    $("#add_incompatible_options_button").prop("disabled",true);
    $("#delete_incompatible_options_button").prop("disabled",true);
}

function activateRulesForOptionsBlock() {
    $("#rules_for_options").removeClass("inactive");
    $("#add_compatible_options_button").prop("disabled",false);
    $("#delete_compatible_options_button").prop("disabled",true);
    $("#add_incompatible_options_button").prop("disabled",false);
    $("#delete_incompatible_options_button").prop("disabled",true);
}

function deactivateOptionsBlock() {
    if(!$("#big_options_block").hasClass("inactive"))
        $("#big_options_block").addClass("inactive");
    $("#delete_option_button").prop("disabled",true);
    $("#add_option_button").prop("disabled",true);
    $("#add_compatible_options_button").prop("disabled",true);
    $("#delete_compatible_options_button").prop("disabled",true);
    $("#add_incompatible_options_button").prop("disabled",true);
    $("#delete_incompatible_options_button").prop("disabled",true);
}
function fillOptionsBlock() {
    var options = "";
    SELECTED_TARIFF.options.forEach(function(value) {
        options += "<tr option-id=\"" + value.id + "\"><td>" + value.name + "</td>";
        options += "<td>" + value.cost + "</td>";
        options += "<td>" + value.price + "</td></tr>";
    });
    $("#options").html(options);
    options = "";
}

function fillRules() {
    var options = "";
        if(SELECTED_OPTION.cOptions.length)
    SELECTED_OPTION.cOptions.forEach(function(value) {
       options += "<tr option-id=\"" + value.id + "\"><td>" + value.name + "</td></tr>";
    });
    options = (options == "" ? "<tr class='no-options'><td>No options</td></tr>" : options);
    $("#compatible_options").html(options);
    options = "";
    SELECTED_OPTION.iOptions.forEach(function(value) {
        options += "<tr option-id=\"" + value.id + "\"><td>" + value.name + "</td></tr>";
    });
    options = (options == "" ? "<tr class='no-options'><td>No options</td></tr>" : options);
    $("#incompatible_options").html(options);
}
$(document).ready(function(){
    $("#forRemove").remove();

    $("#close_message").on('click', function () {
        $("#error_block").css('display','none');
    });
    $("#error_block").css('display','none');

    $("#compatible_options").on("click", "tr",function() {
        if($(this).hasClass("no-options"))
            return;
        var previousRows = $('#compatible_options').find('.selected');
        previousRows.removeClass('selected');
        $(this).addClass('selected');
        SELECTED_COMPATIBLE_OPTION = SELECTED_OPTION.cOptions[$(this).attr("option-id")];
        $("#delete_compatible_options_button").prop("disabled",false);
    });

    $("#incompatible_options").on("click", "tr",function() {
        if($(this).hasClass("no-options"))
            return;
        var previousRows = $('#inccompatible_options').find('.selected');
        previousRows.removeClass('selected');
        $(this).addClass('selected');
        SELECTED_INCOMPATIBLE_OPTION = SELECTED_OPTION.iOptions[$(this).attr("option-id")];
        $("#delete_incompatible_options_button").prop("disabled",false);
    });

    $("#tariffs").on("click","tr",function() {
        SELECTED_TARIFF = tariffs[$(this).attr("name")];
        fillOptionsBlock();
        activateOptionsBlock();
        clearCompatibleOptions();
        deactivateRulesForOptionsBlock();
        $("#delete_tariff_button").prop("disabled",false);
        var previousRows = $('#tariffs').find('.selected');
        previousRows.removeClass('selected');
        $(this).addClass('selected');
    });

    $("#options").on("click","tr",function() {
        SELECTED_OPTION = SELECTED_TARIFF.options[$(this).attr("option-id")];
        fillRules();
        activateRulesForOptionsBlock();
        $("#delete_option_button").prop("disabled",false);
        var previousRows = $('#options').find('.selected');
        previousRows.removeClass('selected');
        $(this).addClass('selected');
    });

    $("#add_incompatible_options").on("click","tr",function() {
        SELECTED_INCOMPATIBLE_OPTION = SELECTED_TARIFF.options[$(this).attr("option-id")];
        $("#add_new_incompatible_option_button").prop("disabled",false);
        var previousRows = $('#add_incompatible_options').find('.selected');
        previousRows.removeClass('selected');
        $(this).addClass('selected');
    });

    $("#add_compatible_options").on("click","tr",function() {
        SELECTED_COMPATIBLE_OPTION = SELECTED_TARIFF.options[$(this).attr("option-id")];
        $("#add_new_compatible_option_button").prop("disabled",false);
        var previousRows = $('#add_compatible_options').find('.selected');
        previousRows.removeClass('selected');
        $(this).addClass('selected');
    });

    $("#delete_compatible_options_button").on("click", function() {
        deleteCompatibleOption();
    });

    $("#delete_incompatible_options_button").on("click", function() {
        deleteIncompatibleOption();
    });

    $("#delete_option_button").on("click", function() {
        deleteOption();
    });

    $('#delete_tariff_button').on("click", function () {
        deleteTariff();
    });

    $('#add_tariff_button').on("click", function () {
        $('#addNewTariffModal').modal('show');
    });

    $('#add_compatible_options_button').on("click", function () {
        fillAvailableCompatibleOptions();
        $("#add_new_compatible_option_button").prop("disabled",true);
        $('#addNewCompatibleOptionModal').modal('show');
    });

    $("#add_new_compatible_option_button").on("click", function () {
        createCompatibleOption();
    });

    $("#add_new_incompatible_option_button").on("click", function () {
        createIncompatibleOption();
    });

    $("#add_new_tariff_button").on("click", function () {
        createTariff();
    });

    $("#add_option_button").on("click", function () {
        createOption();
    });

    $('#add_incompatible_options_button').on("click", function () {
        fillAvailableIncompatibleOptions();
        $("#add_new_incompatible_option_button").prop("disabled",true);
        $('#addNewIncompatibleOptionModal').modal('show');
    });

    initTariffs();

    $('#option_compatible').selectpicker();
    $('#compatible_option').selectpicker();
    $('#option_incompatible').selectpicker();
    $('#incompatible_option').selectpicker();

});
