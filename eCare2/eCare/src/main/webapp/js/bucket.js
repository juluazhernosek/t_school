var contracts = [];
var SELECTED_CONTRACT = null;
var allTariffs = [];

$( document ).ready(function ()
{
    $("#forRemove").remove();

    for (var key in contracts) {
        var contract = contracts[key].phoneNumber;
        if($.session.get("new." + contract + ".options") != undefined ||
            $.session.get("delete." + contract + ".options") != undefined ||
            $.session.get("new." + contract + ".tariff") != undefined) {
            $("#contracts_bucket").append(new Option(contract, contract));
        }
    }

    SELECTED_CONTRACT = contracts[$("#contracts_bucket").val()];

    initContractBucket();

    $("#contracts_bucket").selectpicker().change(function () {
        SELECTED_CONTRACT = contracts[$(this).val()];
        initContractBucket();
    });

    $("#revertButton").on('click', function(){
        clearSession();
        location.reload();
    });

    function clearSession() {
        if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".tariff") != undefined)
            $.session.remove("old." + SELECTED_CONTRACT.phoneNumber + ".tariff");
        if ($.session.get("old." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined)
            $.session.remove("old." + SELECTED_CONTRACT.phoneNumber + ".options");
        if ($.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".tariff") != undefined)
            $.session.remove("new." + SELECTED_CONTRACT.phoneNumber + ".tariff");
        if ($.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined)
            $.session.remove("new." + SELECTED_CONTRACT.phoneNumber + ".options");
        if ($.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined)
            $.session.remove("delete." + SELECTED_CONTRACT.phoneNumber + ".options");
    }

    function initContractBucket(){
        if(SELECTED_CONTRACT != null){
            var tariff = null;
            if($.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".tariff") != undefined) {
                tariff = allTariffs[$.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".tariff")];
                $("#tariff_name").val(tariff.name);
            }
            if($.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined && $.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options") != ""){
                var added_options = $.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options").split(",");
                $("#added_options_table").html("");
                added_options.forEach(function (value) {
                    var option = (tariff == null ? SELECTED_CONTRACT.tariff.options[value] : tariff.options[value]);
                    $("#added_options_table").append("<tr><td>" + option.name + "</td>" +
                        "<td>" + option.cost + "</td>" +
                        "<td>" + option.price + "</td>" +
                        "</tr>");
                });
            }
            if($.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options") != undefined && $.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options") != ""){
                $("#deleted_options_table").html("");
                var deleted_options = $.session.get("delete." + SELECTED_CONTRACT.phoneNumber + ".options").split(",");
                deleted_options.forEach(function (value) {
                    var option = SELECTED_CONTRACT.tariff.options[value];
                    $("#deleted_options_table").append("<tr><td>" + option.name + "</td>" +
                        "<td>" + option.cost + "</td>" +
                        "<td>" + option.price + "</td>" +
                        "</tr>");
                });
            }
        }
    }
}
)
