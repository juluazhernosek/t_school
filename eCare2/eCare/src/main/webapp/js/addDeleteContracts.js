var USER_ID = null;
var IS_MANAGER = true;
function isValidNumber(number) {
    var pattern = /^\d+$/;
    return pattern.test(number);
}

function deleteContract() {
    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        url : 'contracts/remove/' + SELECTED_CONTRACT.id,
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                location.reload();
            }
        }
    });
}

function createContract() {
    var number = $("#new_contract_number").val();
    if(!isValidNumber(number)) {
        $("#error_message_text").html("Number not valid");
        $("#error_block").css('display','');
        return;
    }
    var contract = new ContractECare(0,number,false,true);

    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        data : JSON.stringify(contract),
        url : USER_ID + '/contracts/create',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                location.reload();
            }
        }
    });
    $("#addContractsModal").modal("hide");
}

$(document).ready(function() {
    $("#addContract").on("click",function() {
        $("#addContractsModal").modal("show");
    });
    $("#add_new_contract").on("click", function() {
        createContract();
    });
    $("#deleteContract").on("click",function() {
        deleteContract();
    });

});
