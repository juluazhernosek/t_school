var users = {};
var SELECTED_USER = null;

function isValidName(name){
    var pattern = /^[a-zA-Z\s]{1,20}$/;
    return pattern.test(name);
}

function isValidSurname(surname){
    var pattern = /^[a-zA-Z\s]{1,20}$/;
    return pattern.test(surname);
}

function isValidPassword(password){
    return password != "" && password.length < 25;
}

function isValidBirthday(date){
    var pattern = /^\d{2}\/\d{2}\/\d{4}$/;
    return pattern.test(date);
}

function isValidPassport(passport){
    return passport != "" && passport.length < 50;
}

function isValidAddress(address){
    return address != "" && address.length < 50;
}

function isValidEmail(email){
    var pattern = /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
    return pattern.test(email);
}

function validMessageUser(name,surname,email,password,address,date,passport) {
    if(!isValidName(name))
        return "Name not valid";
    else if(!isValidSurname(surname))
        return "Surname not valid";
    else if(!isValidEmail(email))
        return "Email not valid";
    else if(!isValidPassword(password))
        return "Password not valid";
    else if(!isValidAddress(address))
        return "Address not valid";
    else if(!isValidBirthday(date))
        return "Birthday date not valid";
    else if(!isValidPassport(passport))
        return "Passport not valid";
    return null;
}

function createUser() {
    var name = $("#new_user_name").val();
    var surname = $("#new_user_surname").val();
    var email = $("#new_user_email").val();
    var password = $("#new_user_password").val();
    var address = $("#new_user_address").val();
    var date = $("#new_user_birthday").val();
    var passport = $("#new_user_passport").val();
    var message = validMessageUser(name,surname,email,password,address,date,passport);
    var role = $("#new_user_role").attr("checked");
    if(message != null) {
        $("#error_message_text").html(message);
        $("#error_block").css('display','');
        return;
    }
    var partDate = date.split("/");
    var user = new UserECare(0,name,surname,new Date(partDate[2],partDate[0],partDate[1]),passport,address,email,[],role);
    var request = {};
    request.user = user;
    request.password = password;
    request.role = role;
    var locate = location.href.substr(0,location.href.indexOf("search"));
    locate = locate == "" ? "allContracts/" : locate;
    $.ajax({
        type: 'POST',
        async : false,
        contentType : 'application/json',
        dataType: 'text',
        data : JSON.stringify(request),
        url : locate + 'users/create',
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                location.reload();
            }
        }
    });
    $("#addNewUserModal").modal("hide");
}

function deleteUser() {
    var locate = location.href.substr(0,location.href.indexOf("search"));
    locate = locate == "" ? "allContracts/" : locate;
    $.ajax({
        type: 'POST',
        async : false,
        dataType: 'text',
        url : locate + 'user/remove/' + SELECTED_USER.id,
        success : function(response) {
            if(response != "success") {
                $("#error_message_text").html(response);
                $("#error_block").css('display','');
            }
            else {
                location.reload();
            }
        }
    })
}


function url_redirect(options) {
    var $form = $("<form />");

    $form.attr("action", options.url);
    $form.attr("method", options.method);

    for (var data in options.data)
        $form.append('<input type="hidden" name="' + data + '" value="' + options.data[data] + '" />');

    $("body").append($form);
    $form.submit();
    $form.remove();
}

function addUser(user) {
    var body = "";
    body += "<tr user-id=\"" + user.id + "\"><td>" + user.name + "</td>";
    body += "<td>" + user.surname + "</td>";
    body += "<td>" + user.email + "</td>";
    body += "<td>" + user.birthdayDate.toDateString() + "</td>";
    body += "<td>" + user.passport + "</td></tr>";
    $("#users").append(body);
}

$(document).ready(function() {
    $("#users").html("").on("click","tr", function() {
        SELECTED_USER = users[$(this).attr("user-id")];
        var previousRows = $('#users').find('.selected');
        previousRows.removeClass('selected');
        $(this).addClass('selected');
        $("#deleteUserButton").prop("disabled",false);
        $("#showContracts").prop("disabled",false);
    });
    for(var key in users) {
        addUser(users[key]);
    }

    $("#showContracts").prop("disabled",true).on("click", function() {
        url_redirect({url:"allContracts/user/" + SELECTED_USER.id,method : "GET"})
    });
    $('#userTable').dataTable({
        bFilter: false,
        bInfo: false
    });

    $("#close_message").on('click', function () {
        $("#error_block").css('display','none');
    });
    $("#error_block").css('display','none');

    $("#search_button").on("click",function() {
        var locate = location.href.substr(0,location.href.indexOf("search"));
        locate = locate == "" ? "allContracts/" : locate;
        url_redirect({url: locate + "search", method: "GET", data: {number : $("#searchField").val()}})
    });

    $("#add_new_user_button").on("click",function() {
        $("#addNewUserModal").modal("show");
    });
    $('#new_user_birthday').datepicker();

    $("#addUserButton").on("click",function() {
        createUser();
    });

    $("#deleteUserButton").prop("disabled",true).on("click", function() {
        deleteUser();
    });

});