var contracts = [];
var SELECTED_CONTRACT_BUCKET = null;
function clearSessionManager() {
    if ($.session.get("old." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".tariff") != undefined)
        $.session.remove("old." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".tariff");
    if ($.session.get("old." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options") != undefined)
        $.session.remove("old." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options");
    if ($.session.get("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".tariff") != undefined)
        $.session.remove("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".tariff");
    if ($.session.get("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options") != undefined)
        $.session.remove("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options");
    if ($.session.get("delete." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options") != undefined)
        $.session.remove("delete." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options");
}


function initContractManager(){
    if(SELECTED_CONTRACT_BUCKET != null) {
        $("#tariff_name_bucket").html("");
        if($.session.get("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".tariff") != undefined && $.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".tariff") != "") {
            $("#tariff_name_bucket").val($.session.get("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".tariff"));
        }
        $("#added_options_table_bucket").html("");
        if($.session.get("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options") != undefined && $.session.get("new." + SELECTED_CONTRACT.phoneNumber + ".options") != ""){
            var added_options = $.session.get("new." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options").split(",");
            added_options.forEach(function (value) {
                var option = allTariffs[$("#tariff_name_bucket").val()].options[value];
                $("#added_options_table_bucket").append("<tr><td>" + option.name + "</td>" +
                    "<td>" + option.cost + "</td>" +
                    "<td>" + option.price + "</td>" +
                    "</tr>");
            });
        }
        $("#deleted_options_table_bucket").html("");
        if($.session.get("delete." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options") != undefined && $.session.get("delete." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options") != ""){
            var deleted_options = $.session.get("delete." + SELECTED_CONTRACT_BUCKET.phoneNumber + ".options").split(",");
            deleted_options.forEach(function (value) {
                var tariff = (allTariffs[$("#tariff_name_bucket").val()].options[value] == undefined ? SELECTED_CONTRACT_BUCKET.tariff : allTariffs[$("#tariff_name_bucket").val()]);
                var option = tariff.options[value];
                $("#deleted_options_table_bucket").append("<tr><td>" + option.name + "</td>" +
                    "<td>" + option.cost + "</td>" +
                    "<td>" + option.price + "</td>" +
                    "</tr>");
            });
        }
    }
}
$(document).ready(function () {

    var removeBlock = null;

    $("#block_bucket").popover({
        html: true,
        content: function () {
            if(removeBlock == null) {
                removeBlock = $("#bucket_block_remove").html();
                $("#bucket_block_remove").remove();
            }
            return removeBlock;
        },
        placement: 'bottom',
        container: 'body'
    }).on('shown.bs.popover', function () {
        for (key in contracts) {
            var contract = contracts[key].phoneNumber;
            if($.session.get("new." + contract + ".options") != undefined ||
                $.session.get("delete." + contract + ".options") != undefined ||
                $.session.get("new." + contract + ".tariff") != undefined) {
                $("#contracts_card").append(new Option(contract, contract));
            }
        }
        SELECTED_CONTRACT_BUCKET = contracts[$("#contracts_card").val()];
        $("#contracts_card").change(function(){
            SELECTED_CONTRACT_BUCKET = contracts[$(this).val()];
            initContractManager();
        });
        $("#revertButtonManager").on("click", function() {
            clearSessionManager();
            initContractManager();
            location.reload();
        });
        initContractManager();
        $('#bucket_tab').find('a').on('click',function() {
            $(this).tab('show');
        });
    });

    $("#forRemove").remove();

    $("#add_option").on("click", function () {
        setTimeout(function() {
            SELECTED_CONTRACT_BUCKET = contracts[$("#contracts_card").val()];
            initContractManager();
        },10);
    });

    $("#delete_button").on("click", function () {
        setTimeout(function() {
            SELECTED_CONTRACT_BUCKET = contracts[$("#contracts_card").val()];
            initContractManager();
        },10);
    });

    $('#change_tariff_button').on("click", function () {
        setTimeout(function() {
            SELECTED_CONTRACT_BUCKET = contracts[$("#contracts_card").val()];
            initContractManager();
        },10);
    });




});
