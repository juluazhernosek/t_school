function OptionECare(id,name,cost,price,cOptions, iOptions) {
    this.id = id;
    this.name = name;
    this.cost = cost;
    this.price = price;
    this.cOptions = cOptions;
    this.iOptions = iOptions;
}


function TariffECare(id,name,price,options) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.options = options;

}

function ContractECare(id,number,active,blocked,tariff,options) {
    this.id = id;
    this.phoneNumber = number;
    this.active = active;
    this.whoBlocked = blocked;
    this.tariff = tariff;
    this.options = options;
}

function UserECare(id,name,surname,birthdayDate,passport,address,email,contracts,role) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.birthdayDate = birthdayDate;
    this.passport = passport;
    this.address = address;
    this.email = email;
    this.contracts = contracts;
    this.role = role;
}
