
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="/WEB-INF/views/layouts/_manager_head.jsp" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/personal_cabinet.css" />
</head>
<body>
<div class = "main_block">
    <c:set var="activeMenu" value="index" />
    <%@ include file="/WEB-INF/views/layouts/_manager_body.jsp" %>
    <div class = "data clearfix">
        <div id ="personal_data">
            <h2>Your personal data:</h2>
            <p><img src="${pageContext.request.contextPath}/img/manager.png" width ="150px" height = "180px" id = "user_image"></p>
            <div id = "profile">
                <label class = "profile_label">Name:</label>
                <input type="text" class="user_field" id = "user_name" value = "${user.getName()}" disabled="disabled">
                <br>
                <label class = "profile_label">Surname:</label>
                <input type="text" class="user_field" id = "user_surname" value = "${user.getSurname()}" disabled="disabled">
                <br>
                <label class = "profile_label">Birthday:</label>
                <input type="text" class="user_field" id = "birthday" value = "${user.getBirthdayDate()}" disabled="disabled">
                <br>
                <label class = "profile_label">Passport:</label>
                <input type="text" class="user_field" id = "passport" value = "${user.getPassport()}" disabled="disabled">
                <br>
                <label class = "profile_label">Address:</label>
                <input type="text" class="user_field" id = "address" value = "${user.getAddress()}" disabled="disabled">
                <br>
                <label class = "profile_label">Email:</label>
                <input type="text" class="user_field" id = "email" value = "${user.getEmail()}" disabled="disabled">
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
</div>
</body>
</html>
