
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="/WEB-INF/views/layouts/_manager_head.jsp" %>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/bootstrap/jquery.dataTables.css"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/bootstrap/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/allContracts.css"/>

    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/libs/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/allContracts.js"></script>
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/objects.js"></script>
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/libs/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap-datepicker.js"></script>
    <script type="text/javascript" language="javascript">
        <c:forEach items="${users}" var="user">
            users["${user.id}"] = new UserECare("${user.id}","${user.name}","${user.surname}",
                    new Date("${user.birthdayDate}"),"${user.passport}","${user.address}","${user.email}",[],false);
        </c:forEach>
    </script>
</head>
<body>
<div id="error_block">
    <div id="error_message" class="alert alert-danger alert-dismissible" role="alert">
        <button id="close_message" type="button" class="close"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <strong>Error!</strong> <span id="error_message_text"></span>
    </div>
</div>
<div class="main_block">
    <c:set var="activeMenu" value="contracts" />
    <%@ include file="/WEB-INF/views/layouts/_manager_body.jsp" %>
    <div id="title_user">Users</div>
    <div class="container">
        <div id="search_block">
            <label for="searchField" id="searchLabel">Search (phone)</label>
            <input class="characteristic_field" type="text" id="searchField">
            <button type="button" class="btn btn-info" id="search_button">Search</button>
        </div>
        <table class="table" id="userTable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Email</th>
                <th>Birthday</th>
                <th>Passport</th>
            </tr>
            </thead>
            <tbody id="users">
            </tbody>
        </table>
        <button id = "add_new_user_button" type="button" class="btn btn-success dropdown-toggle">Add user</button>
        <button type="button" class="btn btn-danger dropdown-toggle universalButton" id="deleteUserButton">
            Delete
        </button>
        <button type="button" class="btn btn-info dropdown-toggle universalButton" id="showContracts">
            Show/Change contracts
        </button>
    </div>
    <%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
</div>
<div class="modal fade" id="addNewUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Add new user</h4>
            </div>
            <div class="modal-body">
                <div id="profile">
                    <label class="profile_label">Name:</label>
                    <input type="text" class="user_field" id="new_user_name">
                    <br>
                    <label class="profile_label">Surname:</label>
                    <input type="text" class="user_field" id="new_user_surname">
                    <br>
                    <label class="profile_label">Birthday:</label>
                    <input type="text" class="user_field" id="new_user_birthday">
                    <br>
                    <label class="profile_label">Passport:</label>
                    <input type="text" class="user_field" id="new_user_passport">
                    <br>
                    <label class="profile_label">Address:</label>
                    <input type="text" class="user_field" id="new_user_address">
                    <br>
                    <label class="profile_label">Email:</label>
                    <input type="email" class="user_field" id="new_user_email">
                    <br>
                    <label class="profile_label">Password:</label>
                    <input type="text" class="user_field" id="new_user_password">
                    <br>
                    <label class="profile_label">Manager</label>
                    <input type="checkbox" class="user_field" id="new_user_role">
                </div>
            </div>
            <div class="modal-footer">
                <button id="addUserButton" type="button" class="btn btn-success dropdown-toggle">Add</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
