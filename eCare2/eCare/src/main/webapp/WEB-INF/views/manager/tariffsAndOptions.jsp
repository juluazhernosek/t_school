
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/views/layouts/_manager_head.jsp" %>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/bootstrap/jquery.dataTables.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/tariffs_options.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap-select.css">
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/libs/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/libs/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/tarriffsAndOptions.js"></script>
    <script type="text/javascript" language="javascript"
            src="${pageContext.request.contextPath}/js/objects.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap-select.js"></script>
    <script id="forRemove" type="text/javascript">
        var cacheOptions = [];
        <c:forEach items="${allTariffs}" var="tariff">
            var cOptions;
            var iOptions;
            tariffOptions = [];
            <c:forEach items="${tariff.options}" var="option">
                cOptions = [];
                iOptions = [];
                var option;
                var newOption;
                if(cacheOptions["${option.id}"] == undefined) {
                    newOption = new OptionECare("${option.id}", "${option.name}",
                            ${option.cost}, ${option.price}, cOptions, iOptions)
                    cacheOptions[newOption.id] = newOption;
                } else
                    newOption = cacheOptions["${option.id}"];
                <c:forEach items="${option.incompatibleOptions}" var="iOption">
                    if(cacheOptions["${iOption.id}"] == undefined) {
                        option = new OptionECare("${iOption.id}", "${iOption.name}",
                                ${iOption.cost}, ${iOption.price}, [], []);
                        cacheOptions[option.id] = option;
                    } else
                        option = cacheOptions["${iOption.id}"];
                    option.iOptions[newOption.id] = newOption;
                    iOptions[option.id] = option;
                </c:forEach>
                <c:forEach items="${option.compatibleOptions}" var="cOption">
                    if(cacheOptions["${cOption.id}"] == undefined) {
                        option = new OptionECare("${cOption.id}", "${cOption.name}",
                                ${cOption.cost}, ${cOption.price}, [], []);
                        cacheOptions[option.id] = option;
                    } else
                        option = cacheOptions["${cOption.id}"];
                    option.cOptions[newOption.id] = newOption;
                    cOptions[option.id] = option;
                </c:forEach>
                tariffOptions[newOption.id] = newOption;
            </c:forEach>
            tariff = new TariffECare("${tariff.id}", "${tariff.name}", ${tariff.price}, tariffOptions);
            tariffs[tariff.name] = tariff;
        </c:forEach>
        cacheOptions = null;
    </script>
</head>
<body>
<div id="error_block">
    <div id="error_message" class="alert alert-danger alert-dismissible" role="alert">
        <button id="close_message" type="button" class="close"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <strong>Error!</strong> <span id="error_message_text"></span>
    </div>
</div>
<div class="main_block">
<c:set var="activeMenu" value="tariffs"/>
<%@ include file="/WEB-INF/views/layouts/_manager_body.jsp" %>
<div id="tariffsTable">
    <label class="tariff_label">Tariffs</label>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody id="tariffs">
        </tbody>
    </table>
    <div class="buttons_block">
        <button type="button" class="btn btn-success dropdown-toggle universalButton" id="add_tariff_button">Add
        </button>
        <button type="button" class="btn btn-danger dropdown-toggle universalButton" id="delete_tariff_button" disabled>
            Delete
        </button>
    </div>
</div>

<div id="big_options_block" class="inactive">
    <div class="clearfix">
        <label>Options</label><br>

        <div id="optionsTable">
            <label>All options</label>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Cost</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody id="options">
                </tbody>
            </table>
            <div class="buttons_block">
                <button type="button" class="btn btn-danger dropdown-toggle universalButton"
                        id="delete_option_button" disabled>Delete
                </button>
            </div>
        </div>
        <div id="add_options_block">
            <label>Add options</label>
            <br>
            <label class="characteristic_label">Name:</label>
            <input type="text" class="characteristic_field" id="option_name">
            <br>
            <label class="characteristic_label">Cost:</label>
            <input type="text" class="characteristic_field" id="option_cost">
            <br>
            <label class="characteristic_label">Price:</label>
            <input type="text" class="characteristic_field" id="option_price">
            <button type="button" class="btn btn-success dropdown-toggle universalButton" id="add_option_button"
                    disabled>
                Add
            </button>
        </div>
    </div>
    <div id="rules_for_options">
        <div class="clearfix>">
            <label>Rules for options:</label>
        </div>
        <div id="block_compatible_options">
            <label>Compatible</label>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody id="compatible_options">
                </tbody>
            </table>
            <div class="buttons_block_options">
                <button type="button" class="btn btn-success dropdown-toggle" id="add_compatible_options_button"
                        disabled>
                    Add
                </button>
                <button type="button" class="btn btn-danger dropdown-toggle" id="delete_compatible_options_button"
                        disabled>
                    Delete
                </button>
            </div>
        </div>
        <div id="block_incompatible_options">
            <label>Incompatible</label>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody id="incompatible_options">
                </tbody>
            </table>
            <div class="buttons_block_options">
                <button type="button" class="btn btn-success dropdown-toggle" id="add_incompatible_options_button"
                        disabled>
                    Add
                </button>
                <button type="button" class="btn btn-danger dropdown-toggle"
                        id="delete_incompatible_options_button" disabled>Delete
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addNewTariffModal">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content-tariff">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add tariff</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div>
                        <label class="characteristic_label">Name:</label>
                        <input type="text" class="characteristic_field" id="new_tariff_name">
                    </div>
                    <div>
                        <label class="characteristic_label">Price:</label>
                        <input type="text" class="characteristic_field" id="new_tariff_price">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="add_new_tariff_button" type="button"
                        class="btn btn-success dropdown-toggle universalButton">Add
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addNewCompatibleOptionModal">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content-compat-option">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add compatible option</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody id="add_compatible_options">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="add_new_compatible_option_button" type="button"
                        class="btn btn-success dropdown-toggle universalButton">Add
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addNewIncompatibleOptionModal">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content-incompat-option">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Add incompatible option</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody id="add_incompatible_options">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="add_new_incompatible_option_button" type="button"
                        class="btn btn-success dropdown-toggle universalButton">Add
                </button>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
</div>
</body>
</html>
