
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="/WEB-INF/views/layouts/_manager_head.jsp" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap-select.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/user-contracts.css"/>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/objects.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/jquery.session.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/add_contracts.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/addDeleteContracts.js"></script>
    <script id="forRemove" type="text/javascript">
        USER_ID = ${userId};
        <c:forEach items="${userContracts}" var="contract">
            var tariffOptions = [];
            var cOptions = [];
            var iOptions = [];
            <c:forEach items="${contract.tariff.options}" var="option">
            cOptions = [];
            iOptions = [];
            <c:forEach items="${option.incompatibleOptions}" var="iOption">
            iOptions["${iOption.id}"]=new OptionECare("${iOption.id}","${iOption.name}",
                    ${iOption.cost},${iOption.price},[],[]);
            </c:forEach>
            <c:forEach items="${option.compatibleOptions}" var="cOption">
            cOptions["${cOption.id}"]=new OptionECare("${cOption.id}","${cOption.name}",
                    ${cOption.cost},${cOption.price},[],[]);
            </c:forEach>
            tariffOptions["${option.id}"]=new OptionECare("${option.id}","${option.name}",
                    ${option.cost},${option.price},cOptions,iOptions);

            </c:forEach>
            var tariff = new TariffECare(${contract.tariff.id},"${contract.tariff.name}",${contract.tariff.price},tariffOptions);
            var contractOptions = [];
            <c:forEach items="${contract.selectedOptions}" var="option">
            cOptions = [];
            iOptions = [];
            <c:forEach items="${option.incompatibleOptions}" var="iOption">
            iOptions["${iOption.id}"]=new OptionECare("${iOption.id}","${iOption.name}",
                    ${iOption.cost},${iOption.price},[],[]);
            </c:forEach>
            <c:forEach items="${option.compatibleOptions}" var="cOption">
            cOptions["${cOption.id}"]=new OptionECare("${cOption.id}","${cOption.name}",
                    ${cOption.cost},${cOption.price},[],[]);
            </c:forEach>
            contractOptions.push(new OptionECare("${option.id}","${option.name}",
                    ${option.cost},${option.price},cOptions,iOptions));
            </c:forEach>
            var contract = new ContractECare(${contract.id},"${contract.phoneNumber}",${contract.active},
                    ${contract.whoBlocked},tariff,contractOptions);
            contracts[contract.phoneNumber]=contract;
        </c:forEach>

        <c:forEach items="${allTariffs}" var="tariff">
            tariffOptions = [];
            <c:forEach items="${tariff.options}" var="option">
            cOptions = [];
            iOptions = [];
            <c:forEach items="${option.incompatibleOptions}" var="iOption">
            iOptions["${iOption.id}"]=new OptionECare("${iOption.id}","${iOption.name}",
                    ${iOption.cost},${iOption.price},[],[]);
            </c:forEach>
            <c:forEach items="${option.compatibleOptions}" var="cOption">
            cOptions["${cOption.id}"]=new OptionECare("${cOption.id}","${cOption.name}",
                    ${cOption.cost},${cOption.price},[],[]);
            </c:forEach>
            tariffOptions["${option.id}"]=new OptionECare("${option.id}","${option.name}",
                    ${option.cost},${option.price},cOptions,iOptions);

            </c:forEach>
            tariff = new TariffECare(${tariff.id},"${tariff.name}",${tariff.price},tariffOptions);
            allTariffs[tariff.name]=tariff;
        </c:forEach>
    </script>
</head>
<body>
<c:set var="activeMenu" value="contracts"/>
<div id="error_block">
    <div id="error_message" class="alert alert-danger alert-dismissible" role="alert">
        <button id="close_message" type="button" class="close"><span
                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <strong>Error!</strong> <span id="error_message_text"></span>
    </div>
</div>
<div id="info_block">
    <div id = "info_message" class="alert alert-success alert-dismissible" role="alert">
        <button id="close_info_message" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <span id="info_message_text"></span>
    </div>
</div>
<div class="main_block">
    <%@ include file="/WEB-INF/views/layouts/_manager_body.jsp" %>
    <div id="contracts_selection">
        <div id=switch_block class="onoffswitch">
            <input id="block_check" type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch"
                   checked>
            <label class="onoffswitch-label" for="myonoffswitch">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>
        </div>
        <span id="is_blocked">Blocked</span>

        <div id="contracts_label">
            <label>Contracts</label>
            <select class="selectpicker show-tick" id="contracts">
            </select>
        </div>
        <button type="button" class="btn btn-success dropdown-toggle contract-button clearfix" id="saveButton">Save
        </button>
    </div>
    <c:if test="${!empty userContracts}">
        <div id="contract_info_block">
            <div id="tariff_block">
                <label class="tariff_label">Tariff</label>
                <br>
                <label class="characteristic_label">
                    Name:
                </label>
                <select disabled class="characteristic_field" id="tariff_name">
                </select>
                <button type="button" class="btn btn-success dropdown-toggle" id="change_tariff_button">Ok</button>
                <br>
                <label class="characteristic_label">
                    Price:
                </label>
                <input type="text" class="characteristic_field" id="price" value="5555" disabled="disabled">
                <br>
                <button type="button" class="btn btn-success dropdown-toggle contract-button" id="change_button">
                    Change
                </button>
                <button type="button" class="btn btn-danger contract-button" id="cancel_change_tariff_button">Cancel
                </button>
            </div>
            <div id="options_block">
                <label class="tariff_label">
                    Options:
                </label>
                <br>
                <table class="table table-bordered" id="options_table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody id="selected_options">
                    </tbody>
                </table>
                <button type="button" class="btn btn-success dropdown-toggle contract-button" id="add_button">Add
                </button>
                <button type="button" class="btn btn-danger dropdown-toggle contract-button" id="delete_button">Delete
                </button>
            </div>

        </div>

        <%@ include file="/WEB-INF/views/manager/addDeleteContracts.jsp" %>

        <div class="modal fade" id="addOptionsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                        <h4 class="modal-title">Add options</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label class="characteristic_label" id="modal_characteristic_label">
                                Tariff:
                            </label>
                            <input type="text" class="characteristic_field" value="tariff_0" disabled="disabled"
                                   id="modal_characteristic_field">
                        </div>
                        <div>
                            <ul class="list-group">
                                <li class="list-group-item disabled">Options</li>
                                <div id="options_list">
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="add_option" type="button" class="btn btn-success dropdown-toggle contract-button">
                            Add
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </c:if>

    <%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
</div>
</body>
</html>
