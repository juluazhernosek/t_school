<button id = "addContract" type="button" class="btn btn-success dropdown-toggle">Add</button>
<button type="button" class="btn btn-danger dropdown-toggle universalButton" id="deleteContract">
    Delete
</button>
<div class="modal fade" id="addContractsModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Contract</h4>
            </div>
            <div class="modal-body">
                <div>
                    <label class="contract-label">
                        Phone Number:
                    </label>
                    <input type="text" class="contract-field" id = "new_contract_number">
                </div>
            </div>
            <div class="modal-footer">
                <button id="add_new_contract" type="button" class="btn btn-success dropdown-toggle contract-button">
                    Add
                </button>
            </div>
        </div>
    </div>
</div>


