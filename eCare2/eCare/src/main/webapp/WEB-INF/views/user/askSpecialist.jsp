<%--
  Created by IntelliJ IDEA.
  User: julua
  Date: 15.09.14
  Time: 17:46
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%@ include file="/WEB-INF/views/layouts/_head.jsp" %>
        <link rel="stylesheet" type="text/css" href="./css/ask-specialist.css" />
    </head>
    <body>
    <c:set var="activeMenu" value="ask" />
        <div class = "main_block">
            <%@ include file="/WEB-INF/views/layouts/_body.jsp" %>
            <form role="form">
                <br>
                <label class = "ask_specialist_label">Ask a specialist:</label>
                <textarea class="form-control" id ="spec_textarea" rows="5"></textarea>
                <button type="submit" class="btn btn-default" id = "send_button">Send</button>
            </form>
            <%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
        </div>
    </body>
</html>
