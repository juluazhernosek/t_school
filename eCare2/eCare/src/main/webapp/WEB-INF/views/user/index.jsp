
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="/WEB-INF/views/layouts/_head.jsp" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/personal_cabinet.css" />
</head>
<body>
<c:set var="activeMenu" value="index" />
    <div class = "main_block ">
        <%@ include file="/WEB-INF/views/layouts/_body.jsp" %>
        <div class = "data">
            <div id ="personal_data">
                <h2>Your personal data:</h2>
                <img src="${pageContext.request.contextPath}/img/user.png" width ="150px" height = "180px" id = "user_image">
                <div id = "profile">
                    <label class = "profile_label">Name:</label>
                    <input type="text" class="user_field" id = "user_name" value = "${user.getName()}" disabled="disabled">
                    <br>
                    <label class = "profile_label">Surname:</label>
                    <input type="text" class="user_field" id = "user_surname" value = "${user.getSurname()}" disabled="disabled">
                    <br>
                    <label class = "profile_label">Birthday:</label>
                    <input type="text" class="user_field" id = "birthday" value = "${user.getBirthdayDate()}" disabled="disabled">
                    <br>
                    <label class = "profile_label">Passport:</label>
                    <input type="text" class="user_field" id = "passport" value = "${user.getPassport()}" disabled="disabled">
                    <br>
                    <label class = "profile_label">Address:</label>
                    <input type="text" class="user_field" id = "address" value = "${user.getAddress()}" disabled="disabled">
                    <br>
                    <label class = "profile_label">Email:</label>
                    <input type="text" class="user_field" id = "email" value = "${user.getEmail()}" disabled="disabled">
                </div>
            </div>
            <div class="shadow_block">
                <div id="myCarousel" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="${pageContext.request.contextPath}/img/slide-0.jpg">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1></h1>
                                    <p class="lead"></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="${pageContext.request.contextPath}/img/slide-1.png">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1></h1>
                                    <p class="lead"></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="${pageContext.request.contextPath}/img/slide-2.jpg" >
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1></h1>
                                    <p class="lead"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
    </div>

    <script>
        !function ($) {
            $(function(){
                $('#myCarousel').carousel()
            })
        }(window.jQuery)
    </script>

</body>
</html>
