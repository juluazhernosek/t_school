
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%@ include file="/WEB-INF/views/layouts/_head.jsp" %>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/about.css" />
    </head>
    <body>
    <c:set var="activeMenu" value="about" />
        <div class = "main_block">
            <%@ include file="/WEB-INF/views/layouts/_body.jsp" %>
            <div class = "inf-block">
                <label id = "main_label">Here could be your advertisement...</label>
                <br>
                <label id = "phone_number">Number: +7952264xxxxx</label>
            </div>
            <%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
        </div>
    </body>
</html>
