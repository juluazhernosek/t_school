<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%@ include file="/WEB-INF/views/layouts/_head.jsp" %>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap-select.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bucket.css" />
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/objects.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/jquery.session.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/bucket.js"></script>
        <script id="forRemove" type="text/javascript">
            <c:forEach items="${userContracts}" var="contract">
                var tariffOptions = [];
                var cOptions;
                var iOptions;
                <c:forEach items="${contract.tariff.options}" var="option">
                    cOptions = [];
                    iOptions = [];
                    <c:forEach items="${option.incompatibleOptions}" var="iOption">
                        iOptions["${iOption.id}"]=new OptionECare("${iOption.id}","${iOption.name}",
                                ${iOption.cost},${iOption.price},[],[]);
                    </c:forEach>
                    <c:forEach items="${option.compatibleOptions}" var="cOption">
                        cOptions["${cOption.id}"]=new OptionECare("${cOption.id}","${cOption.name}",
                                ${cOption.cost},${cOption.price},[],[]);
                    </c:forEach>
                    tariffOptions["${option.id}"]=new OptionECare("${option.id}","${option.name}",
                            ${option.cost},${option.price},cOptions,iOptions);
                </c:forEach>
                var tariff = new TariffECare(${contract.tariff.id},"${contract.tariff.name}",${contract.tariff.price},tariffOptions);
                var contractOptions = [];
                <c:forEach items="${contract.selectedOptions}" var="option">
                    cOptions = [];
                    iOptions = [];
                    <c:forEach items="${option.incompatibleOptions}" var="iOption">
                        iOptions["${iOption.id}"]=new OptionECare("${iOption.id}","${iOption.name}",
                                ${iOption.cost},${iOption.price},[],[]);
                    </c:forEach>
                    <c:forEach items="${option.compatibleOptions}" var="cOption">
                        cOptions["${cOption.id}"]=new OptionECare("${cOption.id}","${cOption.name}",
                                ${cOption.cost},${cOption.price},[],[]);
                    </c:forEach>
                    contractOptions["${option.id}"]=new OptionECare("${option.id}","${option.name}",
                            ${option.cost},${option.price},cOptions,iOptions);
                </c:forEach>
                var contract = new ContractECare(${contract.id},"${contract.phoneNumber}",${contract.active},
                        ${contract.whoBlocked},tariff,contractOptions);
                contracts[contract.phoneNumber]=contract;
            </c:forEach>

            <c:forEach items="${allTariffs}" var="tariff">
                tariffOptions = [];
                <c:forEach items="${tariff.options}" var="option">
                    cOptions = [];
                    iOptions = [];
                    <c:forEach items="${option.incompatibleOptions}" var="iOption">
                        iOptions["${iOption.id}"]=new OptionECare("${iOption.id}","${iOption.name}",
                                ${iOption.cost},${iOption.price},[],[]);
                    </c:forEach>
                    <c:forEach items="${option.compatibleOptions}" var="cOption">
                        cOptions["${cOption.id}"]=new OptionECare("${cOption.id}","${cOption.name}",
                                ${cOption.cost},${cOption.price},[],[]);
                    </c:forEach>
                    tariffOptions["${option.id}"]=new OptionECare("${option.id}","${option.name}",
                        ${option.cost},${option.price},cOptions,iOptions);

                </c:forEach>
                tariff = new TariffECare(${tariff.id},"${tariff.name}",${tariff.price},tariffOptions);
                allTariffs[tariff.name]=tariff;
            </c:forEach>
        </script>
    </head>
    <body>
    <c:set var="activeMenu" value="bucket" />
        <div class = "main_block">
            <%@ include file="/WEB-INF/views/layouts/_body.jsp" %>
            <div id = "contracts_selection">
                <label>Contracts</label>
                <select class="selectpicker show-tick" id="contracts_bucket"  class="characteristic_field">
                </select>
                <button type = "button" class = "btn btn-success dropdown-toggle bucket-button clearfix" id = "revertButton">Revert</button>
            </div>
            <div id = "contract_info_block">
                <div id ="changed_tariff_block">
                    <label class = "tariff_label">Changed Tariff</label>
                    <br>
                    <label class = "characteristic_label">
                        Name:
                    </label>
                    <input type="text" class="characteristic_field" id = "tariff_name" value = "" disabled="disabled">
                </div>
                <div id ="added_options_block">
                    <label class = "tariff_label">Added options</label>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Cost</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody id ="added_options_table">
                        </tbody>
                    </table>
                </div>
                <div id ="deleted_options_block">
                    <label class = "tariff_label">Deleted options</label>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Cost</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody id ="deleted_options_table">
                        </tbody>
                    </table>
                </div>
            </div>
            <%@ include file="/WEB-INF/views/layouts/_footer.jsp" %>
        </div>
    </body>
</html>
