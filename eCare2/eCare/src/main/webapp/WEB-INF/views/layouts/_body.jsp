
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div>
    <nav class="navbar navbar-default" role="navigation" id="main_menu">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li <c:if test="${activeMenu eq 'index'}">class="active"</c:if>><a href="index">Private Cabinet</a></li>
                    <li <c:if test="${activeMenu eq 'contracts'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/user/contracts">Contracts</a></li>
                    <li <c:if test="${activeMenu eq 'ask'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/user/askSpecialist">Ask specialist</a></li>
                    <li <c:if test="${activeMenu eq 'about'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/user/about">About</a></li>
                </ul>
                <a id="logout" href="<c:url value="/logout" />">Logout</a>
                <script type="text/javascript">
                    $("#logout").click(function(){
                        $.session.clear();
                    });
                </script>
            </div>
        </div>
    </nav>
    <div id = "logo" class="clearfix">
        <img src="${pageContext.request.contextPath}/img/t-logo.gif" id = "logo_image">
        <span id = "welcome_label">Welcome to our service!</span>
        <div id="block_bucket">
            <img src="${pageContext.request.contextPath}/img/bucket.png" name="bucket"  id = "bucket_image">
            <label for="bucket_image"><a href="${pageContext.request.contextPath}/user/bucket">My Bucket</a></label>
        </div>
    </div>
</div>

