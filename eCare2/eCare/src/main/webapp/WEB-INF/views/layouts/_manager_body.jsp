
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div>
    <nav class="navbar navbar-default" role="navigation" id="main_menu">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li <c:if test="${activeMenu eq 'index'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/manager/index">Private Cabinet</a></li>
                    <li <c:if test="${activeMenu eq 'contracts'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/manager/allContracts">All contracts</a></li>
                    <li <c:if test="${activeMenu eq 'tariffs'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/manager/tariffsAndOptions">Tariffs/Options</a></li>
                    <li <c:if test="${activeMenu eq 'about'}">class="active"</c:if>><a href="">About</a></li>
                </ul>
                <a id="logout" href="${pageContext.request.contextPath}/logout">Logout</a>
                <script type="text/javascript">
                    $("#logout").click(function(){
                        $.session.clear();
                    });
                </script>
            </div>
        </div>
    </nav>
    <div id = "logo" class="clearfix">
        <img src="${pageContext.request.contextPath}/img/t-logo.gif" id = "logo_image">
        <span id = "welcome_label">Welcome to our service!</span>
        <div id="block_bucket">
            <img src="${pageContext.request.contextPath}/img/bucket.png" name="bucket"  id = "bucket_image">
            <label for="bucket_image">My Bucket</label>
        </div>
    </div>

    <div id="bucket_block_remove">
        <div id = "contracts_selection_bucket">
            <label for="contracts_card">Contracts</label>
            </label><select class="form-control" id="contracts_card">
            </select>
            <button type = "button" class = "btn btn-success dropdown-toggle bucket-button clearfix" id = "revertButtonManager">Revert</button>
        </div>
        <ul class="nav nav-tabs" role="tablist" id="bucket_tab">
            <li class="active"><a href="#changedTariffs" role="tab" data-toggle="tab">Changed tariffs</a></li>
            <li><a href="#addedOptions" role="tab" data-toggle="tab">Added options</a></li>
            <li><a href="#deletedOptions" role="tab" data-toggle="tab">Delete options</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active block_popover_bucket" id="changedTariffs">
                <label>Changed Tariff</label>
                <label class = "characteristic_label">
                    Name:
                </label>
                <input type="text" class="characteristic_field_bucket" id = "tariff_name_bucket" value = "" disabled="disabled">
            </div>
            <div class="tab-pane block_popover_bucket" id="addedOptions">
                <label>Added options</label>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody id ="added_options_table_bucket">
                    </tbody>
                </table>
            </div>
            <div class="tab-pane block_popover_bucket" id="deletedOptions">
                <label>Deleted options</label>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody id ="deleted_options_table_bucket">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
