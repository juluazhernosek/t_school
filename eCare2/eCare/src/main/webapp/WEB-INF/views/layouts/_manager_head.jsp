
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main-user.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manager_bucket.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap-select.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap-responsive.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/jquery-2.1.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/activateManagerBucket.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/jquery.session.js"></script>