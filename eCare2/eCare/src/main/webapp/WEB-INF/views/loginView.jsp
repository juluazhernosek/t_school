<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.css" />
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/jquery-2.1.1.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.js"></script>
    </head>
    <body style="background-color:#E0FFFD">
        <div class="container" style="margin-top:10%">
            <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-info" >

                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                    </div>

                    <% if(request.getParameter("error") != null) { %>
                        <div class="alert alert-danger alert-dismissible" role="alert" style="margin: 10px 10px 0;">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <strong>Error!</strong> Login or password incorrect
                        </div>
                    <% } %>
                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                        <form method="POST" class="form-horizontal" action="<c:url value="/j_spring_security_check" />">

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="login" type="text" class="form-control" name="j_username" value="" placeholder="username"/>
                            </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="password" type="password" class="form-control" name="j_password" placeholder="password"/>
                            </div>

                            <div class="input-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="_spring_security_remember_me" />  Remember me
                                    </label>
                                </div>
                            </div>
                            <br>
                            <input type="submit" class="btn btn-success value="Log in" />

                       </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>