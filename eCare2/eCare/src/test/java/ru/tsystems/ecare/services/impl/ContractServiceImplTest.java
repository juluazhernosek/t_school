package ru.tsystems.ecare.services.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.tsystems.ecare.dao.ContractDao;
import ru.tsystems.ecare.dao.OptionDao;
import ru.tsystems.ecare.dao.TariffDao;
import ru.tsystems.ecare.entities.Option;
import ru.tsystems.ecare.services.ContractService;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ContractServiceImplTest {

    @Mock
    private ContractService contractService;

    @Mock
    private ContractDao contractDao;

    @Mock
    private TariffDao tariffDao;

    @Mock
    private OptionDao optionDao;

    private Option option1;
    private Option option2;
    private Option option3;
    private Option option4;


    private void setOptions() {

    }
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        setOptions();
        contractService = new ContractServiceImpl();

    }

    @Test
    public void testIsIncompatibleOptions() throws Exception {
        Option option1 = new Option();
        option1.setId(0);
        Option option2 = new Option();
        option2.setId(1);
        Option option3 = new Option();
        option3.setId(2);
        Option option4 = new Option();
        option4.setId(3);
        option1.setIncompatibleOptions(Arrays.asList(option2, option3));
        option2.setIncompatibleOptions(Arrays.asList(option1,option4));
        assertTrue(contractService.isIncompatibleOptions(option1, option2));
        option1.setIncompatibleOptions(Arrays.asList(option3));
        option2.setIncompatibleOptions(Arrays.asList(option4));
        assertFalse(contractService.isIncompatibleOptions(option1, option2));
    }

    @Test
    public void testIsCompatibleOptions() throws Exception {
        Option option1 = new Option();
        option1.setId(0);
        Option option2 = new Option();
        option2.setId(1);
        Option option3 = new Option();
        option3.setId(2);
        Option option4 = new Option();
        option4.setId(3);
        List<Option> allOptions = Arrays.asList(option1,option2,option3);
        option1.setCompatibleOptions(Arrays.asList(option2, option3));
        assertNull(contractService.isCompatibleOptions(option1, allOptions));
        option1.setCompatibleOptions(Arrays.asList(option3,option4));
        assertNotNull(contractService.isCompatibleOptions(option1, allOptions));
    }

}