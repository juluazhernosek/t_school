package ru.tsystems.ecare.controllers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.tsystems.ecare.dto.ContractDTO;
import ru.tsystems.ecare.dto.TariffDTO;
import ru.tsystems.ecare.dto.UserDTO;
import ru.tsystems.ecare.dto.request.CreateOptionRequestDTO;
import ru.tsystems.ecare.dto.request.SaveUserRequestDTO;
import ru.tsystems.ecare.entities.Contract;
import ru.tsystems.ecare.entities.Option;
import ru.tsystems.ecare.entities.User;
import ru.tsystems.ecare.services.ContractService;
import ru.tsystems.ecare.services.OptionService;
import ru.tsystems.ecare.services.TariffService;
import ru.tsystems.ecare.services.UserService;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ManagerControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private TariffService tariffService;

    @Mock
    private OptionService optionService;

    @Mock
    private ContractService contractService;

    @InjectMocks
    private ManagerController managerController;



    private MockMvc mockMvc;

    @Before
    public void setup() {

        // Process mock annotations
        MockitoAnnotations.initMocks(this);

        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(managerController).build();

    }

    @Test
    public void testIndex() throws Exception {
        User user = new User();
        user.setEmail("123");
        user.setId(1);
        user.setName("");
        user.setSurname("");
        user.setBirthdayDate(null);
        user.setPassport("");
        user.setAddress("");

        when(userService.getUserByLogin(anyString())).thenReturn(user);

        this.mockMvc.perform(get("/manager/index")).
                andExpect(status().isOk()).
                andExpect(view().name("manager/index")).
                andExpect(model().attribute("user",hasProperty("email",is("123"))));
    }


    @Test
    public void testAllContracts() throws Exception {
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setEmail("123");
        user.setId(1);
        user.setName("");
        user.setSurname("");
        user.setBirthdayDate(null);
        user.setPassport("");
        user.setAddress("");
        userList.add(user);
        when(userService.getAllClients()).thenReturn(userList);
        this.mockMvc.perform(get("/manager/allContracts"))
                .andExpect(status().isOk())
                .andExpect(view().name("manager/allContracts"));
    }

    @Test
    public void testAllContractsByPhone() throws Exception {
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setEmail("123");
        user.setId(1);
        user.setName("");
        user.setSurname("");
        user.setBirthdayDate(null);
        user.setPassport("");
        user.setAddress("");
        userList.add(user);
        when(userService.getUserByPhone(anyString())).thenReturn(userList);
        this.mockMvc.perform(get("/manager/allContracts").param("number","123"))
                .andExpect(status().isOk())
                .andExpect(view().name("manager/allContracts"));
    }

    @Test
    public void testCreateTariff() throws Exception {

        TariffDTO tariff = new TariffDTO();
        tariff.setName("123");
        tariff.setPrice(747);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        byte[] object = mapper.writeValueAsBytes(tariff);

        this.mockMvc.perform(post("/manager/allContracts/tariffs/create").
                content(object).
                contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"))))
                .andExpect(status().isOk())
                .andExpect(content().string("success"));
    }

    @Test
    public void testCreateContract() throws Exception {

        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setPhoneNumber("123");
        contractDTO.setActive(false);
        contractDTO.setWhoBlocked(true);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        byte[] object = mapper.writeValueAsBytes(contractDTO);
        Contract contract = new Contract();
        when(contractService.createContract(contract,1L)).thenReturn(true);
        this.mockMvc.perform(post("/manager/allContracts/user/{id}/contracts/create", 1L).
                content(object).
                contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"))))
                .andExpect(status().isOk())
                .andExpect(content().string("success"));
    }

    @Test
    public void testCreateUser() throws Exception {
        SaveUserRequestDTO requestDTO = new SaveUserRequestDTO();
        UserDTO userDTO = new UserDTO();
        userDTO.setId(0);
        requestDTO.setUser(userDTO);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        byte[] object = mapper.writeValueAsBytes(requestDTO);
        Contract contract = new Contract();
        when(contractService.createContract(contract,1L)).thenReturn(true);
        this.mockMvc.perform(post("/manager/allContracts/user/create").
                content(object).
                contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"))))
                .andExpect(status().is(400)); // not valid user
    }

    @Test
    public void testCreateOption() throws Exception {
        CreateOptionRequestDTO requestDTO = new CreateOptionRequestDTO();
        requestDTO.setPrice(54);
        requestDTO.setCost(54);
        requestDTO.setIdTariff(1);
        requestDTO.setName("fqwfr");
        Option option = new Option();
        option.setId(1);
        option.setName("fqwfr");
        option.setPrice(54);
        option.setCost(54);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        byte[] object = mapper.writeValueAsBytes(requestDTO);
        this.mockMvc.perform(post("/manager/allContracts/options/create").
                content(object).
                contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value("success"))
                .andExpect(jsonPath("idOption").value(0));
    }

}