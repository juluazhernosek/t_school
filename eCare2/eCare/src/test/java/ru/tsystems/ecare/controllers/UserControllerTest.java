package ru.tsystems.ecare.controllers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.tsystems.ecare.dto.request.SaveContractRequestDTO;
import ru.tsystems.ecare.entities.User;
import ru.tsystems.ecare.services.ContractService;
import ru.tsystems.ecare.services.TariffService;
import ru.tsystems.ecare.services.UserService;

import java.nio.charset.Charset;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for userController
 */
public class UserControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private TariffService tariffService;

    @Mock
    private ContractService contractService;

    @InjectMocks
    private UserController userController;



    private MockMvc mockMvc;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();

    }

    @Test
    public void indexTest() throws Exception {

        User user = new User();
        user.setEmail("123");
        user.setId(1);
        user.setName("");
        user.setSurname("");
        user.setBirthdayDate(null);
        user.setPassport("");
        user.setAddress("");

        when(userService.getUserByLogin(anyString())).thenReturn(user);

        this.mockMvc.perform(get("/user/index")).
                andExpect(status().isOk()).
                andExpect(view().name("user/index")).
                andExpect(model().attribute("user",hasProperty("email",is("123"))));
    }

    @Test
    public void saveContractTest() throws Exception {

        long[] optionsExample= new long[]{1L,2L,3L};
        when(contractService.saveContract(1L,2L, optionsExample, true, false)).thenReturn(true);

        SaveContractRequestDTO requestDTO = new SaveContractRequestDTO();
        requestDTO.setIdContract(1);
        requestDTO.setIdTariff(2);
        requestDTO.setOptions(optionsExample);
        requestDTO.setActive(true);
        requestDTO.setWhoBlocked(false);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        byte[] object = mapper.writeValueAsBytes(requestDTO);

        this.mockMvc.perform(post("/user/contracts/save").
                        content(object).
                 contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"))))
        .andExpect(status().isOk())
        .andExpect(content().string("success"));

    }

//    @Test
//    public void getUserContractsTest() throws Exception {
//
//        User user = new User();
//        user.setEmail("12");
//        user.setId(1);
//        user.setName("");
//        user.setSurname("");
//        user.setBirthdayDate(null);
//        user.setPassport("");
//        user.setAddress("");
//
//        Tariff tariff0 = new Tariff();
//        tariff0.setName("tariff0");
//
//        List<Option> compatibleOptions = new ArrayList<>();
//        List<Option> incompatibleOptions = new ArrayList<>();
//
//        Option option0 = new Option();
//        option0.setName("Option0");
//        option0.setCompatibleOptions(compatibleOptions);
//        option0.setIncompatibleOptions(incompatibleOptions);
//
//        tariff0.setOptions(compatibleOptions);
//
//        List<Option> selectedOptions = new ArrayList<>();
//        selectedOptions.add(option0);
//
//        Contract contract0 = new Contract();
//        contract0.setUser(user);
//        contract0.setTariff(tariff0);
//        contract0.setSelectedOptions(selectedOptions);
//
//        List<Contract> contracts = new ArrayList<>();
//        contracts.add(contract0);
//
//        ContractDTO contractDTO0 = new ContractDTO();
//        contractDTO0.setPhoneNumber("11111");
//
//        List<ContractDTO> contractsDTO = new ArrayList<>();
//        contractsDTO.add(contractDTO0);
//
//        List<Tariff> tariffs = new ArrayList<>();
//        tariffs.add(tariff0);
//
//        user.setContracts(contracts);
//
//        when(userService.getUserByLogin(anyString())).thenReturn(user);
//
//        UserController spy = PowerMockito.spy(userController);
//        PowerMockito.when(spy, method(UserController.class, "getContractDTOList", List.class))
//                .withArguments(contracts)
//                .thenReturn(contractsDTO);
//
//
//        PowerMockito.when(spy, method(UserController.class, "getAllTariffsDTO", List.class))
//                .withArguments(tariffs)
//                .thenReturn(tariffs);
//
//        this.mockMvc.perform(get("/user/contracts")).
//                andExpect(status().isOk()).
//                andExpect(view().name("user/contracts")).
//                andExpect(model().attribute("user", hasProperty("email", is("12"))));
//    }

}
